<?php get_header(); ?>

<?php
	//$glb_ctp_project biến toàn cục
	$term_info = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$term_id = $term_info->term_id;
	$term_name = $term_info->name;
	$taxonomy_name = $term_info->taxonomy;
	$term_link = esc_url(get_term_link($term_id));
?>

<section class="page-detail">
    <div class="container">
        <div class="module module__page-detail">

            <div class="module__header">
                <h2 class="title">
                    <?php echo $term_name; ?>
                </h2>
            </div>

            <div class="module__content">
                <div class="detail">
                    <div class="page__project">
                        <div class="bs-tab">
                            <div class="tab-container">

                                <div class="tab-content">

                                    <div class="tab-item active" id="tab_<?php echo $term_id; ?>">
                                        <div class="addon-project__group">

											<?php
												$query = query_post_by_taxonomy_paged($glb_ctp_project, $taxonomy_name, $term_id, 12);
												if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

							            		$post_title = get_the_title();
							            		$post_link = get_the_permalink();
							            		$post_image = getPostImage(get_the_ID(),"p-service-news-project");
							            		$post_date = 'Tháng '.get_the_date('m/ Y');
							            		$post_excerpt = cut_string(get_the_excerpt(),300,'...');
							            		$post_author = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
											?>
                                            <div class="addon__project">
                                                <a href="<?php echo $post_link; ?>" class="addon__project--box">
                                                    <div class="frame">
                                                        <img class="frame--image" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                                                    </div>
                                                    <h3 class="addon__project--title">
                                                        <?php echo $post_title; ?>
                                                    </h3>
                                                </a>
                                            </div>
                                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                                        </div>

                                        <?php
                                            $data_navigation = array(
                                                'query'     =>    $query,
                                                'cat_link'     =>    $term_link
                                            );
                                        ?>
                                        <?php get_template_part("resources/views/navigation",$data_navigation); ?>
						
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>