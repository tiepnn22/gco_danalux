<?php get_header(); ?>

<?php
	//$glb_ctp_service biến toàn cục
	$term_info = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$term_id = $term_info->term_id;
	$term_name = $term_info->name;
	$taxonomy_name = $term_info->taxonomy;
	$term_link = esc_url(get_term_link($term_id));
?>

<section class="page-service">
    <div class="container">
        <div class="module module__page-service">
            <div class="module__header">
                <h2 class="title">
                    <?php echo $term_name; ?>
                </h2>
            </div>
            <div class="module__content">
                <div class="row">
                    <div class="col-12  col-lg-8 col-xl-9 order-sm-2 order-md-2 order-lg-1">

						<?php
							$query = query_post_by_taxonomy_paged($glb_ctp_service, $taxonomy_name, $term_id, 5);
							if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

		            		$post_title = get_the_title();
		            		$post_link = get_the_permalink();
		            		$post_image = getPostImage(get_the_ID(),"p-service-news-project");
		            		$post_date = 'Tháng '.get_the_date('m/ Y');
		            		$post_excerpt = cut_string(get_the_excerpt(),300,'...');
		            		$post_author = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
						?>

	                        <div class="page-service__gulp">
	                            <div class="page-service__item">
	                                <a href="<?php echo $post_link; ?>" class="frame">
	                                    <img class="frame--image" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
	                                </a>
	                            </div>
	                            <div class="page-service__item">
	                                <div class="service__content">
	                                    <a href="<?php echo $post_link; ?>" class="service__title">
	                                        <?php echo $post_title; ?>
	                                    </a>
	                                    <div class="service__cateogry">
	                                        <p class="categroy__item">
	                                            <span class="icon">
	                                                <img src="<?php echo asset('images/icons/icon__user.png'); ?>" alt="icon__user.png">
	                                            </span>
	                                            <?php echo $post_author; ?>
	                                        </p>
	                                        <p class="categroy__item">
	                                            <span class="icon">
	                                                <img src="<?php echo asset('images/icons/icon__time.png'); ?>" alt="icon__time.png">
	                                            </span>
	                                            <?php echo $post_date; ?>
	                                        </p>
	                                    </div>
	                                    <div class="desc">
	                                        <p>
	                                            <?php echo $post_excerpt; ?>
	                                        </p>
	                                    </div>
	                                    <a href="<?php echo $post_link; ?>" class="btn btn__view">
	                                        <span class="text">
	                                            Xem thêm
	                                        </span>
	                                        <span class="icon">
	                                            <img src="<?php echo asset('images/icons/icon__view.png'); ?>" alt="icon__view.png">
	                                        </span>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>

                        <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

						<?php
						    $data_navigation = array(
						        'query'     =>    $query,
						        'cat_link'     =>    $term_link
						    );
						?>
						<?php get_template_part("resources/views/navigation",$data_navigation); ?>

                    </div>

                    <?php get_sidebar();?>

                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>