<?php
	//$glb_ctp_service biến toàn cục
	//$glb_ctp_tax_service biến toàn cục
	global $glb_ctp_service;
	global $glb_ctp_tax_service;
	
	global $post;
	$terms = get_the_terms( $post->ID , $glb_ctp_tax_service, 'string');
	$term_ids = wp_list_pluck($terms,'term_id');
	$query = new WP_Query( array(
		'post_type' => $glb_ctp_service,
		'tax_query' => array(
			array(
				'taxonomy' => $glb_ctp_tax_service,
				'field' => 'id',
				'terms' => $term_ids,
				'operator'=> 'IN'
			 )),
		'posts_per_page' => 3,
		'orderby' => 'date',
		'post__not_in'=>array($post->ID)
	) );
?>


<div class="addon__other-article">
    <h3 class="other-article__title">
        Các bài khác
    </h3>
    <div class="row">

		<?php
			if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

			$post_id = get_the_ID();
    		$post_title = get_the_title($post_id);
    		$post_content = get_the_content($post_id);
    		$post_date = get_the_date('Y/m/d', $post_id);
    		$post_link = get_post_permalink($post_id);
    		$post_image = getPostImage($post_id,"p-service-news-project");
    		$post_excerpt = cut_string(get_the_excerpt($post_id),300,'...');
		?>

	        <div class="col-12 col-sm-4 col-lg-4 ">
	            <a href="<?php echo $post_link; ?>" class="other">
	                <div class="frame">
	                    <img class="frame--image" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
	                </div>
	                <h3 class="title">
	                    <?php echo $post_title; ?>
	                </h3>
	            </a>
	        </div>

        <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

    </div>
</div>
