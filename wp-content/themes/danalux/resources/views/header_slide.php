<section class="slide__banner">
    <div class="slick__banner">

        <?php
            $home_slide = get_field('home_slide', 'option');

            foreach ($home_slide as $home_slide_kq) {

            $home_slide_image = $home_slide_kq['image'];
            $home_slide_link = $home_slide_kq['link'];

        ?>

	        <div class="slide__item">
	            <a href="<?php echo $home_slide_link; ?>" class="frame">
	                <img class="frame--image" data-lazy="<?php echo $home_slide_image; ?>" />
	            </a>
	        </div>

        <?php } ?>

    </div>
</section>