<?php
    global $data_single_meta;
    
    $post_author = $data_single_meta['post_author'];
    $post_date = $data_single_meta['post_date'];
?>

<div class="service__cateogry">
    <p class="categroy__item">
        <span class="icon">
            <img src="<?php echo asset('images/icons/icon__user.png'); ?>">
        </span>
        <?php echo $post_author; ?>
    </p>
    <p class="categroy__item">
        <span class="icon">
            <img src="<?php echo asset('images/icons/icon__time.png'); ?>">
        </span>
        Tháng <?php echo $post_date; ?>
    </p>
</div>