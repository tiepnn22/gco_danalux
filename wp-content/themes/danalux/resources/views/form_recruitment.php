<?php
    global $data_recruitment;
    
    $recruitment_form = $data_recruitment['recruitment_form'];

    date_default_timezone_set('Asia/Ho_Chi_Minh');//Ho_Chi_Minh là mặc định ở việt nam rồi
    $date_send = date('Y-m-d H:i:s');
?>

<section id="addon__form" class="addon__form">
    <div class="container">
        <div class="module module__addon__form">

            <?php
                if(!empty( $recruitment_form )) {
                    echo do_shortcode( $recruitment_form );
                }
            ?>

        </div>
    </div>
</section>

<script type="text/javascript">
    jQuery('.wpcf7-submit').click(function(){
        jQuery('.input__date').val( '<?php echo $date_send; ?>' );
    });
    document.addEventListener( 'wpcf7mailsent', function( event ) {
        jQuery('.fileName').empty();
    }, false );
</script>

<style type="text/css">
	.input__date { display: none; }
</style>

<?php
    // Call jss and css contact form 7
    if ( function_exists( 'wpcf7_enqueue_scripts' ) ) { wpcf7_enqueue_scripts(); }
    if ( function_exists( 'wpcf7_enqueue_styles' ) ) { wpcf7_enqueue_styles(); }
?>