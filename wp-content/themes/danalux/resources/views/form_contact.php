<?php
    global $data_contact;
    
    $contact_form = $data_contact['contact_form'];

    date_default_timezone_set('Asia/Ho_Chi_Minh');//Ho_Chi_Minh là mặc định ở việt nam rồi
    $date_send = date('Y-m-d H:i:s');
?>

<section class="addon__form">
    <div class="container">
        <div class="module module__addon__form">

            <?php
                if(!empty( $contact_form )) {
                    echo do_shortcode( $contact_form );
                }
            ?>

        </div>
    </div>
</section>

<script type="text/javascript">
    jQuery('.wpcf7-submit').click(function(){
        jQuery('.input__date').val( '<?php echo $date_send; ?>' );
    });
</script>

<style type="text/css">
    .input__date { display: none; }
</style>

<?php
    // Call jss and css contact form 7
    if ( function_exists( 'wpcf7_enqueue_scripts' ) ) { wpcf7_enqueue_scripts(); }
    if ( function_exists( 'wpcf7_enqueue_styles' ) ) { wpcf7_enqueue_styles(); }
?>