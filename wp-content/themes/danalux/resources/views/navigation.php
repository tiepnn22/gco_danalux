<!--làm thiếu trang search-->

<?php
    global $data_navigation;
    
    $query = $data_navigation['query'];
    $cat_link = $data_navigation['cat_link'];
?>

<div class="addon__pagination">
    <div class="pagination__item">
        <span>Trang</span>
        <input class="number_next" type="number" value="" onkeypress="enterInput(event)">
        <span class="number"></span>
    </div>

	<nav class="navigation">
		<?php wp_pagenavi( array( 'query' => $query ) ); ?>
	</nav>
</div>

<script type="text/javascript">
	var pagination_number_total = jQuery('.wp-pagenavi span.pages').html();
	
	//nếu trang >= 1 mới hiện phân trang
	if(pagination_number_total >= 1){
		jQuery('.pagination__item span.number').html(pagination_number_total);

		var pagination_number_current_redirect = jQuery('.wp-pagenavi span.current').html();
		jQuery('.pagination__item .number_next').val(pagination_number_current_redirect);

		function enterInput(e) {
			if (e.keyCode == 13) {
				// lấy số trang muốn đến
				var pagination_number_current = jQuery('.pagination__item .number_next').val();

				// redirect
				if(pagination_number_current <= pagination_number_total) {
					window.location.href = '<?php echo $cat_link; ?>'+"/page/"+pagination_number_current;
				}
			}
		}
	} else {
		jQuery('.addon__pagination').css('display', 'none');
	}
</script>