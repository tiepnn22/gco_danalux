<?php
	$sidebar_service_select_post = get_field('sidebar_service_select_post', 'option');
?>

<div class="col-12  col-lg-4 col-xl-3 order-sm-1 order-md-1 order-lg-2">
	<!-- <?php dynamic_sidebar('sidebar'); ?> -->

	<div class="addons__category">
	    <div class="addons__category--title">
	        Danh mục
	    </div>
	    <div class="addons__category--body">
	        <ul class="addons__category--list">

                <?php
                    foreach ($sidebar_service_select_post as $sidebar_service_select_post_kq) {

                    $post_id = $sidebar_service_select_post_kq->ID;
                    $post_title = get_the_title($post_id);
                    $post_date = get_the_date('d/m/Y', $post_id);
                    $post_link = get_post_permalink($post_id);
                    $post_image = getPostImage($post_id,"p-service-news-project");
                    $post_excerpt = cut_string(get_the_excerpt($post_id),300,'...');
                ?>

	                <li class="addons__category--item">
	                    <a href="<?php echo $post_link; ?>" class="addons__category--link">
	                        <?php echo $post_title; ?>
	                    </a>
	                </li>

	            <?php } ?>

	        </ul>
	    </div>
	</div>

</div>