<?php get_header(); ?>

<?php
	$page_id = get_the_ID();
	$page_name = get_the_title();
	$page_content = get_the_content(); //woo phải dùng the_content()
?>

<section class="page-introduce">
    <div class="container">
        <div class="module module__page-introduce">
            <div class="module__header">
                <h2 class="title">
                    <?php echo $page_name; ?>
                </h2>
            </div>
            <div class="module__content">
                <div class="introduce">
                    <?php wpautop( the_content() ); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>