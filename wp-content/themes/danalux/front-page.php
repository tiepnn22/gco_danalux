<?php get_header(); ?>

<?php
    $home_intro_title = get_field('home_intro_title');
    $home_intro_desc = get_field('home_intro_desc');
    $home_intro_image = get_field('home_intro_image');

    $home_service_title = get_field('home_service_title');
    $home_service_content = get_field('home_service_content');

    $home_project_title = get_field('home_project_title');
    $home_project_desc = get_field('home_project_desc');
    $home_project_select_cat = get_field('home_project_select_cat');

    $home_ads_content = get_field('home_ads_content');

    $home_intro_video_title = get_field('home_intro_video_title');
    $home_intro_video_image = get_field('home_intro_video_image');
    $home_intro_video_iframe = get_field('home_intro_video_iframe');
    $home_intro_video_desc = get_field('home_intro_video_desc');

    $home_news_title = get_field('home_news_title');
    $home_news_selectpost = get_field('home_news_selectpost');
    $home_news_readmore = get_field('home_news_readmore');
    $home_news_readmore_url = get_field('home_news_readmore_url');
?>

<section class="home-about">
    <div class="container">
        <div class="module module__home-about">
            <div class="module__content">
                <div class="about__group">
                    <div class="about__item">
                        <img class="about--link" src="<?php echo $home_intro_image; ?>" alt="<?php echo $home_intro_title; ?>">
                    </div>
                    <div class="about__item">
                        <div class="about__content">
                            <h3 class="about__title">
                                <?php echo $home_intro_title; ?>
                            </h3>
                            <div class="about__desc">
                                <?php echo wpautop( $home_intro_desc ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if(!empty( $home_service_content )) { ?>
<section class="home-service">
    <div class="module module__home-service">
        <div class="container">
            <div class="module__header">
                <h2 class="title">
                    <?php echo $home_service_title; ?>
                </h2>
            </div>
        </div>
        <div class="module__content">
            <div class="service__group">

                <?php
                    foreach ($home_service_content as $home_service_content_kq) {

                    $post_title = $home_service_content_kq['title'];
                    $post_image = $home_service_content_kq['image'];
                    $post_link = $home_service_content_kq['link'];
                ?>

                <div class="service__item">
                    <a href="<?php echo $post_link; ?>" class="service__box">
                        <img class="service__img" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                        <h2 class="service__text">
                            <?php echo $post_title; ?>
                        </h2>
                    </a>
                </div>
                
                <?php } ?>

            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php if(!empty( $home_project_select_cat )) { ?>
<section class="home-project">
    <div class="container">
        <div class="module module__home-project">
            <div class="module__header">
                <h2 class="title">
                    <?php echo $home_project_title; ?>
                </h2>
                <p class="info">
                    <?php echo $home_project_desc; ?>
                </p>
            </div>
            <div class="module__content">
                <div class="project__group">

                    <?php
                        // $glb_ctp_tax_project biến toàn cục
                        foreach ($home_project_select_cat as $home_project_select_cat_kq) {

                        $term_id = $home_project_select_cat_kq;
                        $term_link = get_term_link(get_term( $term_id ));
                        $term_name = get_term( $term_id, $glb_ctp_tax_project )->name;
                        $term_desc = get_term( $term_id, $glb_ctp_tax_project )->description;
                        $term_image_check = get_field('image_cat', 'category_'.$term_id);
                        $term_image = (!empty($term_image_check)) ? $term_image_check : asset('images/no-image-wc.png');
                    ?>

                    <div class="project__item">
                        <div class="project__box">
                            <a href="<?php echo $term_link; ?>" class="frame">
                                <img class="frame--image" src="<?php echo $term_image; ?>" alt="<?php echo $term_name; ?>">
                            </a>
                            <div class="project__content">
                                <h3 class="project__title">
                                    <a href="<?php echo $term_link; ?>">
                                        <?php echo $term_name; ?>
                                    </a>
                                </h3>
                                <p class="project__desc">
                                    <?php echo $term_desc; ?>
                                </p>
                                <a href="<?php echo $term_link; ?>" class="btn btn__link">
                                    Xem thêm..
                                </a>
                            </div>
                        </div>
                    </div>

                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php if(!empty( $home_ads_content )) { ?>
<section class="home-field">
    <div class="container">
        <div class="module module__home-field">
            <div class="module__content">
                <div class="field__group">

                    <?php
                        foreach ($home_ads_content as $home_ads_content_kq) {

                        $post_title = $home_ads_content_kq['title'];
                        $post_image = $home_ads_content_kq['image'];
                        $post_desc = $home_ads_content_kq['desc'];
                    ?>

                    <div class="field__item">
                        <div class="field__icon">
                            <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                        </div>
                        <div class="field__content">
                            <h3 class="field__number">
                                <?php echo $post_title; ?>
                            </h3>
                            <p class="field__text">
                                <?php echo $post_desc; ?>
                            </p>
                        </div>
                    </div>
                    
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php if(!empty( $home_news_selectpost )) { ?>
<section class="home-new">
    <div class="container">
        <div class="module module__home-new">
            <div class="module__content">
                <div class="new__group">
                    <div class="new__item">
                        <h3 class="new__title">
                            <?php echo $home_intro_video_title; ?>
                        </h3>
                        <div class="video">
                            <div class="play">
                                <div class="introduce__thumbnail">
                                    <button type="button" class="btn btn__play">
                                        <img src="<?php echo asset('images/icons/icon__video--play.png'); ?>">
                                    </button>
                                    <img src="<?php echo $home_intro_video_image; ?>" />
                                </div>
                                <?php echo $home_intro_video_iframe; ?>
                            </div>
                        </div>
                        <p>
                            <?php echo $home_intro_video_desc; ?>
                        </p>
                    </div>
                    <div class="new__item">
                        <h3 class="new__title">
                            <?php echo $home_news_title; ?>
                        </h3>

                        <?php
                            foreach ($home_news_selectpost as $home_news_selectpost_kq) {

                            $post_id = $home_news_selectpost_kq->ID;
                            $post_title = get_the_title($post_id);
                            $post_date = get_the_date('d/m/Y', $post_id);
                            $post_link = get_post_permalink($post_id);
                            $post_image = getPostImage($post_id,"p-service-news-project");
                            $post_excerpt = cut_string(get_the_excerpt($post_id),300,'...');
                        ?>

                        <div class="post__group">
                            <div class="post__item">
                                <a href="<?php echo $post_link; ?>" class="frame">
                                    <img class="frame--image" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                                </a>
                            </div>
                            <div class="post__item">
                                <h3 class="post__title">
                                    <a href="<?php echo $post_link; ?>">
                                        <?php echo $post_title; ?>
                                    </a>
                                </h3>
                                <p class="post__desc">
                                    <?php echo $post_excerpt; ?>
                                </p>
                            </div>
                        </div>

                        <?php } ?>

                        <a href="<?php echo $home_news_readmore_url; ?>" class="btn btn__link">
                            <?php echo $home_news_readmore; ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php get_footer(); ?>

<script>
    $(document).ready(function() {
        function playVideo() {
            $(".play").click(function() {
                $(this).children(".introduce__thumbnail").toggleClass("active");
            });
        }
        playVideo();
    })
</script>
