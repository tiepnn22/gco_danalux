<?php get_header(); ?>

<section class="page-detail">
    <div class="container">
        <div class="module module__page-detail">
            <div class="module__header">
                <h2 class="title">
                    <?php _e('Tìm kiếm cho', 'text_domain'); ?> : <?php echo '['.$_GET['s'].']'; ?>
                </h2>
            </div>
            <div class="module__content">
                <div class="detail">
                    <div class="page__project">

                        <div class="addon-project__group">

							<?php
								if(have_posts()) : while (have_posts() ) : the_post();

			            		$post_title = get_the_title();
			            		$post_link = get_the_permalink();
			            		$post_image = getPostImage(get_the_ID(),"p-service-news-project");
			            		$post_date = get_the_date('d/m/Y');
			            		$post_excerpt = cut_string(get_the_excerpt(),300,'...');
							?>

	                            <div class="addon__project">
	                                <a href="<?php echo $post_link; ?>" class="addon__project--box">
	                                    <div class="frame">
	                                        <img class="frame--image" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
	                                    </div>
	                                    <h3 class="addon__project--title">
	                                        <?php echo $post_title; ?>
	                                    </h3>
	                                </a>
	                            </div>

                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                        </div>

						<div class="addon__pagination">
						    <div class="pagination__item">
						        <span>Trang</span>
						        <input class="number_next" type="number" value="1" onkeypress="enterInput(event)">
						        <span class="number">10</span>
						    </div>

							<nav class="navigation">
								<?php wp_pagenavi(); ?>
							</nav>
						</div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<?php get_footer(); ?>

<script type="text/javascript">
	var pagination_number_total = jQuery('.wp-pagenavi span.pages').html();

	//nếu trang >= 1 mới hiện phân trang
	if(pagination_number_total >= 1){
		jQuery('.pagination__item span.number').html(pagination_number_total);

		var pagination_number_current_redirect = jQuery('.wp-pagenavi span.current').html();
		jQuery('.pagination__item .number_next').val(pagination_number_current_redirect);

		function enterInput(e) {
			if (e.keyCode == 13) {
				// lấy số trang muốn đến
				var pagination_number_current = jQuery('.pagination__item .number_next').val();

				// redirect
				if(pagination_number_current <= pagination_number_total) {
					window.location.href = '<?php echo get_option('home');?>'+"/page/"+pagination_number_current+'?s=<?php echo $_GET['s']; ?>';
				}
			}
		}
	} else {
		jQuery('.addon__pagination').css('display', 'none');
	}
</script>