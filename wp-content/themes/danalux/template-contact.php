<?php
	/*
	Template Name: Template Liên hệ
	*/
?>

<?php get_header(); ?>

<?php
    $page_id = get_the_ID();
    $page_name = get_the_title();
    $page_content = get_the_content();

    $contact_form = get_field('contact_form');
    $data_contact = array(
        'contact_form'     =>    $contact_form
    );
?>

<section class="page-contact">
    <div class="container">
        <div class="module module__page-contact">
            <div class="module__header">
                <h2 class="title">
                    <?php echo $page_name; ?>
                </h2>
            </div>
            <div class="module__content">
                <div class="contact">
                    
                    <?php wpautop( the_content() ); ?>

                    <div class="contact__info">
                        <div class="info__group">
                            <div class="info__item">
                                <h3 class="info__title">Văn phòng tại:</h3>
                                <p class="info__text">
                                    <?php echo wpautop( get_field('contact_address') ); ?>
                                </p>
                            </div>
                        </div>
                        <div class="info__group">
                            <div class="info__item">
                                <h3 class="info__title">EMAIL LIÊN HỆ:</h3>
                                <p class="info__text">
                                    <?php echo wpautop( get_field('contact_email') ); ?>
                                </p>
                            </div>
                            <div class="info__item">
                                <h3 class="info__title">Giờ làm việc:</h3>
                                <p class="info__text">
                                    <?php echo wpautop( get_field('contact_work') ); ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="contact__map">
                        <h2 class="map__title">Bản đồ tới văn phòng công ty</h2>
                        <?php echo get_field('contact_map'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_template_part("resources/views/form_contact",$data_contact); ?>


<?php get_footer(); ?>