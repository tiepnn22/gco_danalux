<?php get_header(); ?>

<?php
	$get_category =  get_the_category(get_the_ID());
	foreach ( $get_category as $get_category_kq ) {
	    $cat_id = $get_category_kq->term_id;
	}
	$cat_name = get_cat_name($cat_id);

	// post_author
	$recent_author = get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
	$post_author = $recent_author->display_name;

    $post_date = get_the_date('m/ Y');

    $data_single_meta = array(
        'post_author'     =>    $post_author, 
        'post_date'    =>    $post_date
    );
?>

<section class="page-detail">
    <div class="container">
        <div class="module module__page-detail">
            <div class="module__content">
                <div class="detail">
                	
                    <h2 class="detail__title">
                        <?php the_title(); ?>
                    </h2>

                    <?php get_template_part("resources/views/single-meta",$data_single_meta); ?>

                    <p>
                    	<?php echo wpautop( the_content() ); ?>
                	</p>

                    <?php get_template_part("resources/views/social-bar"); ?>

					<?php get_template_part("resources/views/template-related-post"); ?>

                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>