<footer id="footer">
    <div class="container">
        <div class="row">

            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                <div class="footer__item">
                    <h2 class="footer__logo">
                        <a href="<?php echo get_option('home');?>" class="footer__link">
                            <img class="logo__link" src="<?php echo get_field('f_logo', 'option'); ?>" alt="<?php echo get_option('blogname'); ?>">
                        </a>
                    </h2>
                    <p class="footer__info">
                        <?php echo get_field('f_slogan', 'option'); ?>
                    </p>
                    <ul class="footer__list">
                        <li class="footer__item">
                            <a href="javascript:void(0)" class="footer__list--link">
                                <span class="icon">
                                    <img src="<?php echo asset('images/icons/icon__home.png'); ?>" alt="icon__home.png">
                                </span>
                                <span class="text">
                                    <?php echo get_field('f_address', 'option'); ?>
                                </span>
                            </a>
                        </li>
                        <li class="footer__item">
                            <a href="javascript:void(0)" class="footer__list--link">
                                <span class="icon">
                                    <img src="<?php echo asset('images/icons/icon__phone.png'); ?>" alt="icon__phone.png">
                                </span>
                                <span class="text">
                                    <?php echo get_field('f_phone', 'option'); ?>
                                </span>
                            </a>
                        </li>
                        <li class="footer__item">
                            <a href="javascript:void(0)" class="footer__list--link">
                                <span class="icon">
                                    <img src="<?php echo asset('images/icons/icon__map.png'); ?>" alt="icon__map.png">
                                </span>
                                <span class="text">
                                    <?php echo get_field('f_gmail', 'option'); ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                <h2 class="footer__title">
                    <?php echo get_field('f_menu_title', 'option'); ?>
                </h2>
                <ul class="footer__menu">

                    <?php
                        $f_menu_content = get_field('f_menu_content', 'option');

                        foreach ($f_menu_content as $f_menu_content_kq) {

                        $f_menu_content_title = $f_menu_content_kq['title'];
                        $f_menu_content_link = $f_menu_content_kq['link'];

                    ?>
                        <li class="footer__item">
                            <a href="<?php echo $f_menu_content_link; ?>" class="footer__item--link">
                                <span class="icon">
                                    <i class="fal fa-angle-right"></i>
                                </span>
                                <span class="text">
                                    <?php echo $f_menu_content_title; ?>
                                </span>
                            </a>
                        </li>
                    <?php } ?>

                </ul>
            </div>

            <div class="col-12  col-lg-4">
                <div class="footer__map">
                    <?php echo get_field('f_map_iframe', 'option'); ?>
                </div>
            </div>

        </div>
    </div>
</footer>

<button class="back-top">
    <i class="fas fa-arrow-up"></i>
</button>

<a href="tel:<?php echo str_replace(' ','',get_field('h_phone', 'option'));?>" class="phone__shaking">
    <div class="phone__icon">
        <img src="<?php echo asset('images/icons/icon__phone.png'); ?>" alt="icon__phone.png">
    </div>
    <span class="text__phone"><?php echo get_field('h_phone', 'option'); ?></span>
</a>

<?php wp_footer(); ?>
</body>
</html>