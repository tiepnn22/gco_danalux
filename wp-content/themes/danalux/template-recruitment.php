<?php
	/*
	Template Name: Template Tuyển dụng
	*/
?>

<?php get_header(); ?>

<?php
    $page_id = get_the_ID();
    $page_name = get_the_title();
    $page_content = get_the_content();

    $contact_recruitment_select_postmap = get_field('recruitment_select_post');

    $recruitment_environment_title = get_field('recruitment_environment_title');
    $recruitment_environment_content = get_field('recruitment_environment_content');

    $recruitment_title = get_field('recruitment_title');

    $recruitment_bonus_title = get_field('recruitment_bonus_title');
    $recruitment_bonus_content = get_field('recruitment_bonus_content');
    $recruitment_bonus_image = get_field('recruitment_bonus_image');

    $recruitment_welfare_title = get_field('recruitment_welfare_title');
    $recruitment_welfare_content = get_field('recruitment_welfare_content');
    $recruitment_welfare_gallery = get_field('recruitment_welfare_gallery');

    $recruitment_form = get_field('recruitment_form');
    $data_recruitment = array(
        'recruitment_form'     =>    $recruitment_form
    );

    $recruitment_envi_title = get_field('recruitment_envi_title');
    $recruitment_envi_content = get_field('recruitment_envi_content');
    $recruitment_envi_image = get_field('recruitment_envi_image');

    $recruitment_time = get_field('recruitment_time');
    $recruitment_time_content = get_field('recruitment_time_content');
    $recruitment_time_image = get_field('recruitment_time_image');
?>

<section class="page-recruitment">
    <div class="container">
        <div class="module module__page-recruitment">
            <div class="module__header">
                <h2 class="title">
                    <?php echo $page_name; ?>
                </h2>
            </div>
            <div class="module__content">
                <div class="recruitment">

                    <div class="recruitment-new">
                        <?php
                            foreach ($contact_recruitment_select_postmap as $contact_recruitment_select_postmap_kq) {

                            $post_id = $contact_recruitment_select_postmap_kq->ID;
                            $post_title = get_the_title($post_id);
                            $post_date = get_the_date('d/m/Y', $post_id);
                            $post_link = get_post_permalink($post_id);
                            $post_image = getPostImage($post_id,"p-recruitment");
                            $post_excerpt = cut_string(get_the_excerpt($post_id),300,'...');
                        ?>

                        <div class="recruitment-new__group">
                            <div class="recruitment-new__item">
                                <a href="<?php echo $post_link; ?>" class="frame">
                                    <img class="frame--image" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                                </a>
                            </div>
                            <div class="recruitment-new__item">
                                <div class="recruitment-new__content">
                                    <h3 class="recruitment-new__title">
                                        <a href="<?php echo $post_link; ?>">
                                            <?php echo $post_title; ?>
                                        </a>
                                    </h3>
                                    <div class="recruitment-new__desc">
                                        <p>
                                            <?php echo $post_excerpt; ?>
                                        </p>
                                    </div>
                                    <a href="#addon__form" class="btn btn__link">
                                        Ứng tuyển ngay
                                    </a>
                                </div>
                            </div>
                        </div>

                        <?php } ?>
                    </div>

                    <div class="work-environment">
                        <h2 class="work-environment__title">
                            <?php echo $recruitment_environment_title; ?>
                        </h2>
                        <div class="work-environment__slide">

                            <?php
                                foreach ($recruitment_environment_content as $recruitment_environment_content_kq) {

                                $post_title = $recruitment_environment_content_kq['title'];
                                $post_image = $recruitment_environment_content_kq['image'];
                                // $post_link = $recruitment_environment_content_kq['link'];
                            ?>

                            <div class="slide__item">
                                <div class="slide__box">
                                    <div class="avata">
                                        <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                                    </div>
                                    <p class="work-environment__desc">
                                        <?php echo $post_title; ?>
                                    </p>
                                </div>
                            </div>

                            <?php } ?>

                        </div>
                    </div>

                    <div class="benefits">
                        <h2 class="benefits__title">
                            <?php echo $recruitment_title; ?>
                        </h2>
                        <div class="group__sm">

                            <div class="benefits__group">
                                <div class="benefits__item">
                                    <div class="frame clickZoom" data-image="<?php echo $recruitment_bonus_image; ?>" data-toggle="modal" data-target=".bd-example-modal-lg">
                                        <img class="frame--image" src="<?php echo $recruitment_bonus_image; ?>">
                                    </div>
                                </div>
                                <div class="benefits__item">
                                    <div class="enviroment__content">
                                        <h3 class="benefis__name">
                                            <?php echo $recruitment_bonus_title; ?>
                                        </h3>
                                        <ul class="benefis__list">

                                            <?php
                                                foreach ($recruitment_bonus_content as $recruitment_bonus_content_kq) {

                                                $post_title = $recruitment_bonus_content_kq['title'];
                                            ?>

                                            <li class="benefis__item">
                                                <span class="icon">
                                                    <img src="<?php echo asset('images/icons/icon__sa.png'); ?>">
                                                </span>
                                                <span class="text">
                                                    <?php echo $post_title; ?>
                                                </span>
                                            </li>

                                            <?php } ?>

                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="benefits__group">
                                <div class="benefits__item">
                                    <div class="benefits__slide">

                                        <?php
                                            foreach ($recruitment_welfare_gallery as $recruitment_welfare_gallery_kq) {

                                            $post_image = $recruitment_welfare_gallery_kq;
                                        ?>

                                        <div class="benefits-slide__item">
                                            <div class="benefits__box">
                                                <div class="frame clickZoom" data-image="<?php echo $post_image; ?>" data-toggle="modal" data-target=".bd-example-modal-lg">
                                                    <img class="frame--image" src="<?php echo $post_image; ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <?php } ?>

                                    </div>
                                </div>
                                <div class="benefits__item">
                                    <div class="benefis__content">
                                        <h3 class="benefis__name">
                                            <?php echo $recruitment_welfare_title; ?>
                                        </h3>
                                        <ul class="benefis__list">

                                            <?php
                                                foreach ($recruitment_welfare_content as $recruitment_welfare_content_kq) {

                                                $post_title = $recruitment_welfare_content_kq['title'];
                                            ?>

                                            <li class="benefis__item">
                                                <span class="icon">
                                                    <img src="<?php echo asset('images/icons/icon__sa.png'); ?>">
                                                </span>
                                                <span class="text">
                                                    <?php echo $post_title; ?>
                                                </span>
                                            </li>

                                            <?php } ?>

                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


<?php get_template_part("resources/views/form_recruitment",$data_recruitment); ?>


<section class="page__enviroment">
    <div class="container">
        <div class="module module__page__enviroment">
            <div class="module__content">
                <div class="enviroment__box">
                    <h2 class="enviroment__title">
                        <?php echo $recruitment_envi_title; ?>
                    </h2>
                    <div class="enviroment__group">
                        <div class="enviroment__item">
                            <ul class="enviroment__list">

                                <?php
                                    foreach ($recruitment_envi_content as $recruitment_envi_content_kq) {

                                    $post_title = $recruitment_envi_content_kq['title'];
                                ?>

                                <li class="enviroment__list--item">
                                    <span class="icon">
                                        <img src="<?php echo asset('images/icons/icon__sa.png'); ?>">
                                    </span>
                                    <span class="text">
                                        <?php echo $post_title; ?>
                                    </span>
                                </li>

                                <?php } ?>

                            </ul>
                        </div>
                        <div class="enviroment__item">
                            <div class="frame clickZoom" data-image="<?php echo $recruitment_envi_image; ?>" data-toggle="modal" data-target=".bd-example-modal-lg">
                                <img class="frame--image" src="<?php echo $recruitment_envi_image; ?>" alt="enviroment__1.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="enviroment__box">
                    <h2 class="enviroment__title">
                        <?php echo $recruitment_time; ?>
                    </h2>
                    <div class="enviroment__group">
                        <div class="enviroment__item">
                            <ul class="enviroment__list">

                                <?php
                                    foreach ($recruitment_time_content as $recruitment_time_content_kq) {

                                    $post_title = $recruitment_time_content_kq['title'];
                                ?>

                                <li class="enviroment__list--item">
                                    <span class="icon">
                                        <img src="<?php echo asset('images/icons/icon__sa.png'); ?>">
                                    </span>
                                    <span class="text">
                                        <?php echo $post_title; ?>
                                    </span>
                                </li>

                                <?php } ?>

                            </ul>
                        </div>
                        <div class="enviroment__item">
                            <div class="frame clickZoom" data-image="<?php echo $recruitment_time_image; ?>" data-toggle="modal" data-target=".bd-example-modal-lg">
                                <img class="frame--image" src="<?php echo $recruitment_time_image; ?>" alt="enviroment__2.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <button type="button" class="close close__modal" data-dismiss="modal" aria-label="Close">
                <i class="fas fa-times-circle"></i>
            </button>
            <div id="html__img">
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

<script>
    $(document).ready(function() {
        function slideEnvironment() {
            $('.work-environment__slide').slick({
                dots: false,
                infinite: false,
                arrows: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 3,
                responsive: [{
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }

                ]
            });
        }
        slideEnvironment();

        function slideBenefits() {
            $('.benefits__slide').slick({
                dots: false,
                infinite: false,
                arrows: false,
                speed: 300,
                slidesToShow: 2,
                slidesToScroll: 1,
                responsive: [{
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }

                ]
            });
        }
        slideBenefits();

        function slickZoomImage() {
            $('.clickZoom').click(function() {
                var linkUrl = $(this).attr('data-image');
                $('#html__img').html("<img class='zoom__w' src='" + linkUrl + "' alt=" + linkUrl + " >")


            })
        }
        slickZoomImage();
    })
</script>