<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">
</head>
<body <?php body_class(); ?>>


<?php
    //Biến toàn cục cho custom post type, taxonomy
    global $glb_ctp_service;
    global $glb_ctp_project;
    global $glb_ctp_recruitment;

    global $glb_ctp_tax_service;
    global $glb_ctp_tax_project;

    $glb_ctp_service = "dichvu";
    $glb_ctp_project = "duan";
    $glb_ctp_recruitment = "tuyendung";

    $glb_ctp_tax_service = "danhmuc-dichvu"; //(còn sót 1 trong widget tax)
    $glb_ctp_tax_project = "danhmuc-duan";
?>


<header id="header">

    <div class="header__top">
        <div class="container">
            <div class="top__group">

                <div class="top__item">
                    <a href="mailto:<?php echo get_field('h_gmail', 'option'); ?>" class="top__text">
                        <span class="icon"> <i class="fal fa-envelope"></i> </span>
                        <?php echo get_field('h_gmail', 'option'); ?>
                    </a>
                    <a href="tel:<?php echo str_replace(' ','',get_field('h_phone', 'option'));?>" class="top__text">
                        Hotline tư vấn: <?php echo get_field('h_phone', 'option'); ?>
                    </a>
                </div>

                <form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="top__item form__search">
                    <input type="text" class="form-control form__input" placeholder="Tìm kiếm" name="s" value="<?php echo get_search_query(); ?>" required>
                    <button type="submit" class="btn btn__search"><i class="fal fa-search"></i></button>
                </form>

            </div>
        </div>
    </div>

    <div class="scrolled__container">
        <div class="container">
            <div class="header__group">

                <div class="group__item">
                    <h1 class="logo">
                        <a href="<?php echo get_option('home');?>" class="logo__link">
                            <img src="<?php echo get_field('h_logo', 'option'); ?>" alt="<?php echo get_option('blogname'); ?>" />
                        </a>
                    </h1>
                </div>

                <div class="group__item">
                    <div class="menu__container">

                        <button class="btn btn__menu"><i class="fas fa-bars"></i></button>
                        <div class="search__mobile">
                            <button class="btn btn__click__search"><i class="far fa-search"></i></button>
			                <form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="form__search form__search__mobile">
			                	<div class="search__group">
				                    <input type="text" placeholder="Tìm kiếm" name="s" value="<?php echo get_search_query(); ?>" required>
				                    <button class="btn btn__search"><i class="far fa-search"></i></button>
				                </div>
			                </form>
                        </div>

                        <div class="header__menu">
                            <button class="btn btn__back"><i class="fas fa-arrow-left"></i></button>
					        <?php
					            if(function_exists('wp_nav_menu')){
					                $args = array(
					                    'theme_location' => 'primary',
					                    'container_class'=>'container_class',
					                    'menu_class'=>'menu',
					                    'add_li_class'  => 'your-class-name1 your-class-name-2'
					                );
					                wp_nav_menu( $args );
					            }
					        ?>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    
</header>

<?php
	get_template_part("resources/views/header_slide");
?>

