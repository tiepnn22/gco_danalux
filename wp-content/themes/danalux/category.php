<?php get_header(); ?>

<?php
	$category_info = get_category_by_slug( get_query_var( 'category_name' ) );
	$cat_id = $category_info->term_id;
	$cat_name = get_cat_name($cat_id);
	$cat_link = esc_url(get_term_link($cat_id));
?>

<section class="page-detail">
    <div class="container">
        <div class="module module__page-detail">
            <div class="module__header">
                <h2 class="title">
                    <?php echo $cat_name; ?>
                </h2>
            </div>
            <div class="module__content">
                <div class="detail">
                    <div class="page__project">

                        <div class="addon-project__group">

							<?php
								$query = query_post_by_category_paged($cat_id, 12);
								if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

			            		$post_title = get_the_title();
			            		$post_link = get_the_permalink();
			            		$post_image = getPostImage(get_the_ID(),"p-service-news-project");
			            		$post_date = get_the_date('d/m/Y');
			            		$post_excerpt = cut_string(get_the_excerpt(),300,'...');
							?>

	                            <div class="addon__project">
	                                <a href="<?php echo $post_link; ?>" class="addon__project--box">
	                                    <div class="frame">
	                                        <img class="frame--image" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
	                                    </div>
	                                    <h3 class="addon__project--title">
	                                        <?php echo $post_title; ?>
	                                    </h3>
	                                </a>
	                            </div>

                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                        </div>

						<?php
						    $data_navigation = array(
						        'query'     =>    $query,
						        'cat_link'     =>    $cat_link
						    );
						?>
						<?php get_template_part("resources/views/navigation",$data_navigation); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
	
<?php get_footer(); ?>