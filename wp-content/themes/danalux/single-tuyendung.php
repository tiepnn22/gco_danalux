<?php get_header(); ?>

<?php
	//$glb_ctp_tax_project biến toàn cục
	// $terms = wp_get_object_terms($post->ID, $glb_ctp_tax_project);
	// if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];
	// $term_id = $term->term_id;
	// $term_name = $term->name;

    // post_author
    $recent_author = get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
    $post_author = $recent_author->display_name;

    $post_date = get_the_date('m/ Y');

    $data_single_meta = array(
        'post_author'     =>    $post_author, 
        'post_date'    =>    $post_date
    );
    
    $single_recruitment_form = get_field('single_recruitment_form', 'option');
?>

<section class="page-detail">
    <div class="container">
        <div class="module module__page-detail">
            <div class="module__content">
                <div class="detail">

                    <h2 class="detail__title">
                        <?php the_title(); ?>
                    </h2>

                    <?php get_template_part("resources/views/single-meta",$data_single_meta); ?>

                    <p>
                        <?php echo wpautop( the_content() ); ?>
                    </p>

                </div>
            </div>
        </div>
    </div>
</section>


<?php
    $data_recruitment = array(
        'recruitment_form'     =>    $single_recruitment_form
    );
?>
<?php get_template_part("resources/views/form_recruitment",$data_recruitment); ?>


<?php get_footer(); ?>