<?php get_header(); ?>

<?php
	//$glb_ctp_tax_project biến toàn cục
    //$glb_ctp_project biến toàn cục
	$terms_info = get_terms( $glb_ctp_tax_project, array(
		'parent'=> 0,
	    'hide_empty' => false
	) );
	$taxonomy_name = $terms_info[0]->taxonomy;
    $archive_link = get_post_type_archive_link( $glb_ctp_project );
?>

<section class="page-detail">
    <div class="container">
        <div class="module module__page-detail">

            <div class="module__header">
                <h2 class="title">
                    Dự án
                </h2>
            </div>

            <div class="module__content">
                <div class="detail">
                    <div class="page__project">
                        <div class="bs-tab">
                            <div class="tab-container">

                                <div class="tab-control">
                                    <ul class="control-list">

						            	<?php $i = 0; foreach ($terms_info as $terms_info_kq) {
						            		$term_id = $terms_info_kq->term_id;
						            		$term_link = get_term_link(get_term( $term_id ));
						            		$term_name = $terms_info_kq->name;
						            		// $thumbnail_id = get_term_meta( $term_id, 'thumbnail_id', true );
						            		// $term_image = wp_get_attachment_url( $thumbnail_id );
						        		?>
					        			<li class="control-list__item <?php if($i == 0) { echo 'active'; } ?>" tab-show="#tab_<?php echo $term_id; ?>">
					        				<?php echo $term_name; ?>
					        			</li>
                                        <?php $i++; } ?>
                                    </ul>
                                </div>

                                <div class="tab-content">

					            	<?php $i = 0; foreach ($terms_info as $terms_info_kq) {
					            		$term_id = $terms_info_kq->term_id;
					            		$term_link = get_term_link(get_term( $term_id ));
					            		$term_name = $terms_info_kq->name;
					            		// $thumbnail_id = get_term_meta( $term_id, 'thumbnail_id', true );
					            		// $term_image = wp_get_attachment_url( $thumbnail_id );
					        		?>
                                    <div class="tab-item <?php if($i == 0) { echo 'active'; } ?>" id="tab_<?php echo $term_id; ?>">
                                        <div class="addon-project__group">

											<?php
												$query = query_post_by_taxonomy_paged($glb_ctp_project, $taxonomy_name, $term_id, 12);
												if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

							            		$post_title = get_the_title();
							            		$post_link = get_the_permalink();
							            		$post_image = getPostImage(get_the_ID(),"p-service-news-project");
							            		$post_date = 'Tháng '.get_the_date('m/ Y');
							            		$post_excerpt = cut_string(get_the_excerpt(),300,'...');
							            		$post_author = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
											?>
                                            <div class="addon__project">
                                                <a href="<?php echo $post_link; ?>" class="addon__project--box">
                                                    <div class="frame">
                                                        <img class="frame--image" src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                                                    </div>
                                                    <h3 class="addon__project--title">
                                                        <?php echo $post_title; ?>
                                                    </h3>
                                                </a>
                                            </div>
                                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                                        </div>

                                        <?php
                                            $data_navigation = array(
                                                'query'     =>    $query,
                                                'cat_link'     =>    $archive_link
                                            );
                                        ?>
                                        <?php get_template_part("resources/views/navigation",$data_navigation); ?>
						
                                    </div>
                                    <?php $i++; } ?>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>