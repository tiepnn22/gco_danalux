-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 05, 2021 at 10:08 AM
-- Server version: 5.7.30
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gco_danalux`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_actions`
--

CREATE TABLE `wp_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_actionscheduler_actions`
--

INSERT INTO `wp_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(90, 'action_scheduler/migration_hook', 'complete', '2021-03-01 08:09:50', '2021-03-01 08:09:50', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1614586190;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1614586190;}', 1, 1, '2021-03-01 08:10:05', '2021-03-01 15:10:05', 0, NULL),
(91, 'action_scheduler/migration_hook', 'complete', '2021-03-01 08:11:05', '2021-03-01 08:11:05', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1614586265;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1614586265;}', 1, 1, '2021-03-01 08:11:13', '2021-03-01 15:11:13', 0, NULL),
(92, 'wp_mail_smtp_admin_notifications_update', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[1]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 2, 1, '2021-03-01 08:10:55', '2021-03-01 15:10:55', 0, NULL),
(93, 'wp_mail_smtp_admin_notifications_update', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[2]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 2, 1, '2021-03-02 08:31:30', '2021-03-02 15:31:30', 0, NULL),
(94, 'wp_mail_smtp_admin_notifications_update', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[3]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 2, 1, '2021-03-04 01:13:56', '2021-03-04 08:13:56', 0, NULL),
(95, 'action_scheduler/migration_hook', 'complete', '2021-03-04 07:18:24', '2021-03-04 07:18:24', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1614842304;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1614842304;}', 1, 1, '2021-03-04 07:19:56', '2021-03-04 14:19:56', 0, NULL),
(96, 'action_scheduler/migration_hook', 'complete', '2021-03-04 07:20:56', '2021-03-04 07:20:56', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1614842456;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1614842456;}', 1, 1, '2021-03-04 07:21:04', '2021-03-04 14:21:04', 0, NULL),
(97, 'action_scheduler/migration_hook', 'complete', '2021-03-04 07:22:15', '2021-03-04 07:22:15', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1614842535;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1614842535;}', 1, 1, '2021-03-04 07:22:29', '2021-03-04 14:22:29', 0, NULL),
(98, 'action_scheduler/migration_hook', 'pending', '2021-03-04 08:04:00', '2021-03-04 08:04:00', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1614845040;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1614845040;}', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_claims`
--

CREATE TABLE `wp_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_groups`
--

CREATE TABLE `wp_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_actionscheduler_groups`
--

INSERT INTO `wp_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration'),
(2, 'wp_mail_smtp');

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_logs`
--

CREATE TABLE `wp_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_actionscheduler_logs`
--

INSERT INTO `wp_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 90, 'action created', '2021-03-01 08:08:50', '2021-03-01 08:08:50'),
(2, 90, 'action started via WP Cron', '2021-03-01 08:10:05', '2021-03-01 08:10:05'),
(3, 90, 'action complete via WP Cron', '2021-03-01 08:10:05', '2021-03-01 08:10:05'),
(4, 91, 'action created', '2021-03-01 08:10:05', '2021-03-01 08:10:05'),
(5, 92, 'action created', '2021-03-01 08:10:42', '2021-03-01 08:10:42'),
(6, 92, 'action started via WP Cron', '2021-03-01 08:10:55', '2021-03-01 08:10:55'),
(7, 92, 'action complete via WP Cron', '2021-03-01 08:10:55', '2021-03-01 08:10:55'),
(8, 91, 'action started via Async Request', '2021-03-01 08:11:13', '2021-03-01 08:11:13'),
(9, 91, 'action complete via Async Request', '2021-03-01 08:11:13', '2021-03-01 08:11:13'),
(10, 93, 'action created', '2021-03-02 08:31:28', '2021-03-02 08:31:28'),
(11, 93, 'action started via Async Request', '2021-03-02 08:31:29', '2021-03-02 08:31:29'),
(12, 93, 'action complete via Async Request', '2021-03-02 08:31:30', '2021-03-02 08:31:30'),
(13, 94, 'action created', '2021-03-04 01:13:55', '2021-03-04 01:13:55'),
(14, 94, 'action started via Async Request', '2021-03-04 01:13:56', '2021-03-04 01:13:56'),
(15, 94, 'action complete via Async Request', '2021-03-04 01:13:56', '2021-03-04 01:13:56'),
(16, 95, 'action created', '2021-03-04 07:17:24', '2021-03-04 07:17:24'),
(17, 95, 'action started via WP Cron', '2021-03-04 07:19:56', '2021-03-04 07:19:56'),
(18, 95, 'action complete via WP Cron', '2021-03-04 07:19:56', '2021-03-04 07:19:56'),
(19, 96, 'action created', '2021-03-04 07:19:56', '2021-03-04 07:19:56'),
(20, 96, 'action started via Async Request', '2021-03-04 07:21:04', '2021-03-04 07:21:04'),
(21, 96, 'action complete via Async Request', '2021-03-04 07:21:04', '2021-03-04 07:21:04'),
(22, 97, 'action created', '2021-03-04 07:21:15', '2021-03-04 07:21:15'),
(23, 97, 'action started via Async Request', '2021-03-04 07:22:29', '2021-03-04 07:22:29'),
(24, 97, 'action complete via Async Request', '2021-03-04 07:22:29', '2021-03-04 07:22:29'),
(25, 98, 'action created', '2021-03-04 08:03:00', '2021-03-04 08:03:00');

-- --------------------------------------------------------

--
-- Table structure for table `wp_cf7_vdata`
--

CREATE TABLE `wp_cf7_vdata` (
  `id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_cf7_vdata`
--

INSERT INTO `wp_cf7_vdata` (`id`, `created`) VALUES
(1, '2021-03-04 01:30:06'),
(2, '2021-03-04 02:15:08'),
(3, '2021-03-04 02:15:29'),
(4, '2021-03-04 02:16:44'),
(5, '2021-03-04 02:18:50'),
(6, '2021-03-04 02:20:03'),
(7, '2021-03-04 02:21:09'),
(8, '2021-03-04 02:21:45'),
(9, '2021-03-04 02:23:13'),
(10, '2021-03-04 21:39:20'),
(11, '2021-03-04 21:45:07'),
(12, '2021-03-04 23:51:38'),
(13, '2021-03-05 00:59:25');

-- --------------------------------------------------------

--
-- Table structure for table `wp_cf7_vdata_entry`
--

CREATE TABLE `wp_cf7_vdata_entry` (
  `id` int(11) NOT NULL,
  `cf7_id` int(11) NOT NULL,
  `data_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_cf7_vdata_entry`
--

INSERT INTO `wp_cf7_vdata_entry` (`id`, `cf7_id`, `data_id`, `name`, `value`) VALUES
(1, 203, 1, 'your-name', 'tiep nguyen'),
(2, 203, 1, 'your-email', 'tiepnguyen220194@gmail.com'),
(3, 203, 1, 'your-subject', 'aaa'),
(4, 203, 1, 'your-message', ''),
(5, 203, 1, 'file-562', 'http://danalux.local/wp-content/uploads/advanced-cf7-upload/4.jpg'),
(6, 203, 1, 'submit_time', '2021-03-04 08:30:06'),
(7, 203, 1, 'submit_ip', '127.0.0.1'),
(8, 179, 2, 'your-name', 'tiep nguyen'),
(9, 179, 2, 'your-email', 'tiepnguyen220194@gmail.com'),
(10, 179, 2, 'your-tel', '0359117301'),
(11, 179, 2, 'your-select', 'Thực tập sinh 2'),
(12, 179, 2, 'your-message', '222'),
(13, 179, 2, 'your-file', 'http://danalux.local/wp-content/uploads/advanced-cf7-upload/TienhoavaTaohoa.pdf'),
(14, 179, 2, 'submit_time', '2021-03-04 09:15:07'),
(15, 179, 2, 'submit_ip', '127.0.0.1'),
(16, 179, 3, 'your-name', 'tiep nguyen'),
(17, 179, 3, 'your-email', 'tiepnguyen220194@gmail.com'),
(18, 179, 3, 'your-tel', '0359117301'),
(19, 179, 3, 'your-select', 'Thực tập sinh 2'),
(20, 179, 3, 'your-message', '22'),
(21, 179, 3, 'your-file', 'http://danalux.local/wp-content/uploads/advanced-cf7-upload/TienhoavaTaohoa-1.pdf'),
(22, 179, 3, 'submit_time', '2021-03-04 09:15:29'),
(23, 179, 3, 'submit_ip', '127.0.0.1'),
(24, 179, 4, 'your-name', 'tiep nguyen'),
(25, 179, 4, 'your-email', 'tiepnguyen220194@gmail.com'),
(26, 179, 4, 'your-tel', '0359117301'),
(27, 179, 4, 'your-select', 'Thực tập sinh 2'),
(28, 179, 4, 'your-message', '22'),
(29, 179, 4, 'your-file', 'http://danalux.local/wp-content/uploads/advanced-cf7-upload/TienhoavaTaohoa-2.pdf'),
(30, 179, 4, 'submit_time', '2021-03-04 09:16:44'),
(31, 179, 4, 'submit_ip', '127.0.0.1'),
(32, 179, 5, 'your-name', 'tiep'),
(33, 179, 5, 'your-email', 'tiepnguyen220194@gmail.com'),
(34, 179, 5, 'your-tel', '0359117301'),
(35, 179, 5, 'your-select', 'Thực tập sinh 1'),
(36, 179, 5, 'your-message', 'kk'),
(37, 179, 5, 'your-file', 'http://danalux.local/wp-content/uploads/advanced-cf7-upload/TienhoavaTaohoa-3.pdf'),
(38, 179, 5, 'submit_time', '2021-03-04 09:18:50'),
(39, 179, 5, 'submit_ip', '127.0.0.1'),
(40, 179, 6, 'your-name', 'tiep nguyen'),
(41, 179, 6, 'your-email', 'tiepnguyen220194@gmail.com'),
(42, 179, 6, 'your-tel', '0359117301'),
(43, 179, 6, 'your-select', 'Thực tập sinh 2'),
(44, 179, 6, 'your-message', 'p'),
(45, 179, 6, 'your-file', 'http://danalux.local/wp-content/uploads/advanced-cf7-upload/TienhoavaTaohoa-4.pdf'),
(46, 179, 6, 'submit_time', '2021-03-04 09:20:03'),
(47, 179, 6, 'submit_ip', '127.0.0.1'),
(48, 179, 7, 'your-name', 'tiep nguyen'),
(49, 179, 7, 'your-email', 'tiepnguyen220194@gmail.com'),
(50, 179, 7, 'your-tel', '0359117301'),
(51, 179, 7, 'your-select', 'Thực tập sinh 2'),
(52, 179, 7, 'your-message', '2'),
(53, 179, 7, 'your-file', 'http://danalux.local/wp-content/uploads/advanced-cf7-upload/TienhoavaTaohoa-5.pdf'),
(54, 179, 7, 'submit_time', '2021-03-04 09:21:09'),
(55, 179, 7, 'submit_ip', '127.0.0.1'),
(56, 179, 8, 'your-name', 'tiep nguyen'),
(57, 179, 8, 'your-email', 'tiepnguyen220194@gmail.com'),
(58, 179, 8, 'your-tel', '0359117301'),
(59, 179, 8, 'your-select', 'Thực tập sinh 2'),
(60, 179, 8, 'your-message', '22'),
(61, 179, 8, 'your-file', 'http://danalux.local/wp-content/uploads/advanced-cf7-upload/TienhoavaTaohoa-6.pdf'),
(62, 179, 8, 'submit_time', '2021-03-04 09:21:45'),
(63, 179, 8, 'submit_ip', '127.0.0.1'),
(64, 179, 9, 'your-name', 'tiep nguyen'),
(65, 179, 9, 'your-email', 'tiepnguyen220194@gmail.com'),
(66, 179, 9, 'your-tel', '0359117301'),
(67, 179, 9, 'your-select', 'Thực tập sinh 2'),
(68, 179, 9, 'your-message', '11'),
(69, 179, 9, 'your-file', 'http://danalux.local/wp-content/uploads/advanced-cf7-upload/TienhoavaTaohoa-7.pdf'),
(70, 179, 9, 'submit_time', '2021-03-04 09:23:13'),
(71, 179, 9, 'submit_ip', '127.0.0.1'),
(72, 179, 10, 'your-name', 'tiep nguyen'),
(73, 179, 10, 'your-email', 'tiepnguyen220194@gmail.com'),
(74, 179, 10, 'your-tel', '0359117301'),
(75, 179, 10, 'your-select', 'Thực tập sinh 2'),
(76, 179, 10, 'your-message', '11'),
(77, 179, 10, 'your-date', '2021-03-05 11:38:49'),
(78, 179, 10, 'your-file', 'http://danalux.local/wp-content/uploads/advanced-cf7-upload/TienhoavaTaohoa-8.pdf'),
(79, 179, 10, 'submit_time', '2021-03-05 04:39:20'),
(80, 179, 10, 'submit_ip', '127.0.0.1'),
(81, 12, 11, 'your-name', 'tiep nguyen'),
(82, 12, 11, 'your-email', 'tiepnguyen220194@gmail.com'),
(83, 12, 11, 'your-tel', '0359117301'),
(84, 12, 11, 'your-message', '11'),
(85, 12, 11, 'your-date', '2021-03-05 11:44:18'),
(86, 12, 11, 'submit_time', '2021-03-05 04:45:07'),
(87, 12, 11, 'submit_ip', '127.0.0.1'),
(88, 179, 12, 'your-name', 'tiep nguyen'),
(89, 179, 12, 'your-email', 'tiepnguyen220194@gmail.com'),
(90, 179, 12, 'your-tel', '0359117301'),
(91, 179, 12, 'your-select', 'Thực tập sinh 2'),
(92, 179, 12, 'your-message', ',,,'),
(93, 179, 12, 'your-date', '2021-03-05 13:51:19'),
(94, 179, 12, 'your-file', 'http://danalux.local/wp-content/uploads/advanced-cf7-upload/TienhoavaTaohoa-9.pdf'),
(95, 179, 12, 'submit_time', '2021-03-05 06:51:38'),
(96, 179, 12, 'submit_ip', '127.0.0.1'),
(97, 179, 13, 'your-name', 'tiep'),
(98, 179, 13, 'your-email', 'tiepn20194@gmail.com'),
(99, 179, 13, 'your-tel', '0359117301'),
(100, 179, 13, 'your-select', 'Thực tập sinh 1'),
(101, 179, 13, 'your-message', 'ee'),
(102, 179, 13, 'your-date', '2021-03-05 14:59:09'),
(103, 179, 13, 'your-file', 'http://danalux.local/wp-content/uploads/advanced-cf7-upload/TienhoavaTaohoa-10.pdf'),
(104, 179, 13, 'submit_time', '2021-03-05 07:59:25'),
(105, 179, 13, 'submit_ip', '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://danalux.local', 'yes'),
(2, 'home', 'http://danalux.local', 'yes'),
(3, 'blogname', 'Danalux', 'yes'),
(4, 'blogdescription', 'Tư vấn &amp; đầu tư', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'tiepnguyen220194@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '1', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '1', 'yes'),
(23, 'date_format', 'd/m/Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%category%/%postname%.html', 'yes'),
(29, 'rewrite_rules', 'a:179:{s:9:\"dichvu/?$\";s:26:\"index.php?post_type=dichvu\";s:39:\"dichvu/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=dichvu&feed=$matches[1]\";s:34:\"dichvu/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=dichvu&feed=$matches[1]\";s:26:\"dichvu/page/([0-9]{1,})/?$\";s:44:\"index.php?post_type=dichvu&paged=$matches[1]\";s:7:\"duan/?$\";s:24:\"index.php?post_type=duan\";s:37:\"duan/feed/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=duan&feed=$matches[1]\";s:32:\"duan/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=duan&feed=$matches[1]\";s:24:\"duan/page/([0-9]{1,})/?$\";s:42:\"index.php?post_type=duan&paged=$matches[1]\";s:12:\"tuyendung/?$\";s:29:\"index.php?post_type=tuyendung\";s:42:\"tuyendung/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=tuyendung&feed=$matches[1]\";s:37:\"tuyendung/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=tuyendung&feed=$matches[1]\";s:29:\"tuyendung/page/([0-9]{1,})/?$\";s:47:\"index.php?post_type=tuyendung&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:34:\"dichvu/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"dichvu/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"dichvu/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"dichvu/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"dichvu/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"dichvu/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"dichvu/([^/]+)/embed/?$\";s:39:\"index.php?dichvu=$matches[1]&embed=true\";s:27:\"dichvu/([^/]+)/trackback/?$\";s:33:\"index.php?dichvu=$matches[1]&tb=1\";s:47:\"dichvu/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?dichvu=$matches[1]&feed=$matches[2]\";s:42:\"dichvu/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?dichvu=$matches[1]&feed=$matches[2]\";s:35:\"dichvu/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?dichvu=$matches[1]&paged=$matches[2]\";s:42:\"dichvu/([^/]+)/comment-page-([0-9]{1,})/?$\";s:46:\"index.php?dichvu=$matches[1]&cpage=$matches[2]\";s:31:\"dichvu/([^/]+)(?:/([0-9]+))?/?$\";s:45:\"index.php?dichvu=$matches[1]&page=$matches[2]\";s:23:\"dichvu/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:33:\"dichvu/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:53:\"dichvu/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"dichvu/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"dichvu/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"dichvu/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:32:\"duan/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"duan/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"duan/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"duan/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"duan/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"duan/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:21:\"duan/([^/]+)/embed/?$\";s:37:\"index.php?duan=$matches[1]&embed=true\";s:25:\"duan/([^/]+)/trackback/?$\";s:31:\"index.php?duan=$matches[1]&tb=1\";s:45:\"duan/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?duan=$matches[1]&feed=$matches[2]\";s:40:\"duan/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?duan=$matches[1]&feed=$matches[2]\";s:33:\"duan/([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?duan=$matches[1]&paged=$matches[2]\";s:40:\"duan/([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?duan=$matches[1]&cpage=$matches[2]\";s:29:\"duan/([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?duan=$matches[1]&page=$matches[2]\";s:21:\"duan/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:31:\"duan/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:51:\"duan/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"duan/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"duan/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:27:\"duan/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:37:\"tuyendung/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"tuyendung/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"tuyendung/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"tuyendung/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"tuyendung/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"tuyendung/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"tuyendung/([^/]+)/embed/?$\";s:42:\"index.php?tuyendung=$matches[1]&embed=true\";s:30:\"tuyendung/([^/]+)/trackback/?$\";s:36:\"index.php?tuyendung=$matches[1]&tb=1\";s:50:\"tuyendung/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?tuyendung=$matches[1]&feed=$matches[2]\";s:45:\"tuyendung/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?tuyendung=$matches[1]&feed=$matches[2]\";s:38:\"tuyendung/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?tuyendung=$matches[1]&paged=$matches[2]\";s:45:\"tuyendung/([^/]+)/comment-page-([0-9]{1,})/?$\";s:49:\"index.php?tuyendung=$matches[1]&cpage=$matches[2]\";s:34:\"tuyendung/([^/]+)(?:/([0-9]+))?/?$\";s:48:\"index.php?tuyendung=$matches[1]&page=$matches[2]\";s:26:\"tuyendung/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:36:\"tuyendung/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:56:\"tuyendung/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"tuyendung/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"tuyendung/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:32:\"tuyendung/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:55:\"danhmuc-dichvu/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?danhmuc-dichvu=$matches[1]&feed=$matches[2]\";s:50:\"danhmuc-dichvu/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?danhmuc-dichvu=$matches[1]&feed=$matches[2]\";s:31:\"danhmuc-dichvu/([^/]+)/embed/?$\";s:47:\"index.php?danhmuc-dichvu=$matches[1]&embed=true\";s:43:\"danhmuc-dichvu/([^/]+)/page/?([0-9]{1,})/?$\";s:54:\"index.php?danhmuc-dichvu=$matches[1]&paged=$matches[2]\";s:25:\"danhmuc-dichvu/([^/]+)/?$\";s:36:\"index.php?danhmuc-dichvu=$matches[1]\";s:53:\"danhmuc-duan/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?danhmuc-duan=$matches[1]&feed=$matches[2]\";s:48:\"danhmuc-duan/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?danhmuc-duan=$matches[1]&feed=$matches[2]\";s:29:\"danhmuc-duan/([^/]+)/embed/?$\";s:45:\"index.php?danhmuc-duan=$matches[1]&embed=true\";s:41:\"danhmuc-duan/([^/]+)/page/?([0-9]{1,})/?$\";s:52:\"index.php?danhmuc-duan=$matches[1]&paged=$matches[2]\";s:23:\"danhmuc-duan/([^/]+)/?$\";s:34:\"index.php?danhmuc-duan=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=21&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:36:\".+?/[^/]+.html/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\".+?/[^/]+.html/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\".+?/[^/]+.html/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\".+?/[^/]+.html/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\".+?/[^/]+.html/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\".+?/[^/]+.html/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:27:\"(.+?)/([^/]+).html/embed/?$\";s:63:\"index.php?category_name=$matches[1]&name=$matches[2]&embed=true\";s:31:\"(.+?)/([^/]+).html/trackback/?$\";s:57:\"index.php?category_name=$matches[1]&name=$matches[2]&tb=1\";s:51:\"(.+?)/([^/]+).html/feed/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&feed=$matches[3]\";s:46:\"(.+?)/([^/]+).html/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&feed=$matches[3]\";s:39:\"(.+?)/([^/]+).html/page/?([0-9]{1,})/?$\";s:70:\"index.php?category_name=$matches[1]&name=$matches[2]&paged=$matches[3]\";s:46:\"(.+?)/([^/]+).html/comment-page-([0-9]{1,})/?$\";s:70:\"index.php?category_name=$matches[1]&name=$matches[2]&cpage=$matches[3]\";s:35:\"(.+?)/([^/]+).html(?:/([0-9]+))?/?$\";s:69:\"index.php?category_name=$matches[1]&name=$matches[2]&page=$matches[3]\";s:25:\".+?/[^/]+.html/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\".+?/[^/]+.html/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\".+?/[^/]+.html/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\".+?/[^/]+.html/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\".+?/[^/]+.html/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\".+?/[^/]+.html/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:38:\"(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:33:\"(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:14:\"(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:26:\"(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:33:\"(.+?)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&cpage=$matches[2]\";s:8:\"(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:35:\"advanced-cf7-db/advanced-cf7-db.php\";i:1;s:34:\"advanced-custom-fields-pro/acf.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:27:\"wp-pagenavi/wp-pagenavi.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '7', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'danalux', 'yes'),
(41, 'stylesheet', 'danalux', 'yes'),
(42, 'comment_registration', '0', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '49752', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '0', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'page', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '0', 'yes'),
(57, 'thumbnail_size_h', '0', 'yes'),
(58, 'thumbnail_crop', '', 'yes'),
(59, 'medium_size_w', '0', 'yes'),
(60, 'medium_size_h', '0', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '0', 'yes'),
(63, 'large_size_h', '0', 'yes'),
(64, 'image_default_link_type', '', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '0', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '0', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(77, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(78, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'uninstall_plugins', 'a:1:{s:27:\"wp-pagenavi/wp-pagenavi.php\";s:14:\"__return_false\";}', 'no'),
(80, 'timezone_string', '', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '21', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '9', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1629951667', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'auto_update_core_dev', 'enabled', 'yes'),
(96, 'auto_update_core_minor', 'enabled', 'yes'),
(97, 'auto_update_core_major', 'enabled', 'yes'),
(98, 'initial_db_version', '49752', 'yes'),
(99, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:67:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"cf7_db_form_view12\";b:1;s:19:\"cf7_db_form_edit_12\";b:1;s:19:\"cf7_db_form_view179\";b:1;s:20:\"cf7_db_form_edit_179\";b:1;s:19:\"cf7_db_form_view203\";b:1;s:20:\"cf7_db_form_edit_203\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(100, 'fresh_site', '0', 'yes'),
(101, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'sidebars_widgets', 'a:4:{s:7:\"adv-ads\";a:1:{i:0;s:20:\"show_post_taxonomy-2\";}s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:15:\"sidebar-service\";a:1:{i:0;s:20:\"show_post_taxonomy-3\";}s:13:\"array_version\";i:3;}', 'yes'),
(107, 'cron', 'a:7:{i:1614939685;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1614961285;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1615004485;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1615004953;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1615004956;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1615090885;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(108, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(116, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(118, 'recovery_keys', 'a:1:{s:22:\"ozgaSgTkBrvDD1i3ow4r7x\";a:2:{s:10:\"hashed_key\";s:34:\"$P$Bgbi5S4YE2NAaO534eXIsTaBPCrnk31\";s:10:\"created_at\";i:1614846352;}}', 'yes'),
(119, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.6.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.6.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.6.2-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.6.2\";s:7:\"version\";s:5:\"5.6.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1614400114;s:15:\"version_checked\";s:5:\"5.6.2\";s:12:\"translations\";a:0:{}}', 'no'),
(124, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1614918091;s:7:\"checked\";a:1:{s:7:\"danalux\";s:3:\"1.0\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(125, '_site_transient_timeout_browser_e82e8dd7ed2a8c38b0a45e033d2582b6', '1615004954', 'no'),
(126, '_site_transient_browser_e82e8dd7ed2a8c38b0a45e033d2582b6', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"88.0.4324.192\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(127, '_site_transient_timeout_php_check_e26e33de4a278e301580d402dcb3d659', '1615004955', 'no'),
(128, '_site_transient_php_check_e26e33de4a278e301580d402dcb3d659', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(130, 'can_compress_scripts', '1', 'no'),
(147, 'finished_updating_comment_type', '1', 'yes'),
(148, 'WPLANG', 'vi', 'yes'),
(149, 'new_admin_email', 'tiepnguyen220194@gmail.com', 'yes'),
(160, 'recently_activated', 'a:3:{s:29:\"wp-mail-smtp/wp_mail_smtp.php\";i:1614844996;s:37:\"tinymce-advanced/tinymce-advanced.php\";i:1614844979;s:33:\"admin-menu-editor/menu-editor.php\";i:1614842475;}', 'yes'),
(161, 'theme_mods_twentytwentyone', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1614400366;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(162, 'current_theme', 'Danalux', 'yes'),
(163, 'theme_mods_danalux', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:6;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(164, 'theme_switched', '', 'yes'),
(179, 'acf_version', '5.9.3', 'yes'),
(183, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.3.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1614403827;s:7:\"version\";s:3:\"5.4\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(184, 'pagenavi_options', 'a:15:{s:10:\"pages_text\";s:13:\"%TOTAL_PAGES%\";s:12:\"current_text\";s:13:\"%PAGE_NUMBER%\";s:9:\"page_text\";s:13:\"%PAGE_NUMBER%\";s:10:\"first_text\";s:2:\"«\";s:9:\"last_text\";s:2:\"»\";s:9:\"prev_text\";s:53:\"<i class=\"fal fa-long-arrow-left\"></i> Trang trước\";s:9:\"next_text\";s:57:\"Trang tiếp theo <i class=\"fal fa-long-arrow-right\"></i>\";s:12:\"dotleft_text\";s:3:\"...\";s:13:\"dotright_text\";s:3:\"...\";s:9:\"num_pages\";i:5;s:23:\"num_larger_page_numbers\";i:3;s:28:\"larger_page_numbers_multiple\";i:10;s:11:\"always_show\";i:0;s:16:\"use_pagenavi_css\";i:1;s:5:\"style\";i:1;}', 'yes'),
(185, 'vsz_cf7_db_version', '2.0.0', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(188, 'ws_menu_editor', 'a:26:{s:22:\"hide_advanced_settings\";b:1;s:16:\"show_extra_icons\";b:0;s:11:\"custom_menu\";a:6:{s:4:\"tree\";a:18:{s:9:\"index.php\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:10:\">index.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:2:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:19:\"index.php>index.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:11:\"Trang chủ\";s:12:\"access_level\";s:4:\"read\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:9:\"index.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:9:\"index.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:9:\"index.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:1;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:25:\"index.php>update-core.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:92:\"Cập nhật <span class=\"update-plugins count-0\"><span class=\"update-count\">0</span></span>\";s:12:\"access_level\";s:11:\"update_core\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:15:\"update-core.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:9:\"index.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:15:\"update-core.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:10:\"Bảng tin\";s:12:\"access_level\";s:4:\"read\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:9:\"index.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:43:\"menu-top menu-top-first menu-icon-dashboard\";s:8:\"hookname\";s:14:\"menu-dashboard\";s:8:\"icon_url\";s:19:\"dashicons-dashboard\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:9:\"index.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:11:\"separator_6\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:1;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:12:\">separator_6\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:0:\"\";s:12:\"access_level\";s:4:\"read\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:11:\"separator_6\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:17:\"wp-menu-separator\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:1;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:0:\"\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:8:\"edit.php\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:2;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:9:\">edit.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:4:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:17:\"edit.php>edit.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:22:\"Tất cả bài viết\";s:12:\"access_level\";s:10:\"edit_posts\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:8:\"edit.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:8:\"edit.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:8:\"edit.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:1;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:21:\"edit.php>post-new.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:17:\"Viết bài mới\";s:12:\"access_level\";s:10:\"edit_posts\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:12:\"post-new.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:8:\"edit.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:12:\"post-new.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:2;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:2;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:40:\"edit.php>edit-tags.php?taxonomy=category\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:13:\"Chuyên mục\";s:12:\"access_level\";s:17:\"manage_categories\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:31:\"edit-tags.php?taxonomy=category\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:8:\"edit.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:31:\"edit-tags.php?taxonomy=category\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:3;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:3;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:40:\"edit.php>edit-tags.php?taxonomy=post_tag\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:5:\"Thẻ\";s:12:\"access_level\";s:16:\"manage_post_tags\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:31:\"edit-tags.php?taxonomy=post_tag\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:8:\"edit.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:31:\"edit-tags.php?taxonomy=post_tag\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:11:\"Bài viết\";s:12:\"access_level\";s:10:\"edit_posts\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:8:\"edit.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:37:\"menu-top menu-icon-post open-if-no-js\";s:8:\"hookname\";s:10:\"menu-posts\";s:8:\"icon_url\";s:20:\"dashicons-admin-post\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:8:\"edit.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:10:\"upload.php\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:3;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:11:\">upload.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:2:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:21:\"upload.php>upload.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:11:\"Thư viện\";s:12:\"access_level\";s:12:\"upload_files\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:10:\"upload.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:10:\"upload.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:10:\"upload.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:1;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:24:\"upload.php>media-new.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:10:\"Tải lên\";s:12:\"access_level\";s:12:\"upload_files\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:13:\"media-new.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:10:\"upload.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:13:\"media-new.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:5:\"Media\";s:12:\"access_level\";s:12:\"upload_files\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:10:\"upload.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:24:\"menu-top menu-icon-media\";s:8:\"hookname\";s:10:\"menu-media\";s:8:\"icon_url\";s:21:\"dashicons-admin-media\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:10:\"upload.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:23:\"edit.php?post_type=page\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:4;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:24:\">edit.php?post_type=page\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:2:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:47:\"edit.php?post_type=page>edit.php?post_type=page\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:21:\"Tất cả các trang\";s:12:\"access_level\";s:10:\"edit_pages\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:23:\"edit.php?post_type=page\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:23:\"edit.php?post_type=page\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:23:\"edit.php?post_type=page\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:1;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:51:\"edit.php?post_type=page>post-new.php?post_type=page\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:17:\"Thêm trang mới\";s:12:\"access_level\";s:10:\"edit_pages\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:27:\"post-new.php?post_type=page\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:23:\"edit.php?post_type=page\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:27:\"post-new.php?post_type=page\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:5:\"Trang\";s:12:\"access_level\";s:10:\"edit_pages\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:23:\"edit.php?post_type=page\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:23:\"menu-top menu-icon-page\";s:8:\"hookname\";s:10:\"menu-pages\";s:8:\"icon_url\";s:20:\"dashicons-admin-page\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:23:\"edit.php?post_type=page\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:17:\"edit-comments.php\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:5;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:18:\">edit-comments.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:1:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:35:\"edit-comments.php>edit-comments.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:1;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:23:\"Tất cả phản hồi\";s:12:\"access_level\";s:10:\"edit_posts\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:17:\"edit-comments.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:17:\"edit-comments.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:17:\"edit-comments.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:1;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:213:\"Phản hồi <span class=\"awaiting-mod count-0\"><span class=\"pending-count\" aria-hidden=\"true\">0</span><span class=\"comments-in-moderation-text screen-reader-text\">0 bình luận cần kiểm duyệt</span></span>\";s:12:\"access_level\";s:10:\"edit_posts\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:17:\"edit-comments.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:27:\"menu-top menu-icon-comments\";s:8:\"hookname\";s:13:\"menu-comments\";s:8:\"icon_url\";s:24:\"dashicons-admin-comments\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:17:\"edit-comments.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:27:\"edit.php?post_type=services\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:6;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:28:\">edit.php?post_type=services\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:3:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:55:\"edit.php?post_type=services>edit.php?post_type=services\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:15:\"All Dịch vụ\";s:12:\"access_level\";s:10:\"edit_pages\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:27:\"edit.php?post_type=services\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:27:\"edit.php?post_type=services\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:27:\"edit.php?post_type=services\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:1;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:59:\"edit.php?post_type=services>post-new.php?post_type=services\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:7:\"Add New\";s:12:\"access_level\";s:10:\"edit_pages\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:31:\"post-new.php?post_type=services\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:27:\"edit.php?post_type=services\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:31:\"post-new.php?post_type=services\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:2;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:2;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:86:\"edit.php?post_type=services>edit-tags.php?taxonomy=services-cat&amp;post_type=services\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:22:\"Danh mục Dịch vụ\";s:12:\"access_level\";s:17:\"manage_categories\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:58:\"edit-tags.php?taxonomy=services-cat&amp;post_type=services\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:27:\"edit.php?post_type=services\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:54:\"edit-tags.php?taxonomy=services-cat&post_type=services\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:11:\"Dịch vụ\";s:12:\"access_level\";s:10:\"edit_pages\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:27:\"edit.php?post_type=services\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:27:\"menu-top menu-icon-services\";s:8:\"hookname\";s:19:\"menu-posts-services\";s:8:\"icon_url\";s:20:\"dashicons-admin-post\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:27:\"edit.php?post_type=services\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:5:\"wpcf7\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:7;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:6:\">wpcf7\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:3:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:11:\"wpcf7>wpcf7\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:17:\"Edit Contact Form\";s:10:\"menu_title\";s:13:\"Contact Forms\";s:12:\"access_level\";s:24:\"wpcf7_read_contact_forms\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:5:\"wpcf7\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:5:\"wpcf7\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:20:\"admin.php?page=wpcf7\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:1;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:15:\"wpcf7>wpcf7-new\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:20:\"Add New Contact Form\";s:10:\"menu_title\";s:7:\"Add New\";s:12:\"access_level\";s:24:\"wpcf7_edit_contact_forms\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:9:\"wpcf7-new\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:5:\"wpcf7\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:24:\"admin.php?page=wpcf7-new\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:2;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:2;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:23:\"wpcf7>wpcf7-integration\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:29:\"Integration with External API\";s:10:\"menu_title\";s:11:\"Integration\";s:12:\"access_level\";s:24:\"wpcf7_manage_integration\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:17:\"wpcf7-integration\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:5:\"wpcf7\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:32:\"admin.php?page=wpcf7-integration\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:14:\"Contact Form 7\";s:10:\"menu_title\";s:7:\"Contact\";s:12:\"access_level\";s:24:\"wpcf7_read_contact_forms\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:5:\"wpcf7\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:28:\"menu-top toplevel_page_wpcf7\";s:8:\"hookname\";s:19:\"toplevel_page_wpcf7\";s:8:\"icon_url\";s:15:\"dashicons-email\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:20:\"admin.php?page=wpcf7\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:20:\"contact-form-listing\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:8;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:21:\">contact-form-listing\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:5:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:41:\"contact-form-listing>contact-form-listing\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:15:\"Advanced CF7 DB\";s:10:\"menu_title\";s:15:\"Advanced CF7 DB\";s:12:\"access_level\";s:5:\"exist\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:20:\"contact-form-listing\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:20:\"contact-form-listing\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:35:\"admin.php?page=contact-form-listing\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:1;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:35:\"contact-form-listing>import_cf7_csv\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:10:\"Import CSV\";s:10:\"menu_title\";s:10:\"Import CSV\";s:12:\"access_level\";s:5:\"exist\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:14:\"import_cf7_csv\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:20:\"contact-form-listing\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:29:\"admin.php?page=import_cf7_csv\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:2;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:2;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:30:\"contact-form-listing>shortcode\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:17:\"Developer Support\";s:10:\"menu_title\";s:17:\"Developer Support\";s:12:\"access_level\";s:5:\"exist\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:9:\"shortcode\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:20:\"contact-form-listing\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:24:\"admin.php?page=shortcode\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:3;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:3;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:31:\"contact-form-listing>extentions\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:7:\"Add-ons\";s:10:\"menu_title\";s:7:\"Add-ons\";s:12:\"access_level\";s:5:\"exist\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:10:\"extentions\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:20:\"contact-form-listing\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:25:\"admin.php?page=extentions\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:4;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:4;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:35:\"contact-form-listing>mounstride-CRM\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:14:\"mounstride CRM\";s:10:\"menu_title\";s:14:\"mounstride CRM\";s:12:\"access_level\";s:5:\"exist\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:14:\"mounstride-CRM\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:20:\"contact-form-listing\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:29:\"admin.php?page=mounstride-CRM\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:15:\"Advanced CF7 DB\";s:10:\"menu_title\";s:15:\"Advanced CF7 DB\";s:12:\"access_level\";s:5:\"exist\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:20:\"contact-form-listing\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:43:\"menu-top toplevel_page_contact-form-listing\";s:8:\"hookname\";s:34:\"toplevel_page_contact-form-listing\";s:8:\"icon_url\";s:20:\"dashicons-visibility\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:35:\"admin.php?page=contact-form-listing\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:11:\"separator_7\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:9;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:1;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:12:\">separator_7\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:0:\"\";s:12:\"access_level\";s:4:\"read\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:11:\"separator_7\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:17:\"wp-menu-separator\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:1;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:0:\"\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:10:\"themes.php\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:10;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:11:\">themes.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:5:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:21:\"themes.php>themes.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:11:\"Giao diện\";s:12:\"access_level\";s:13:\"switch_themes\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:10:\"themes.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:10:\"themes.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:10:\"themes.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:1;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:24:\"themes.php>customize.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:11:\"Tùy biến\";s:12:\"access_level\";s:9:\"customize\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:75:\"customize.php?return=%2Fwp-admin%2Foptions-general.php%3Fpage%3Dmenu_editor\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:10:\"themes.php\";s:9:\"css_class\";s:20:\"hide-if-no-customize\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:75:\"customize.php?return=%2Fwp-admin%2Foptions-general.php%3Fpage%3Dmenu_editor\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:2;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:2;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:22:\"themes.php>widgets.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:6:\"Widget\";s:12:\"access_level\";s:18:\"edit_theme_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:11:\"widgets.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:10:\"themes.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:11:\"widgets.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:3;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:3;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:24:\"themes.php>nav-menus.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:4:\"Menu\";s:12:\"access_level\";s:18:\"edit_theme_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:13:\"nav-menus.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:10:\"themes.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:13:\"nav-menus.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:4;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:4;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:27:\"themes.php>theme-editor.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:17:\"Sửa giao diện\";s:10:\"menu_title\";s:17:\"Sửa giao diện\";s:12:\"access_level\";s:11:\"edit_themes\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:16:\"theme-editor.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:10:\"themes.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:16:\"theme-editor.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:11:\"Giao diện\";s:12:\"access_level\";s:13:\"switch_themes\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:10:\"themes.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:29:\"menu-top menu-icon-appearance\";s:8:\"hookname\";s:15:\"menu-appearance\";s:8:\"icon_url\";s:26:\"dashicons-admin-appearance\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:10:\"themes.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:11:\"plugins.php\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:11;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:12:\">plugins.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:3:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:23:\"plugins.php>plugins.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:23:\"Plugin đã cài đặt\";s:12:\"access_level\";s:16:\"activate_plugins\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:11:\"plugins.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:11:\"plugins.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:11:\"plugins.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:1;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:30:\"plugins.php>plugin-install.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:10:\"Cài mới\";s:12:\"access_level\";s:15:\"install_plugins\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:18:\"plugin-install.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:11:\"plugins.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:18:\"plugin-install.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:2;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:2;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:29:\"plugins.php>plugin-editor.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:12:\"Sửa plugin\";s:12:\"access_level\";s:12:\"edit_plugins\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:17:\"plugin-editor.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:11:\"plugins.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:17:\"plugin-editor.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:86:\"Plugin <span class=\"update-plugins count-0\"><span class=\"plugin-count\">0</span></span>\";s:12:\"access_level\";s:16:\"activate_plugins\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:11:\"plugins.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:26:\"menu-top menu-icon-plugins\";s:8:\"hookname\";s:12:\"menu-plugins\";s:8:\"icon_url\";s:23:\"dashicons-admin-plugins\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:11:\"plugins.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:9:\"users.php\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:12;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:10:\">users.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:3:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:19:\"users.php>users.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:25:\"Tất cả người dùng\";s:12:\"access_level\";s:10:\"list_users\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:9:\"users.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:9:\"users.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:9:\"users.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:1;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:22:\"users.php>user-new.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:11:\"Thêm mới\";s:12:\"access_level\";s:12:\"create_users\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:12:\"user-new.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:9:\"users.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:12:\"user-new.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:2;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:2;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:21:\"users.php>profile.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:8:\"Hồ sơ\";s:12:\"access_level\";s:4:\"read\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:11:\"profile.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:9:\"users.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:11:\"profile.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:12:\"Thành viên\";s:12:\"access_level\";s:10:\"list_users\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:9:\"users.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:24:\"menu-top menu-icon-users\";s:8:\"hookname\";s:10:\"menu-users\";s:8:\"icon_url\";s:21:\"dashicons-admin-users\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:9:\"users.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:9:\"tools.php\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:13;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:10:\">tools.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:6:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:19:\"tools.php>tools.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:15:\"Các công cụ\";s:12:\"access_level\";s:10:\"edit_posts\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:9:\"tools.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:9:\"tools.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:9:\"tools.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:1;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:20:\"tools.php>import.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:11:\"Nhập vào\";s:12:\"access_level\";s:6:\"import\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:10:\"import.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:9:\"tools.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:10:\"import.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:2;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:2;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:20:\"tools.php>export.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:9:\"Xuất ra\";s:12:\"access_level\";s:6:\"export\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:10:\"export.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:9:\"tools.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:10:\"export.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:3;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:3;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:25:\"tools.php>site-health.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:23:\"Kiểm tra hệ thống\";s:12:\"access_level\";s:23:\"view_site_health_checks\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:15:\"site-health.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:9:\"tools.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:15:\"site-health.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:4;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:4;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:34:\"tools.php>export-personal-data.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:28:\"Xuất dữ liệu cá nhân\";s:12:\"access_level\";s:27:\"export_others_personal_data\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:24:\"export-personal-data.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:9:\"tools.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:24:\"export-personal-data.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:5;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:5;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:33:\"tools.php>erase-personal-data.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:26:\"Xóa dữ liệu cá nhân\";s:12:\"access_level\";s:26:\"erase_others_personal_data\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:23:\"erase-personal-data.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:9:\"tools.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:23:\"erase-personal-data.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:10:\"Công cụ\";s:12:\"access_level\";s:10:\"edit_posts\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:9:\"tools.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:24:\"menu-top menu-icon-tools\";s:8:\"hookname\";s:10:\"menu-tools\";s:8:\"icon_url\";s:21:\"dashicons-admin-tools\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:9:\"tools.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:19:\"options-general.php\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:14;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:20:\">options-general.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:9:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:39:\"options-general.php>options-general.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:11:\"Tổng quan\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:19:\"options-general.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:19:\"options-general.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:19:\"options-general.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:1;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:39:\"options-general.php>options-writing.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:6:\"Viết\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:19:\"options-writing.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:19:\"options-general.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:19:\"options-writing.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:2;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:2;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:39:\"options-general.php>options-reading.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:6:\"Đọc\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:19:\"options-reading.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:19:\"options-general.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:19:\"options-reading.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:3;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:3;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:42:\"options-general.php>options-discussion.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:13:\"Thảo luận\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:22:\"options-discussion.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:19:\"options-general.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:22:\"options-discussion.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:4;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:4;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:37:\"options-general.php>options-media.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:5:\"Media\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:17:\"options-media.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:19:\"options-general.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:17:\"options-media.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:5;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:5;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:41:\"options-general.php>options-permalink.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:21:\"Đường dẫn tĩnh\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:21:\"options-permalink.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:19:\"options-general.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:21:\"options-permalink.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:6;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:6;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:39:\"options-general.php>options-privacy.php\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:10:\"Riêng tư\";s:12:\"access_level\";s:22:\"manage_privacy_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:19:\"options-privacy.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:19:\"options-general.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:19:\"options-privacy.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:7;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:7;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:28:\"options-general.php>pagenavi\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:17:\"PageNavi Settings\";s:10:\"menu_title\";s:8:\"PageNavi\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:8:\"pagenavi\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:19:\"options-general.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:33:\"options-general.php?page=pagenavi\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:8;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:8;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:31:\"options-general.php>menu_editor\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:11:\"Menu Editor\";s:10:\"menu_title\";s:11:\"Menu Editor\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:11:\"menu_editor\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:19:\"options-general.php\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:36:\"options-general.php?page=menu_editor\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:11:\"Cài đặt\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:19:\"options-general.php\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:27:\"menu-top menu-icon-settings\";s:8:\"hookname\";s:13:\"menu-settings\";s:8:\"icon_url\";s:24:\"dashicons-admin-settings\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:19:\"options-general.php\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:34:\"edit.php?post_type=acf-field-group\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:15;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:35:\">edit.php?post_type=acf-field-group\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:4:{i:0;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:0;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:69:\"edit.php?post_type=acf-field-group>edit.php?post_type=acf-field-group\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:12:\"Field Groups\";s:10:\"menu_title\";s:12:\"Field Groups\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:34:\"edit.php?post_type=acf-field-group\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:34:\"edit.php?post_type=acf-field-group\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:34:\"edit.php?post_type=acf-field-group\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:1;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:1;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:73:\"edit.php?post_type=acf-field-group>post-new.php?post_type=acf-field-group\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:7:\"Add New\";s:10:\"menu_title\";s:7:\"Add New\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:38:\"post-new.php?post_type=acf-field-group\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:34:\"edit.php?post_type=acf-field-group\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:38:\"post-new.php?post_type=acf-field-group\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:2;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:2;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:44:\"edit.php?post_type=acf-field-group>acf-tools\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:5:\"Tools\";s:10:\"menu_title\";s:5:\"Tools\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:9:\"acf-tools\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:34:\"edit.php?post_type=acf-field-group\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:49:\"edit.php?post_type=acf-field-group&page=acf-tools\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}i:3;a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:3;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:55:\"edit.php?post_type=acf-field-group>acf-settings-updates\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:7:\"Updates\";s:10:\"menu_title\";s:7:\"Updates\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:20:\"acf-settings-updates\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";s:34:\"edit.php?post_type=acf-field-group\";s:9:\"css_class\";s:0:\"\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:60:\"edit.php?post_type=acf-field-group&page=acf-settings-updates\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:13:\"Custom Fields\";s:10:\"menu_title\";s:13:\"Custom Fields\";s:12:\"access_level\";s:14:\"manage_options\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:34:\"edit.php?post_type=acf-field-group\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:53:\"menu-top toplevel_page_edit?post_type=acf-field-group\";s:8:\"hookname\";s:44:\"toplevel_page_edit?post_type=acf-field-group\";s:8:\"icon_url\";s:31:\"dashicons-welcome-widgets-menus\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:34:\"edit.php?post_type=acf-field-group\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:14:\"theme-settings\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:16;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:0;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:15:\">theme-settings\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:13:\"Theme options\";s:10:\"menu_title\";s:13:\"Theme options\";s:12:\"access_level\";s:10:\"edit_posts\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:14:\"theme-settings\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:55:\"menu-top menu-icon-generic toplevel_page_theme-settings\";s:8:\"hookname\";s:28:\"toplevel_page_theme-settings\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:0;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:1;s:6:\"custom\";b:0;s:3:\"url\";s:29:\"admin.php?page=theme-settings\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}s:11:\"separator_8\";a:32:{s:10:\"page_title\";N;s:10:\"menu_title\";N;s:12:\"access_level\";N;s:16:\"extra_capability\";N;s:4:\"file\";N;s:12:\"page_heading\";N;s:8:\"position\";i:17;s:6:\"parent\";N;s:9:\"css_class\";N;s:8:\"hookname\";N;s:8:\"icon_url\";N;s:9:\"separator\";b:1;s:6:\"colors\";N;s:14:\"is_always_open\";N;s:7:\"open_in\";N;s:13:\"iframe_height\";N;s:25:\"is_iframe_scroll_disabled\";N;s:11:\"template_id\";s:12:\">separator_8\";s:14:\"is_plugin_page\";N;s:6:\"custom\";b:0;s:3:\"url\";N;s:16:\"embedded_page_id\";N;s:21:\"embedded_page_blog_id\";N;s:5:\"items\";a:0:{}s:12:\"grant_access\";a:0:{}s:7:\"missing\";b:0;s:6:\"unused\";b:0;s:6:\"hidden\";b:0;s:17:\"hidden_from_actor\";a:0:{}s:24:\"restrict_access_to_items\";b:0;s:24:\"had_access_before_hiding\";N;s:8:\"defaults\";a:21:{s:10:\"page_title\";s:0:\"\";s:10:\"menu_title\";s:0:\"\";s:12:\"access_level\";s:4:\"read\";s:16:\"extra_capability\";s:0:\"\";s:4:\"file\";s:11:\"separator_8\";s:12:\"page_heading\";s:0:\"\";s:6:\"parent\";N;s:9:\"css_class\";s:17:\"wp-menu-separator\";s:8:\"hookname\";s:0:\"\";s:8:\"icon_url\";s:23:\"dashicons-admin-generic\";s:9:\"separator\";b:1;s:6:\"colors\";b:0;s:14:\"is_always_open\";b:0;s:7:\"open_in\";s:11:\"same_window\";s:13:\"iframe_height\";i:0;s:25:\"is_iframe_scroll_disabled\";b:0;s:14:\"is_plugin_page\";b:0;s:6:\"custom\";b:0;s:3:\"url\";s:0:\"\";s:16:\"embedded_page_id\";i:0;s:21:\"embedded_page_blog_id\";i:1;}}}s:6:\"format\";a:3:{s:4:\"name\";s:22:\"Admin Menu Editor menu\";s:7:\"version\";s:3:\"7.0\";s:13:\"is_normalized\";b:1;}s:13:\"color_presets\";a:0:{}s:20:\"component_visibility\";a:0:{}s:22:\"has_modified_dashicons\";b:0;s:21:\"prebuilt_virtual_caps\";a:2:{i:2;a:0:{}i:3;a:0:{}}}s:19:\"custom_network_menu\";N;s:18:\"first_install_time\";i:1614403920;s:21:\"display_survey_notice\";b:1;s:17:\"plugin_db_version\";i:140;s:24:\"security_logging_enabled\";b:0;s:17:\"menu_config_scope\";s:6:\"global\";s:13:\"plugin_access\";s:14:\"manage_options\";s:15:\"allowed_user_id\";N;s:28:\"plugins_page_allowed_user_id\";N;s:27:\"show_deprecated_hide_button\";b:1;s:37:\"dashboard_hiding_confirmation_enabled\";b:1;s:21:\"submenu_icons_enabled\";s:9:\"if_custom\";s:22:\"force_custom_dashicons\";b:1;s:16:\"ui_colour_scheme\";s:7:\"classic\";s:13:\"visible_users\";a:0:{}s:23:\"show_plugin_menu_notice\";b:0;s:20:\"unused_item_position\";s:8:\"relative\";s:23:\"unused_item_permissions\";s:9:\"unchanged\";s:15:\"error_verbosity\";i:2;s:20:\"compress_custom_menu\";b:0;s:20:\"wpml_support_enabled\";b:1;s:24:\"bbpress_override_enabled\";b:0;s:16:\"is_active_module\";a:1:{s:19:\"highlight-new-menus\";b:0;}}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(192, '_transient_health-check-site-status-result', '{\"good\":\"12\",\"recommended\":\"8\",\"critical\":\"0\"}', 'yes'),
(206, 'danhmuc-dichvu_children', 'a:0:{}', 'yes'),
(219, 'service_cat_children', 'a:0:{}', 'yes'),
(221, 'category_children', 'a:0:{}', 'yes'),
(228, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(232, 'options_h_logo', '9', 'no'),
(233, '_options_h_logo', 'field_603c6b6490fd4', 'no'),
(234, 'options_h_gmail', 'abc@gmail.com', 'no'),
(235, '_options_h_gmail', 'field_603c6bb490fd6', 'no'),
(236, 'options_h_phone', '0987.654.321', 'no'),
(237, '_options_h_phone', 'field_603c6be390fd8', 'no'),
(240, 'options_f_logo', '63', 'no'),
(241, '_options_f_logo', 'field_603c6d8294513', 'no'),
(242, 'options_f_slogan', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip', 'no'),
(243, '_options_f_slogan', 'field_603c6e1994515', 'no'),
(244, 'options_f_address', 'address name company', 'no'),
(245, '_options_f_address', 'field_603c6e3f94517', 'no'),
(246, 'options_f_phone', '0987. 654. 321/ 01234. 567. 897', 'no'),
(247, '_options_f_phone', 'field_603c6e5994519', 'no'),
(248, 'options_f_gmail', 'abc@gmail.com', 'no'),
(249, '_options_f_gmail', 'field_603c6e759451b', 'no'),
(250, 'options_f_menu_title', 'Khám phá', 'no'),
(251, '_options_f_menu_title', 'field_603c6ea89451d', 'no'),
(252, 'options_f_menu_content_0_title', 'Tư vấn thiết kế xây dựng', 'no'),
(253, '_options_f_menu_content_0_title', 'field_603c6f079451f', 'no'),
(254, 'options_f_menu_content_0_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate', 'no'),
(255, '_options_f_menu_content_0_link', 'field_603c6f1094520', 'no'),
(256, 'options_f_menu_content_1_title', 'Tư vấn thiết kế xây dựng', 'no'),
(257, '_options_f_menu_content_1_title', 'field_603c6f079451f', 'no'),
(258, 'options_f_menu_content_1_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate', 'no'),
(259, '_options_f_menu_content_1_link', 'field_603c6f1094520', 'no'),
(260, 'options_f_menu_content_2_title', 'Tư vấn thiết kế xây dựng', 'no'),
(261, '_options_f_menu_content_2_title', 'field_603c6f079451f', 'no'),
(262, 'options_f_menu_content_2_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate', 'no'),
(263, '_options_f_menu_content_2_link', 'field_603c6f1094520', 'no'),
(264, 'options_f_menu_content', '3', 'no'),
(265, '_options_f_menu_content', 'field_603c6ed79451e', 'no'),
(266, 'options_f_map_iframe', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62368.041231704294!2d109.06817800623656!3d12.315613026709041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3170435ce0bf1c0f%3A0x9b48703e9a7d5dbc!2zRGnDqm4gxJBp4buBbiwgRGnDqm4gS2jDoW5oLCBLaMOhbmggSMOyYSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1612229609105!5m2!1svi!2s\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>', 'no'),
(267, '_options_f_map_iframe', 'field_603c6f5d5f622', 'no'),
(268, 'options_home_slide_0_image', '68', 'no'),
(269, '_options_home_slide_0_image', 'field_603c73780ac67', 'no'),
(270, 'options_home_slide_0_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate', 'no'),
(271, '_options_home_slide_0_link', 'field_603c738b0ac68', 'no'),
(272, 'options_home_slide_1_image', '68', 'no'),
(273, '_options_home_slide_1_image', 'field_603c73780ac67', 'no'),
(274, 'options_home_slide_1_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate', 'no'),
(275, '_options_home_slide_1_link', 'field_603c738b0ac68', 'no'),
(276, 'options_home_slide', '2', 'no'),
(277, '_options_home_slide', 'field_603c72f40ac66', 'no'),
(279, 'secret_key', '#DD{Fne</?ZT9ai|o9V7&kyd_f0V*F=;j6L<Z`|^y]eqIB[pjNW-_Lix|LLYA6TH', 'no'),
(288, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1614918088;s:7:\"checked\";a:7:{s:33:\"admin-menu-editor/menu-editor.php\";s:5:\"1.9.8\";s:35:\"advanced-cf7-db/advanced-cf7-db.php\";s:5:\"1.8.1\";s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.9.3\";s:37:\"tinymce-advanced/tinymce-advanced.php\";s:5:\"5.6.0\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.3.1\";s:27:\"wp-pagenavi/wp-pagenavi.php\";s:6:\"2.93.4\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:5:\"2.6.0\";}s:8:\"response\";a:2:{s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:3:\"5.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/contact-form-7.5.4.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";s:3:\"svg\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.7\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.9.5\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:3:\"5.6\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:12:\"translations\";a:3:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"5.3.1\";s:7:\"updated\";s:19:\"2019-11-12 17:58:46\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.3.1/vi.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"wp-pagenavi\";s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:6:\"2.93.3\";s:7:\"updated\";s:19:\"2016-12-05 01:59:40\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/plugin/wp-pagenavi/2.93.3/vi.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"2.6.0\";s:7:\"updated\";s:19:\"2019-11-12 11:21:12\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/plugin/wp-mail-smtp/2.6.0/vi.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:5:{s:33:\"admin-menu-editor/menu-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:31:\"w.org/plugins/admin-menu-editor\";s:4:\"slug\";s:17:\"admin-menu-editor\";s:6:\"plugin\";s:33:\"admin-menu-editor/menu-editor.php\";s:11:\"new_version\";s:5:\"1.9.8\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/admin-menu-editor/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/admin-menu-editor.1.9.8.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:70:\"https://ps.w.org/admin-menu-editor/assets/icon-128x128.png?rev=1418604\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/admin-menu-editor/assets/banner-772x250.png?rev=1419590\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"advanced-cf7-db/advanced-cf7-db.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/advanced-cf7-db\";s:4:\"slug\";s:15:\"advanced-cf7-db\";s:6:\"plugin\";s:35:\"advanced-cf7-db/advanced-cf7-db.php\";s:11:\"new_version\";s:5:\"1.8.1\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/advanced-cf7-db/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/advanced-cf7-db.1.8.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/advanced-cf7-db/assets/icon-256x256.jpg?rev=1696186\";s:2:\"1x\";s:68:\"https://ps.w.org/advanced-cf7-db/assets/icon-128x128.jpg?rev=1696186\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/advanced-cf7-db/assets/banner-1544x500.jpg?rev=1696186\";s:2:\"1x\";s:70:\"https://ps.w.org/advanced-cf7-db/assets/banner-772x250.jpg?rev=1696186\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"tinymce-advanced/tinymce-advanced.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/tinymce-advanced\";s:4:\"slug\";s:16:\"tinymce-advanced\";s:6:\"plugin\";s:37:\"tinymce-advanced/tinymce-advanced.php\";s:11:\"new_version\";s:5:\"5.6.0\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/tinymce-advanced/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/tinymce-advanced.5.6.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/tinymce-advanced/assets/icon-256x256.png?rev=971511\";s:2:\"1x\";s:68:\"https://ps.w.org/tinymce-advanced/assets/icon-128x128.png?rev=971511\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/tinymce-advanced/assets/banner-1544x500.png?rev=2390186\";s:2:\"1x\";s:71:\"https://ps.w.org/tinymce-advanced/assets/banner-772x250.png?rev=2390186\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"wp-pagenavi/wp-pagenavi.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/wp-pagenavi\";s:4:\"slug\";s:11:\"wp-pagenavi\";s:6:\"plugin\";s:27:\"wp-pagenavi/wp-pagenavi.php\";s:11:\"new_version\";s:6:\"2.93.4\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/wp-pagenavi/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-pagenavi.2.93.4.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:55:\"https://ps.w.org/wp-pagenavi/assets/icon.svg?rev=977997\";s:3:\"svg\";s:55:\"https://ps.w.org/wp-pagenavi/assets/icon.svg?rev=977997\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/wp-pagenavi/assets/banner-1544x500.jpg?rev=1206758\";s:2:\"1x\";s:66:\"https://ps.w.org/wp-pagenavi/assets/banner-772x250.jpg?rev=1206758\";}s:11:\"banners_rtl\";a:0:{}}s:29:\"wp-mail-smtp/wp_mail_smtp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/wp-mail-smtp\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:6:\"plugin\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:11:\"new_version\";s:5:\"2.6.0\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wp-mail-smtp/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-mail-smtp.2.6.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-256x256.png?rev=1755440\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-128x128.png?rev=1755440\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/wp-mail-smtp/assets/banner-1544x500.png?rev=2468655\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-mail-smtp/assets/banner-772x250.png?rev=2468655\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(289, 'wp_mail_smtp_initial_version', '2.6.0', 'no'),
(290, 'wp_mail_smtp_version', '2.6.0', 'no'),
(291, 'wp_mail_smtp', 'a:7:{s:4:\"mail\";a:6:{s:10:\"from_email\";s:26:\"tiepnguyen220194@gmail.com\";s:9:\"from_name\";s:7:\"Danalux\";s:6:\"mailer\";s:4:\"smtp\";s:11:\"return_path\";b:0;s:16:\"from_email_force\";b:1;s:15:\"from_name_force\";b:0;}s:4:\"smtp\";a:7:{s:7:\"autotls\";b:1;s:4:\"auth\";b:1;s:4:\"host\";s:14:\"smtp.gmail.com\";s:10:\"encryption\";s:3:\"ssl\";s:4:\"port\";i:465;s:4:\"user\";s:26:\"tiepnguyen220194@gmail.com\";s:4:\"pass\";s:76:\"7GSsczPge5I3Hrjo1b0R0bGBnRqoFrPxIbzBty5nr/lo6zfAas6Mn8jugtVUBFaFlHUIaqQAM70=\";}s:7:\"smtpcom\";a:2:{s:7:\"api_key\";s:0:\"\";s:7:\"channel\";s:0:\"\";}s:10:\"sendinblue\";a:2:{s:7:\"api_key\";s:0:\"\";s:6:\"domain\";s:0:\"\";}s:7:\"mailgun\";a:3:{s:7:\"api_key\";s:0:\"\";s:6:\"domain\";s:0:\"\";s:6:\"region\";s:2:\"US\";}s:8:\"sendgrid\";a:2:{s:7:\"api_key\";s:0:\"\";s:6:\"domain\";s:0:\"\";}s:5:\"gmail\";a:2:{s:9:\"client_id\";s:0:\"\";s:13:\"client_secret\";s:0:\"\";}}', 'no'),
(292, 'wp_mail_smtp_activated_time', '1614586128', 'no'),
(293, 'wp_mail_smtp_activated', 'a:1:{s:4:\"lite\";i:1614586128;}', 'yes'),
(296, 'action_scheduler_hybrid_store_demarkation', '89', 'yes'),
(297, 'schema-ActionScheduler_StoreSchema', '3.0.1614586129', 'yes'),
(298, 'schema-ActionScheduler_LoggerSchema', '2.0.1614586129', 'yes'),
(302, 'wp_mail_smtp_migration_version', '3', 'yes'),
(303, 'wp_mail_smtp_activation_prevent_redirect', '1', 'yes'),
(304, 'action_scheduler_lock_async-request-runner', '1614845031', 'yes'),
(305, 'wp_mail_smtp_review_notice', 'a:2:{s:4:\"time\";i:1614586142;s:9:\"dismissed\";b:0;}', 'yes'),
(308, 'wp_mail_smtp_debug', 'a:0:{}', 'no'),
(309, 'wp_mail_smtp_mail_key', 'ryL3pyv8+teeugYv8IvcqBS3JCbdguw06axtT/XYYps=', 'yes'),
(311, 'wp_mail_smtp_notifications', 'a:4:{s:6:\"update\";i:1614820436;s:4:\"feed\";a:0:{}s:6:\"events\";a:0:{}s:9:\"dismissed\";a:0:{}}', 'yes'),
(312, 'tadv_settings', 'a:10:{s:9:\"toolbar_1\";s:119:\"formatselect,bold,italic,blockquote,bullist,numlist,alignleft,aligncenter,alignright,alignjustify,link,unlink,undo,redo\";s:9:\"toolbar_2\";s:103:\"fontselect,fontsizeselect,outdent,indent,pastetext,removeformat,charmap,wp_more,forecolor,table,wp_help\";s:9:\"toolbar_3\";s:0:\"\";s:9:\"toolbar_4\";s:0:\"\";s:21:\"toolbar_classic_block\";s:123:\"formatselect,bold,italic,blockquote,bullist,numlist,alignleft,aligncenter,alignright,link,forecolor,backcolor,table,wp_help\";s:13:\"toolbar_block\";s:67:\"core/code,core/image,core/strikethrough,tadv/mark,tadv/removeformat\";s:18:\"toolbar_block_side\";s:46:\"core/superscript,core/subscript,core/underline\";s:12:\"panels_block\";s:44:\"tadv/color-panel,tadv/background-color-panel\";s:7:\"options\";s:81:\"menubar_block,menubar,merge_toolbars,advlist,contextmenu,advlink,fontsize_formats\";s:7:\"plugins\";s:30:\"table,advlist,link,contextmenu\";}', 'yes'),
(313, 'tadv_admin_settings', 'a:2:{s:7:\"options\";s:153:\"classic_paragraph_block,hybrid_mode,replace_block_editor,no_autop,table_resize_bars,table_default_attributes,table_grid,table_tab_navigation,table_advtab\";s:16:\"disabled_editors\";s:0:\"\";}', 'yes'),
(314, 'tadv_version', '5600', 'yes'),
(417, 'project_cat_children', 'a:0:{}', 'yes'),
(427, 'widget_show_post_taxonomy', 'a:3:{i:2;a:4:{s:5:\"title\";s:10:\"Danh mục\";s:11:\"id_taxonomy\";s:1:\"3\";s:11:\"number_post\";s:1:\"1\";s:5:\"theme\";s:9:\"giao-dien\";}i:3;a:3:{s:5:\"title\";s:10:\"Danh mục\";s:11:\"id_taxonomy\";s:1:\"2\";s:11:\"number_post\";s:1:\"4\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(638, 'options_nhập_form', '[contact-form-7 id=\"179\" title=\"Form tuyển dụng\"]', 'no'),
(639, '_options_nhập_form', 'field_603de4756efcb', 'no'),
(642, 'options_single_recruitment_form', '[contact-form-7 id=\"179\" title=\"Form tuyển dụng\"]', 'no'),
(643, '_options_single_recruitment_form', 'field_603de4756efcb', 'no'),
(710, '_transient_timeout_acf_plugin_updates', '1614993236', 'no'),
(711, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.9.5\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:3:\"5.6\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.9.3\";}}', 'no'),
(823, 'vsz_cf7_settings_field_12', 'a:6:{s:10:\"your-email\";a:2:{s:5:\"label\";s:5:\"Email\";s:4:\"show\";i:1;}s:8:\"your-tel\";a:2:{s:5:\"label\";s:20:\"Số điện thoại\";s:4:\"show\";i:1;}s:12:\"your-message\";a:2:{s:5:\"label\";s:10:\"Tin nhắn\";s:4:\"show\";i:1;}s:9:\"submit_ip\";a:2:{s:5:\"label\";s:9:\"submit_ip\";s:4:\"show\";i:0;}s:11:\"submit_time\";a:2:{s:5:\"label\";s:11:\"submit_time\";s:4:\"show\";i:0;}s:9:\"your-text\";a:2:{s:5:\"label\";s:4:\"Tên\";s:4:\"show\";i:1;}}', 'no'),
(824, 'vsz_cf7_settings_show_record_12', '10', 'yes'),
(830, 'vsz_cf7_settings_field_179', 'a:8:{s:9:\"your-name\";a:2:{s:5:\"label\";s:4:\"Tên\";s:4:\"show\";i:0;}s:10:\"your-email\";a:2:{s:5:\"label\";s:5:\"Email\";s:4:\"show\";i:0;}s:8:\"your-tel\";a:2:{s:5:\"label\";s:20:\"Số điện thoại\";s:4:\"show\";i:0;}s:11:\"your-select\";a:2:{s:5:\"label\";s:23:\"Ứng tuyển vị trí\";s:4:\"show\";i:1;}s:12:\"your-message\";a:2:{s:5:\"label\";s:10:\"Tin nhắn\";s:4:\"show\";i:1;}s:9:\"your-file\";a:2:{s:5:\"label\";s:2:\"CV\";s:4:\"show\";i:1;}s:9:\"submit_ip\";a:2:{s:5:\"label\";s:9:\"submit_ip\";s:4:\"show\";i:0;}s:11:\"submit_time\";a:2:{s:5:\"label\";s:11:\"submit_time\";s:4:\"show\";i:0;}}', 'no'),
(831, 'vsz_cf7_settings_show_record_179', '10', 'yes'),
(910, '_transient_timeout_as-post-store-dependencies-met', '1614928644', 'no'),
(911, '_transient_as-post-store-dependencies-met', 'yes', 'no'),
(948, 'recovery_mode_email_last_sent', '1614846352', 'yes'),
(950, 'options_sidebar_service_select_post', 'a:1:{i:0;s:2:\"15\";}', 'no'),
(951, '_options_sidebar_service_select_post', 'field_6040a86fdb97b', 'no'),
(958, 'danhmuc-duan_children', 'a:0:{}', 'yes'),
(960, '_site_transient_timeout_theme_roots', '1614919890', 'no'),
(961, '_site_transient_theme_roots', 'a:1:{s:7:\"danalux\";s:7:\"/themes\";}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_menu_item_type', 'custom'),
(4, 5, '_menu_item_menu_item_parent', '0'),
(5, 5, '_menu_item_object_id', '5'),
(6, 5, '_menu_item_object', 'custom'),
(7, 5, '_menu_item_target', ''),
(8, 5, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(9, 5, '_menu_item_xfn', ''),
(10, 5, '_menu_item_url', 'http://danalux.local/'),
(11, 5, '_menu_item_orphaned', '1614400385'),
(28, 9, '_wp_attached_file', '2021/02/icon__logo.png'),
(29, 9, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:176;s:6:\"height\";i:68;s:4:\"file\";s:22:\"2021/02/icon__logo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(30, 10, '_wp_trash_meta_status', 'publish'),
(31, 10, '_wp_trash_meta_time', '1614402561'),
(34, 12, '_form', '<div class=\"module__header\"><h2 class=\"title\">Thông tin liên hệ</h2></div>\n<div class=\"module__content\">\n<div class=\"row\">\n\n<div class=\"col-12\">\n<div class=\"form-group\">\n    [text* your-name class:form-control class:input__text placeholder \"Họ và tên\"]\n</div>\n</div>\n<div class=\"col-12 col-lg-6\">\n<div class=\"form-group\">\n    [email* your-email class:form-control class:input__text placeholder \"Email\"]\n</div>\n</div>\n<div class=\"col-12 col-lg-6\">\n<div class=\"form-group\">\n    [tel* your-tel class:form-control class:input__text placeholder \"Số điện thoại\"]\n</div>\n</div>\n<div class=\"col-12\">\n<div class=\"form-group\">\n    [textarea* your-message class:form-control class:textare__text placeholder \"Ghi chú\"]\n</div>\n</div>\n\n<div class=\"col-12\">\n<div class=\"form-group\">\n    [text* your-date class:form-control class:input__date placeholder \"Date\"]\n</div>\n</div>\n\n<div class=\"col-12\">\n    [submit class:btn class:btn__sen \"GỬI\"]\n</div>\n\n</div>\n</div>'),
(35, 12, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:24:\"[your-name] - Liên hệ\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@danalux.local>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:148:\"Từ: [your-name] <[your-email]>\nSố điện thoại: [your-tel]\n\nTin nhắn: [your-message]\n\nEmail được gửi từ [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(36, 12, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@danalux.local>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:105:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(37, 12, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(38, 12, '_additional_settings', ''),
(39, 12, '_locale', 'vi'),
(40, 14, '_edit_last', '1'),
(41, 14, '_edit_lock', '1614405523:1'),
(42, 15, '_edit_last', '1'),
(43, 15, '_edit_lock', '1614567323:1'),
(44, 16, '_edit_last', '1'),
(45, 16, '_edit_lock', '1614653827:1'),
(46, 17, '_edit_last', '1'),
(47, 17, '_edit_lock', '1614599209:1'),
(48, 18, '_edit_last', '1'),
(49, 18, '_edit_lock', '1614590863:1'),
(50, 19, '_edit_last', '1'),
(51, 19, '_edit_lock', '1614593813:1'),
(54, 21, '_edit_last', '1'),
(55, 21, '_wp_page_template', 'default'),
(56, 21, '_edit_lock', '1614915262:1'),
(57, 23, '_edit_last', '1'),
(58, 23, '_wp_page_template', 'default'),
(59, 23, '_edit_lock', '1614581208:1'),
(60, 25, '_edit_last', '1'),
(61, 25, '_wp_page_template', 'template-contact.php'),
(62, 25, '_edit_lock', '1614586414:1'),
(63, 27, '_edit_last', '1'),
(64, 27, '_wp_page_template', 'template-recruitment.php'),
(65, 27, '_edit_lock', '1614831449:1'),
(66, 29, '_edit_last', '1'),
(67, 29, '_edit_lock', '1614568557:1'),
(68, 30, '_menu_item_type', 'post_type'),
(69, 30, '_menu_item_menu_item_parent', '0'),
(70, 30, '_menu_item_object_id', '21'),
(71, 30, '_menu_item_object', 'page'),
(72, 30, '_menu_item_target', ''),
(73, 30, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(74, 30, '_menu_item_xfn', ''),
(75, 30, '_menu_item_url', ''),
(77, 31, '_menu_item_type', 'post_type'),
(78, 31, '_menu_item_menu_item_parent', '0'),
(79, 31, '_menu_item_object_id', '23'),
(80, 31, '_menu_item_object', 'page'),
(81, 31, '_menu_item_target', ''),
(82, 31, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(83, 31, '_menu_item_xfn', ''),
(84, 31, '_menu_item_url', ''),
(86, 32, '_menu_item_type', 'post_type'),
(87, 32, '_menu_item_menu_item_parent', '0'),
(88, 32, '_menu_item_object_id', '25'),
(89, 32, '_menu_item_object', 'page'),
(90, 32, '_menu_item_target', ''),
(91, 32, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(92, 32, '_menu_item_xfn', ''),
(93, 32, '_menu_item_url', ''),
(95, 33, '_menu_item_type', 'post_type'),
(96, 33, '_menu_item_menu_item_parent', '0'),
(97, 33, '_menu_item_object_id', '27'),
(98, 33, '_menu_item_object', 'page'),
(99, 33, '_menu_item_target', ''),
(100, 33, '_menu_item_classes', 'a:1:{i:0;s:23:\"single-menu-recruitment\";}'),
(101, 33, '_menu_item_xfn', ''),
(102, 33, '_menu_item_url', ''),
(122, 36, '_menu_item_type', 'taxonomy'),
(123, 36, '_menu_item_menu_item_parent', '0'),
(124, 36, '_menu_item_object_id', '5'),
(125, 36, '_menu_item_object', 'category'),
(126, 36, '_menu_item_target', ''),
(127, 36, '_menu_item_classes', 'a:1:{i:0;s:16:\"single-menu-post\";}'),
(128, 36, '_menu_item_xfn', ''),
(129, 36, '_menu_item_url', ''),
(131, 38, '_edit_last', '1'),
(132, 38, '_edit_lock', '1614582836:1'),
(133, 45, '_edit_last', '1'),
(134, 45, '_edit_lock', '1614574146:1'),
(135, 63, '_wp_attached_file', '2021/03/icon__footer-logo.png'),
(136, 63, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:305;s:6:\"height\";i:92;s:4:\"file\";s:29:\"2021/03/icon__footer-logo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(137, 68, '_wp_attached_file', '2021/03/slide__1.jpg'),
(138, 68, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:501;s:4:\"file\";s:20:\"2021/03/slide__1.jpg\";s:5:\"sizes\";a:2:{s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"slide__1-768x200.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:21:\"slide__1-1536x401.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(139, 72, '_edit_last', '1'),
(140, 72, '_edit_lock', '1614586556:1'),
(141, 25, 'contact_address', '<p>20 Nguyễn Văn Tố, P. Thuận Phước, Q.Hải Châu, Thành phố Đà Nẵng.<br />\r\n<b> Hotline:</b> 0905 133 077</p>'),
(142, 25, '_contact_address', 'field_603c94fea3c50'),
(143, 25, 'contact_email', '<p><b> Email:</b> Danalux@gmail.com</p>'),
(144, 25, '_contact_email', 'field_603c954ac47eb'),
(145, 25, 'contact_work', '<p><b> Sáng:</b> 8h00 – 12h00<br />\r\n<b> Chiều:</b> 13h00 – 17h00</p>'),
(146, 25, '_contact_work', 'field_603c956bc47ed'),
(147, 25, 'contact_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62368.041231704294!2d109.06817800623656!3d12.315613026709041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3170435ce0bf1c0f%3A0x9b48703e9a7d5dbc!2zRGnDqm4gxJBp4buBbiwgRGnDqm4gS2jDoW5oLCBLaMOhbmggSMOyYSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1612229609105!5m2!1svi!2s\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>'),
(148, 25, '_contact_map', 'field_603c958cc47ef'),
(149, 25, 'contact_form', '[contact-form-7 id=\"12\" title=\"Contact form 1\"]'),
(150, 25, '_contact_form', 'field_603c95bbc47f1'),
(151, 83, 'contact_address', '20 Nguyễn Văn Tố, P. Thuận Phước, Q.Hải Châu,\r\n                                    Thành phố Đà Nẵng. <br />\r\n                                    <b> Hotline:</b> 0905 133 077'),
(152, 83, '_contact_address', 'field_603c94fea3c50'),
(153, 83, 'contact_email', '<b> Email:</b> Danalux@gmail.com'),
(154, 83, '_contact_email', 'field_603c954ac47eb'),
(155, 83, 'contact_work', '<b> Sáng:</b> 8h00 – 12h00<br />\r\n                                    <b> Chiều:</b> 13h00 – 17h00'),
(156, 83, '_contact_work', 'field_603c956bc47ed'),
(157, 83, 'contact_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62368.041231704294!2d109.06817800623656!3d12.315613026709041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3170435ce0bf1c0f%3A0x9b48703e9a7d5dbc!2zRGnDqm4gxJBp4buBbiwgRGnDqm4gS2jDoW5oLCBLaMOhbmggSMOyYSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1612229609105!5m2!1svi!2s\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>'),
(158, 83, '_contact_map', 'field_603c958cc47ef'),
(159, 83, 'contact_form', '[contact-form-7 id=\"12\" title=\"Contact form 1\"]'),
(160, 83, '_contact_form', 'field_603c95bbc47f1'),
(161, 84, 'contact_address', '20 Nguyễn Văn Tố, P. Thuận Phước, Q.Hải Châu, Thành phố Đà Nẵng. <br />\r\n<b> Hotline:</b> 0905 133 077'),
(162, 84, '_contact_address', 'field_603c94fea3c50'),
(163, 84, 'contact_email', '<b> Email:</b> Danalux@gmail.com'),
(164, 84, '_contact_email', 'field_603c954ac47eb'),
(165, 84, 'contact_work', '<b> Sáng:</b> 8h00 – 12h00<br />\r\n<b> Chiều:</b> 13h00 – 17h00'),
(166, 84, '_contact_work', 'field_603c956bc47ed'),
(167, 84, 'contact_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62368.041231704294!2d109.06817800623656!3d12.315613026709041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3170435ce0bf1c0f%3A0x9b48703e9a7d5dbc!2zRGnDqm4gxJBp4buBbiwgRGnDqm4gS2jDoW5oLCBLaMOhbmggSMOyYSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1612229609105!5m2!1svi!2s\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>'),
(168, 84, '_contact_map', 'field_603c958cc47ef'),
(169, 84, 'contact_form', '[contact-form-7 id=\"12\" title=\"Contact form 1\"]'),
(170, 84, '_contact_form', 'field_603c95bbc47f1'),
(171, 86, 'contact_address', '20 Nguyễn Văn Tố, P. Thuận Phước, Q.Hải Châu, Thành phố Đà Nẵng. <br />\r\n<b> Hotline:</b> 0905 133 077'),
(172, 86, '_contact_address', 'field_603c94fea3c50'),
(173, 86, 'contact_email', '<b> Email:</b> Danalux@gmail.com'),
(174, 86, '_contact_email', 'field_603c954ac47eb'),
(175, 86, 'contact_work', '<b> Sáng:</b> 8h00 – 12h00<br />\r\n<b> Chiều:</b> 13h00 – 17h00'),
(176, 86, '_contact_work', 'field_603c956bc47ed'),
(177, 86, 'contact_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62368.041231704294!2d109.06817800623656!3d12.315613026709041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3170435ce0bf1c0f%3A0x9b48703e9a7d5dbc!2zRGnDqm4gxJBp4buBbiwgRGnDqm4gS2jDoW5oLCBLaMOhbmggSMOyYSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1612229609105!5m2!1svi!2s\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>'),
(178, 86, '_contact_map', 'field_603c958cc47ef'),
(179, 86, 'contact_form', '[contact-form-7 id=\"12\" title=\"Contact form 1\"]'),
(180, 86, '_contact_form', 'field_603c95bbc47f1'),
(181, 87, 'contact_address', '20 Nguyễn Văn Tố, P. Thuận Phước, Q.Hải Châu, Thành phố Đà Nẵng.\r\n<b> Hotline:</b> 0905 133 077'),
(182, 87, '_contact_address', 'field_603c94fea3c50'),
(183, 87, 'contact_email', '<b> Email:</b> Danalux@gmail.com'),
(184, 87, '_contact_email', 'field_603c954ac47eb'),
(185, 87, 'contact_work', '<b> Sáng:</b> 8h00 – 12h00\r\n<b> Chiều:</b> 13h00 – 17h00'),
(186, 87, '_contact_work', 'field_603c956bc47ed'),
(187, 87, 'contact_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62368.041231704294!2d109.06817800623656!3d12.315613026709041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3170435ce0bf1c0f%3A0x9b48703e9a7d5dbc!2zRGnDqm4gxJBp4buBbiwgRGnDqm4gS2jDoW5oLCBLaMOhbmggSMOyYSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1612229609105!5m2!1svi!2s\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>'),
(188, 87, '_contact_map', 'field_603c958cc47ef'),
(189, 87, 'contact_form', '[contact-form-7 id=\"12\" title=\"Contact form 1\"]'),
(190, 87, '_contact_form', 'field_603c95bbc47f1'),
(191, 88, 'contact_address', '20 Nguyễn Văn Tố, P. Thuận Phước, Q.Hải Châu, Thành phố Đà Nẵng.\r\n<b> Hotline:</b> 0905 133 077'),
(192, 88, '_contact_address', 'field_603c94fea3c50'),
(193, 88, 'contact_email', '<b> Email:</b> Danalux@gmail.com'),
(194, 88, '_contact_email', 'field_603c954ac47eb'),
(195, 88, 'contact_work', '<b> Sáng:</b> 8h00 – 12h00\r\n<b> Chiều:</b> 13h00 – 17h00'),
(196, 88, '_contact_work', 'field_603c956bc47ed'),
(197, 88, 'contact_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62368.041231704294!2d109.06817800623656!3d12.315613026709041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3170435ce0bf1c0f%3A0x9b48703e9a7d5dbc!2zRGnDqm4gxJBp4buBbiwgRGnDqm4gS2jDoW5oLCBLaMOhbmggSMOyYSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1612229609105!5m2!1svi!2s\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>'),
(198, 88, '_contact_map', 'field_603c958cc47ef'),
(199, 88, 'contact_form', '[contact-form-7 id=\"12\" title=\"Contact form 1\"]'),
(200, 88, '_contact_form', 'field_603c95bbc47f1'),
(201, 89, 'contact_address', '<p>20 Nguyễn Văn Tố, P. Thuận Phước, Q.Hải Châu, Thành phố Đà Nẵng.<br />\r\n<b> Hotline:</b> 0905 133 077</p>'),
(202, 89, '_contact_address', 'field_603c94fea3c50'),
(203, 89, 'contact_email', '<p><b> Email:</b> Danalux@gmail.com</p>'),
(204, 89, '_contact_email', 'field_603c954ac47eb'),
(205, 89, 'contact_work', '<p><b> Sáng:</b> 8h00 – 12h00<br />\r\n<b> Chiều:</b> 13h00 – 17h00</p>'),
(206, 89, '_contact_work', 'field_603c956bc47ed'),
(207, 89, 'contact_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62368.041231704294!2d109.06817800623656!3d12.315613026709041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3170435ce0bf1c0f%3A0x9b48703e9a7d5dbc!2zRGnDqm4gxJBp4buBbiwgRGnDqm4gS2jDoW5oLCBLaMOhbmggSMOyYSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1612229609105!5m2!1svi!2s\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>'),
(208, 89, '_contact_map', 'field_603c958cc47ef'),
(209, 89, 'contact_form', '[contact-form-7 id=\"12\" title=\"Contact form 1\"]'),
(210, 89, '_contact_form', 'field_603c95bbc47f1'),
(211, 90, 'contact_address', '<p>20 Nguyễn Văn Tố, P. Thuận Phước, Q.Hải Châu, Thành phố Đà Nẵng.<br />\r\n<b> Hotline:</b> 0905 133 077</p>'),
(212, 90, '_contact_address', 'field_603c94fea3c50'),
(213, 90, 'contact_email', '<p><b> Email:</b> Danalux@gmail.com</p>'),
(214, 90, '_contact_email', 'field_603c954ac47eb'),
(215, 90, 'contact_work', '<p><b> Sáng:</b> 8h00 – 12h00<br />\r\n<b> Chiều:</b> 13h00 – 17h00</p>'),
(216, 90, '_contact_work', 'field_603c956bc47ed'),
(217, 90, 'contact_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62368.041231704294!2d109.06817800623656!3d12.315613026709041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3170435ce0bf1c0f%3A0x9b48703e9a7d5dbc!2zRGnDqm4gxJBp4buBbiwgRGnDqm4gS2jDoW5oLCBLaMOhbmggSMOyYSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1612229609105!5m2!1svi!2s\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>'),
(218, 90, '_contact_map', 'field_603c958cc47ef'),
(219, 90, 'contact_form', '[contact-form-7 id=\"12\" title=\"Contact form 1\"]'),
(220, 90, '_contact_form', 'field_603c95bbc47f1'),
(221, 91, 'contact_address', '<p>20 Nguyễn Văn Tố, P. Thuận Phước, Q.Hải Châu, Thành phố Đà Nẵng.<br />\r\n<b> Hotline:</b> 0905 133 077</p>'),
(222, 91, '_contact_address', 'field_603c94fea3c50'),
(223, 91, 'contact_email', '<p><b> Email:</b> Danalux@gmail.com</p>'),
(224, 91, '_contact_email', 'field_603c954ac47eb'),
(225, 91, 'contact_work', '<p><b> Sáng:</b> 8h00 – 12h00<br />\r\n<b> Chiều:</b> 13h00 – 17h00</p>'),
(226, 91, '_contact_work', 'field_603c956bc47ed'),
(227, 91, 'contact_map', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62368.041231704294!2d109.06817800623656!3d12.315613026709041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3170435ce0bf1c0f%3A0x9b48703e9a7d5dbc!2zRGnDqm4gxJBp4buBbiwgRGnDqm4gS2jDoW5oLCBLaMOhbmggSMOyYSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1612229609105!5m2!1svi!2s\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>'),
(228, 91, '_contact_map', 'field_603c958cc47ef'),
(229, 91, 'contact_form', '[contact-form-7 id=\"12\" title=\"Contact form 1\"]'),
(230, 91, '_contact_form', 'field_603c95bbc47f1'),
(240, 93, '_edit_last', '1'),
(241, 93, '_edit_lock', '1614832672:1'),
(242, 121, '_wp_attached_file', '2021/03/about__1.jpg'),
(243, 121, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:371;s:6:\"height\";i:371;s:4:\"file\";s:20:\"2021/03/about__1.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(244, 122, '_wp_attached_file', '2021/03/service__1.jpg'),
(245, 122, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1001;s:6:\"height\";i:551;s:4:\"file\";s:22:\"2021/03/service__1.jpg\";s:5:\"sizes\";a:1:{s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"service__1-768x423.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:423;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(246, 123, '_wp_attached_file', '2021/03/service__2.jpg'),
(247, 123, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:920;s:6:\"height\";i:276;s:4:\"file\";s:22:\"2021/03/service__2.jpg\";s:5:\"sizes\";a:1:{s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"service__2-768x230.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(248, 21, 'home_intro_title', 'Giới thiệu chung về DANALUX'),
(249, 21, '_home_intro_title', 'field_603ca3be78a30'),
(250, 21, 'home_intro_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.'),
(251, 21, '_home_intro_desc', 'field_603ca41d78a31'),
(252, 21, 'home_intro_image', '121'),
(253, 21, '_home_intro_image', 'field_603ca43c78a32'),
(254, 21, 'home_service_title', 'DỊCH VỤ'),
(255, 21, '_home_service_title', 'field_603ca46a78a34'),
(256, 21, 'home_service_content_0_title', 'Tư vấn thiết kế xây dựng'),
(257, 21, '_home_service_content_0_title', 'field_603ca49478a36'),
(258, 21, 'home_service_content_0_image', '122'),
(259, 21, '_home_service_content_0_image', 'field_603ca4b278a37'),
(260, 21, 'home_service_content_0_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(261, 21, '_home_service_content_0_link', 'field_603ca4d178a38'),
(262, 21, 'home_service_content_1_title', 'Tư vấn thiết kế xây dựng'),
(263, 21, '_home_service_content_1_title', 'field_603ca49478a36'),
(264, 21, 'home_service_content_1_image', '123'),
(265, 21, '_home_service_content_1_image', 'field_603ca4b278a37'),
(266, 21, 'home_service_content_1_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(267, 21, '_home_service_content_1_link', 'field_603ca4d178a38'),
(268, 21, 'home_service_content_2_title', 'Tư vấn thiết kế xây dựng'),
(269, 21, '_home_service_content_2_title', 'field_603ca49478a36'),
(270, 21, 'home_service_content_2_image', '125'),
(271, 21, '_home_service_content_2_image', 'field_603ca4b278a37'),
(272, 21, 'home_service_content_2_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(273, 21, '_home_service_content_2_link', 'field_603ca4d178a38'),
(274, 21, 'home_service_content', '3'),
(275, 21, '_home_service_content', 'field_603ca47778a35'),
(276, 21, 'home_project_title', 'DỰ ÁN'),
(277, 21, '_home_project_title', 'field_603ca51f296cc'),
(278, 21, 'home_project_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent'),
(279, 21, '_home_project_desc', 'field_603ca53f296cd'),
(280, 21, 'home_ads_content_0_image', '126'),
(281, 21, '_home_ads_content_0_image', 'field_603ca5a9296d1'),
(282, 21, 'home_ads_content_0_title', '5 +'),
(283, 21, '_home_ads_content_0_title', 'field_603ca5a1296d0'),
(284, 21, 'home_ads_content_0_desc', 'NĂM KINH NGHIỆM <br> CHUYÊN SÂU'),
(285, 21, '_home_ads_content_0_desc', 'field_603ca5bd296d2'),
(286, 21, 'home_ads_content_1_image', '127'),
(287, 21, '_home_ads_content_1_image', 'field_603ca5a9296d1'),
(288, 21, 'home_ads_content_1_title', '15 +'),
(289, 21, '_home_ads_content_1_title', 'field_603ca5a1296d0'),
(290, 21, 'home_ads_content_1_desc', 'CHUYÊN GIA <br> TƯ VẤN'),
(291, 21, '_home_ads_content_1_desc', 'field_603ca5bd296d2'),
(292, 21, 'home_ads_content_2_image', '128'),
(293, 21, '_home_ads_content_2_image', 'field_603ca5a9296d1'),
(294, 21, 'home_ads_content_2_title', '15 +'),
(295, 21, '_home_ads_content_2_title', 'field_603ca5a1296d0'),
(296, 21, 'home_ads_content_2_desc', 'NGÀNH NGHỀ <br> LĨNH VỰC'),
(297, 21, '_home_ads_content_2_desc', 'field_603ca5bd296d2'),
(298, 21, 'home_ads_content_3_image', '129'),
(299, 21, '_home_ads_content_3_image', 'field_603ca5a9296d1'),
(300, 21, 'home_ads_content_3_title', '500 +'),
(301, 21, '_home_ads_content_3_title', 'field_603ca5a1296d0'),
(302, 21, 'home_ads_content_3_desc', 'DỰ ÁN <br> ĐÃ HOÀN THÀNH'),
(303, 21, '_home_ads_content_3_desc', 'field_603ca5bd296d2'),
(304, 21, 'home_ads_content', '4'),
(305, 21, '_home_ads_content', 'field_603ca57c296cf'),
(306, 21, 'home_intro_video_title', 'VIDEO'),
(307, 21, '_home_intro_video_title', 'field_603ca62d863f4'),
(308, 21, 'home_intro_video_iframe', '<iframe class=\"frame--iframe\" width=\"100%\" src=\"https://www.youtube.com/embed/seuNwJCKdk4\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(309, 21, '_home_intro_video_iframe', 'field_603ca65a863f6'),
(310, 21, 'home_intro_video_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt'),
(311, 21, '_home_intro_video_desc', 'field_603ca642863f5'),
(312, 21, 'home_news_title', 'TIN MỚI NHẤT'),
(313, 21, '_home_news_title', 'field_603ca6ac863f8'),
(314, 21, 'home_news_selectpost', 'a:3:{i:0;s:2:\"19\";i:1;s:3:\"137\";i:2;s:3:\"140\";}'),
(315, 21, '_home_news_selectpost', 'field_603ca6ca863f9'),
(316, 21, 'home_news_readmore', 'Tin mới khác'),
(317, 21, '_home_news_readmore', 'field_603ca726863fa'),
(318, 21, 'home_news_readmore_url', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(319, 21, '_home_news_readmore_url', 'field_603ca73e863fb'),
(320, 124, 'home_intro_title', 'Giới thiệu chung về DANALUX'),
(321, 124, '_home_intro_title', 'field_603ca3be78a30'),
(322, 124, 'home_intro_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.'),
(323, 124, '_home_intro_desc', 'field_603ca41d78a31'),
(324, 124, 'home_intro_image', '121'),
(325, 124, '_home_intro_image', 'field_603ca43c78a32'),
(326, 124, 'home_service_title', 'DỊCH VỤ'),
(327, 124, '_home_service_title', 'field_603ca46a78a34'),
(328, 124, 'home_service_content_0_title', 'Tư vấn thiết kế xây dựng'),
(329, 124, '_home_service_content_0_title', 'field_603ca49478a36'),
(330, 124, 'home_service_content_0_image', '122'),
(331, 124, '_home_service_content_0_image', 'field_603ca4b278a37'),
(332, 124, 'home_service_content_0_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(333, 124, '_home_service_content_0_link', 'field_603ca4d178a38'),
(334, 124, 'home_service_content_1_title', 'Tư vấn thiết kế xây dựng'),
(335, 124, '_home_service_content_1_title', 'field_603ca49478a36'),
(336, 124, 'home_service_content_1_image', '123'),
(337, 124, '_home_service_content_1_image', 'field_603ca4b278a37'),
(338, 124, 'home_service_content_1_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(339, 124, '_home_service_content_1_link', 'field_603ca4d178a38'),
(340, 124, 'home_service_content_2_title', 'Tư vấn thiết kế xây dựng'),
(341, 124, '_home_service_content_2_title', 'field_603ca49478a36'),
(342, 124, 'home_service_content_2_image', ''),
(343, 124, '_home_service_content_2_image', 'field_603ca4b278a37'),
(344, 124, 'home_service_content_2_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(345, 124, '_home_service_content_2_link', 'field_603ca4d178a38'),
(346, 124, 'home_service_content', '3'),
(347, 124, '_home_service_content', 'field_603ca47778a35'),
(348, 124, 'home_project_title', 'DỰ ÁN'),
(349, 124, '_home_project_title', 'field_603ca51f296cc'),
(350, 124, 'home_project_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent'),
(351, 124, '_home_project_desc', 'field_603ca53f296cd'),
(352, 124, 'home_ads_content_0_image', ''),
(353, 124, '_home_ads_content_0_image', 'field_603ca5a9296d1'),
(354, 124, 'home_ads_content_0_title', ''),
(355, 124, '_home_ads_content_0_title', 'field_603ca5a1296d0'),
(356, 124, 'home_ads_content_0_desc', ''),
(357, 124, '_home_ads_content_0_desc', 'field_603ca5bd296d2'),
(358, 124, 'home_ads_content_1_image', ''),
(359, 124, '_home_ads_content_1_image', 'field_603ca5a9296d1'),
(360, 124, 'home_ads_content_1_title', ''),
(361, 124, '_home_ads_content_1_title', 'field_603ca5a1296d0'),
(362, 124, 'home_ads_content_1_desc', ''),
(363, 124, '_home_ads_content_1_desc', 'field_603ca5bd296d2'),
(364, 124, 'home_ads_content_2_image', ''),
(365, 124, '_home_ads_content_2_image', 'field_603ca5a9296d1'),
(366, 124, 'home_ads_content_2_title', ''),
(367, 124, '_home_ads_content_2_title', 'field_603ca5a1296d0'),
(368, 124, 'home_ads_content_2_desc', ''),
(369, 124, '_home_ads_content_2_desc', 'field_603ca5bd296d2'),
(370, 124, 'home_ads_content_3_image', ''),
(371, 124, '_home_ads_content_3_image', 'field_603ca5a9296d1'),
(372, 124, 'home_ads_content_3_title', ''),
(373, 124, '_home_ads_content_3_title', 'field_603ca5a1296d0'),
(374, 124, 'home_ads_content_3_desc', ''),
(375, 124, '_home_ads_content_3_desc', 'field_603ca5bd296d2'),
(376, 124, 'home_ads_content', '4'),
(377, 124, '_home_ads_content', 'field_603ca57c296cf'),
(378, 124, 'home_intro_video_title', ''),
(379, 124, '_home_intro_video_title', 'field_603ca62d863f4'),
(380, 124, 'home_intro_video_iframe', ''),
(381, 124, '_home_intro_video_iframe', 'field_603ca65a863f6'),
(382, 124, 'home_intro_video_desc', ''),
(383, 124, '_home_intro_video_desc', 'field_603ca642863f5'),
(384, 124, 'home_news_title', ''),
(385, 124, '_home_news_title', 'field_603ca6ac863f8'),
(386, 124, 'home_news_selectpost', ''),
(387, 124, '_home_news_selectpost', 'field_603ca6ca863f9'),
(388, 124, 'home_news_readmore', ''),
(389, 124, '_home_news_readmore', 'field_603ca726863fa'),
(390, 124, 'home_news_readmore_url', ''),
(391, 124, '_home_news_readmore_url', 'field_603ca73e863fb'),
(392, 125, '_wp_attached_file', '2021/03/service__3.jpg'),
(393, 125, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:920;s:6:\"height\";i:276;s:4:\"file\";s:22:\"2021/03/service__3.jpg\";s:5:\"sizes\";a:1:{s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"service__3-768x230.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(394, 126, '_wp_attached_file', '2021/03/icon__field-1.png'),
(395, 126, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:59;s:6:\"height\";i:58;s:4:\"file\";s:25:\"2021/03/icon__field-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(396, 127, '_wp_attached_file', '2021/03/icon__field-2.png'),
(397, 127, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:58;s:6:\"height\";i:56;s:4:\"file\";s:25:\"2021/03/icon__field-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(398, 128, '_wp_attached_file', '2021/03/icon__field-3.png'),
(399, 128, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:49;s:6:\"height\";i:49;s:4:\"file\";s:25:\"2021/03/icon__field-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(400, 129, '_wp_attached_file', '2021/03/icon__field-4.png'),
(401, 129, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:73;s:6:\"height\";i:60;s:4:\"file\";s:25:\"2021/03/icon__field-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(402, 130, 'home_intro_title', 'Giới thiệu chung về DANALUX'),
(403, 130, '_home_intro_title', 'field_603ca3be78a30'),
(404, 130, 'home_intro_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.'),
(405, 130, '_home_intro_desc', 'field_603ca41d78a31'),
(406, 130, 'home_intro_image', '121'),
(407, 130, '_home_intro_image', 'field_603ca43c78a32'),
(408, 130, 'home_service_title', 'DỊCH VỤ'),
(409, 130, '_home_service_title', 'field_603ca46a78a34'),
(410, 130, 'home_service_content_0_title', 'Tư vấn thiết kế xây dựng'),
(411, 130, '_home_service_content_0_title', 'field_603ca49478a36'),
(412, 130, 'home_service_content_0_image', '122'),
(413, 130, '_home_service_content_0_image', 'field_603ca4b278a37'),
(414, 130, 'home_service_content_0_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(415, 130, '_home_service_content_0_link', 'field_603ca4d178a38'),
(416, 130, 'home_service_content_1_title', 'Tư vấn thiết kế xây dựng'),
(417, 130, '_home_service_content_1_title', 'field_603ca49478a36'),
(418, 130, 'home_service_content_1_image', '123'),
(419, 130, '_home_service_content_1_image', 'field_603ca4b278a37'),
(420, 130, 'home_service_content_1_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(421, 130, '_home_service_content_1_link', 'field_603ca4d178a38'),
(422, 130, 'home_service_content_2_title', 'Tư vấn thiết kế xây dựng'),
(423, 130, '_home_service_content_2_title', 'field_603ca49478a36'),
(424, 130, 'home_service_content_2_image', '125'),
(425, 130, '_home_service_content_2_image', 'field_603ca4b278a37'),
(426, 130, 'home_service_content_2_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(427, 130, '_home_service_content_2_link', 'field_603ca4d178a38'),
(428, 130, 'home_service_content', '3'),
(429, 130, '_home_service_content', 'field_603ca47778a35'),
(430, 130, 'home_project_title', 'DỰ ÁN'),
(431, 130, '_home_project_title', 'field_603ca51f296cc'),
(432, 130, 'home_project_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent'),
(433, 130, '_home_project_desc', 'field_603ca53f296cd'),
(434, 130, 'home_ads_content_0_image', '126'),
(435, 130, '_home_ads_content_0_image', 'field_603ca5a9296d1'),
(436, 130, 'home_ads_content_0_title', '5 +'),
(437, 130, '_home_ads_content_0_title', 'field_603ca5a1296d0'),
(438, 130, 'home_ads_content_0_desc', 'NĂM KINH NGHIỆM <br> CHUYÊN SÂU'),
(439, 130, '_home_ads_content_0_desc', 'field_603ca5bd296d2'),
(440, 130, 'home_ads_content_1_image', '127'),
(441, 130, '_home_ads_content_1_image', 'field_603ca5a9296d1'),
(442, 130, 'home_ads_content_1_title', '15 +'),
(443, 130, '_home_ads_content_1_title', 'field_603ca5a1296d0'),
(444, 130, 'home_ads_content_1_desc', 'CHUYÊN GIA <br> TƯ VẤN'),
(445, 130, '_home_ads_content_1_desc', 'field_603ca5bd296d2'),
(446, 130, 'home_ads_content_2_image', '128'),
(447, 130, '_home_ads_content_2_image', 'field_603ca5a9296d1'),
(448, 130, 'home_ads_content_2_title', '15 +'),
(449, 130, '_home_ads_content_2_title', 'field_603ca5a1296d0'),
(450, 130, 'home_ads_content_2_desc', 'NGÀNH NGHỀ <br> LĨNH VỰC'),
(451, 130, '_home_ads_content_2_desc', 'field_603ca5bd296d2'),
(452, 130, 'home_ads_content_3_image', '129'),
(453, 130, '_home_ads_content_3_image', 'field_603ca5a9296d1'),
(454, 130, 'home_ads_content_3_title', '500 +'),
(455, 130, '_home_ads_content_3_title', 'field_603ca5a1296d0'),
(456, 130, 'home_ads_content_3_desc', 'DỰ ÁN <br> ĐÃ HOÀN THÀNH'),
(457, 130, '_home_ads_content_3_desc', 'field_603ca5bd296d2'),
(458, 130, 'home_ads_content', '4'),
(459, 130, '_home_ads_content', 'field_603ca57c296cf'),
(460, 130, 'home_intro_video_title', 'VIDEO'),
(461, 130, '_home_intro_video_title', 'field_603ca62d863f4'),
(462, 130, 'home_intro_video_iframe', '<iframe class=\"frame--iframe\" width=\"100%\" src=\"https://www.youtube.com/embed/seuNwJCKdk4\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(463, 130, '_home_intro_video_iframe', 'field_603ca65a863f6'),
(464, 130, 'home_intro_video_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt'),
(465, 130, '_home_intro_video_desc', 'field_603ca642863f5'),
(466, 130, 'home_news_title', 'TIN MỚI NHẤT'),
(467, 130, '_home_news_title', 'field_603ca6ac863f8'),
(468, 130, 'home_news_selectpost', 'a:1:{i:0;s:2:\"19\";}'),
(469, 130, '_home_news_selectpost', 'field_603ca6ca863f9'),
(470, 130, 'home_news_readmore', 'Tin mới khác'),
(471, 130, '_home_news_readmore', 'field_603ca726863fa'),
(472, 130, 'home_news_readmore_url', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(473, 130, '_home_news_readmore_url', 'field_603ca73e863fb'),
(474, 21, 'home_project_select_cat', 'a:2:{i:0;s:2:\"10\";i:1;s:1:\"9\";}'),
(475, 21, '_home_project_select_cat', 'field_603cb1e456fde'),
(476, 132, 'home_intro_title', 'Giới thiệu chung về DANALUX'),
(477, 132, '_home_intro_title', 'field_603ca3be78a30'),
(478, 132, 'home_intro_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.'),
(479, 132, '_home_intro_desc', 'field_603ca41d78a31'),
(480, 132, 'home_intro_image', '121'),
(481, 132, '_home_intro_image', 'field_603ca43c78a32'),
(482, 132, 'home_service_title', 'DỊCH VỤ'),
(483, 132, '_home_service_title', 'field_603ca46a78a34'),
(484, 132, 'home_service_content_0_title', 'Tư vấn thiết kế xây dựng'),
(485, 132, '_home_service_content_0_title', 'field_603ca49478a36'),
(486, 132, 'home_service_content_0_image', '122'),
(487, 132, '_home_service_content_0_image', 'field_603ca4b278a37'),
(488, 132, 'home_service_content_0_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(489, 132, '_home_service_content_0_link', 'field_603ca4d178a38'),
(490, 132, 'home_service_content_1_title', 'Tư vấn thiết kế xây dựng'),
(491, 132, '_home_service_content_1_title', 'field_603ca49478a36'),
(492, 132, 'home_service_content_1_image', '123'),
(493, 132, '_home_service_content_1_image', 'field_603ca4b278a37'),
(494, 132, 'home_service_content_1_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(495, 132, '_home_service_content_1_link', 'field_603ca4d178a38'),
(496, 132, 'home_service_content_2_title', 'Tư vấn thiết kế xây dựng'),
(497, 132, '_home_service_content_2_title', 'field_603ca49478a36'),
(498, 132, 'home_service_content_2_image', '125'),
(499, 132, '_home_service_content_2_image', 'field_603ca4b278a37'),
(500, 132, 'home_service_content_2_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(501, 132, '_home_service_content_2_link', 'field_603ca4d178a38'),
(502, 132, 'home_service_content', '3'),
(503, 132, '_home_service_content', 'field_603ca47778a35'),
(504, 132, 'home_project_title', 'DỰ ÁN'),
(505, 132, '_home_project_title', 'field_603ca51f296cc'),
(506, 132, 'home_project_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent'),
(507, 132, '_home_project_desc', 'field_603ca53f296cd'),
(508, 132, 'home_ads_content_0_image', '126'),
(509, 132, '_home_ads_content_0_image', 'field_603ca5a9296d1'),
(510, 132, 'home_ads_content_0_title', '5 +'),
(511, 132, '_home_ads_content_0_title', 'field_603ca5a1296d0'),
(512, 132, 'home_ads_content_0_desc', 'NĂM KINH NGHIỆM <br> CHUYÊN SÂU'),
(513, 132, '_home_ads_content_0_desc', 'field_603ca5bd296d2'),
(514, 132, 'home_ads_content_1_image', '127'),
(515, 132, '_home_ads_content_1_image', 'field_603ca5a9296d1'),
(516, 132, 'home_ads_content_1_title', '15 +'),
(517, 132, '_home_ads_content_1_title', 'field_603ca5a1296d0'),
(518, 132, 'home_ads_content_1_desc', 'CHUYÊN GIA <br> TƯ VẤN'),
(519, 132, '_home_ads_content_1_desc', 'field_603ca5bd296d2'),
(520, 132, 'home_ads_content_2_image', '128'),
(521, 132, '_home_ads_content_2_image', 'field_603ca5a9296d1'),
(522, 132, 'home_ads_content_2_title', '15 +'),
(523, 132, '_home_ads_content_2_title', 'field_603ca5a1296d0'),
(524, 132, 'home_ads_content_2_desc', 'NGÀNH NGHỀ <br> LĨNH VỰC'),
(525, 132, '_home_ads_content_2_desc', 'field_603ca5bd296d2'),
(526, 132, 'home_ads_content_3_image', '129'),
(527, 132, '_home_ads_content_3_image', 'field_603ca5a9296d1'),
(528, 132, 'home_ads_content_3_title', '500 +'),
(529, 132, '_home_ads_content_3_title', 'field_603ca5a1296d0'),
(530, 132, 'home_ads_content_3_desc', 'DỰ ÁN <br> ĐÃ HOÀN THÀNH'),
(531, 132, '_home_ads_content_3_desc', 'field_603ca5bd296d2'),
(532, 132, 'home_ads_content', '4'),
(533, 132, '_home_ads_content', 'field_603ca57c296cf'),
(534, 132, 'home_intro_video_title', 'VIDEO'),
(535, 132, '_home_intro_video_title', 'field_603ca62d863f4'),
(536, 132, 'home_intro_video_iframe', '<iframe class=\"frame--iframe\" width=\"100%\" src=\"https://www.youtube.com/embed/seuNwJCKdk4\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(537, 132, '_home_intro_video_iframe', 'field_603ca65a863f6'),
(538, 132, 'home_intro_video_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt'),
(539, 132, '_home_intro_video_desc', 'field_603ca642863f5'),
(540, 132, 'home_news_title', 'TIN MỚI NHẤT'),
(541, 132, '_home_news_title', 'field_603ca6ac863f8'),
(542, 132, 'home_news_selectpost', 'a:1:{i:0;s:2:\"19\";}'),
(543, 132, '_home_news_selectpost', 'field_603ca6ca863f9'),
(544, 132, 'home_news_readmore', 'Tin mới khác'),
(545, 132, '_home_news_readmore', 'field_603ca726863fa'),
(546, 132, 'home_news_readmore_url', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(547, 132, '_home_news_readmore_url', 'field_603ca73e863fb'),
(548, 132, 'home_project_select_cat', 'a:2:{i:0;s:1:\"8\";i:1;s:1:\"7\";}'),
(549, 132, '_home_project_select_cat', 'field_603cb1e456fde'),
(550, 133, '_edit_last', '1'),
(551, 133, '_edit_lock', '1614915254:1'),
(552, 135, '_wp_attached_file', '2021/03/project__1.jpg'),
(553, 135, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:349;s:6:\"height\";i:349;s:4:\"file\";s:22:\"2021/03/project__1.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(554, 136, '_wp_attached_file', '2021/03/project__2.jpg'),
(555, 136, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:349;s:6:\"height\";i:349;s:4:\"file\";s:22:\"2021/03/project__2.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(556, 137, '_edit_last', '1'),
(557, 137, '_edit_lock', '1614593705:1'),
(558, 137, '_thumbnail_id', '122'),
(561, 19, '_thumbnail_id', '135'),
(564, 140, '_edit_last', '1'),
(565, 140, '_edit_lock', '1614595572:1'),
(568, 142, '_edit_last', '1'),
(569, 142, '_edit_lock', '1614598683:1'),
(570, 143, '_menu_item_type', 'post_type_archive'),
(571, 143, '_menu_item_menu_item_parent', '0');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(572, 143, '_menu_item_object_id', '-16'),
(573, 143, '_menu_item_object', 'service'),
(574, 143, '_menu_item_target', ''),
(575, 143, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(576, 143, '_menu_item_xfn', ''),
(577, 143, '_menu_item_url', ''),
(578, 143, '_menu_item_orphaned', '1614650169'),
(579, 144, '_menu_item_type', 'taxonomy'),
(580, 144, '_menu_item_menu_item_parent', '0'),
(581, 144, '_menu_item_object_id', '2'),
(582, 144, '_menu_item_object', 'danhmuc-dichvu'),
(583, 144, '_menu_item_target', ''),
(584, 144, '_menu_item_classes', 'a:1:{i:0;s:19:\"single-menu-service\";}'),
(585, 144, '_menu_item_xfn', ''),
(586, 144, '_menu_item_url', ''),
(588, 145, '_menu_item_type', 'post_type_archive'),
(589, 145, '_menu_item_menu_item_parent', '0'),
(590, 145, '_menu_item_object_id', '-19'),
(591, 145, '_menu_item_object', 'duan'),
(592, 145, '_menu_item_target', ''),
(593, 145, '_menu_item_classes', 'a:1:{i:0;s:19:\"single-menu-project\";}'),
(594, 145, '_menu_item_xfn', ''),
(595, 145, '_menu_item_url', ''),
(597, 146, '_edit_last', '1'),
(598, 146, '_edit_lock', '1614655394:1'),
(599, 147, '_edit_last', '1'),
(600, 147, '_edit_lock', '1614653844:1'),
(601, 16, '_wp_old_slug', 'bai-viet-du-an-1'),
(602, 147, '_wp_old_slug', 'bai-viet-du-an-2'),
(603, 148, '_edit_last', '1'),
(604, 148, '_edit_lock', '1614655383:1'),
(605, 149, '_edit_last', '1'),
(606, 149, '_edit_lock', '1614653798:1'),
(607, 149, '_wp_trash_meta_status', 'publish'),
(608, 149, '_wp_trash_meta_time', '1614653947'),
(609, 149, '_wp_desired_post_slug', 'du-an-thuc-hien-2'),
(610, 150, '_edit_last', '1'),
(611, 150, '_edit_lock', '1614655408:1'),
(612, 151, '_edit_last', '1'),
(613, 151, '_edit_lock', '1614655467:1'),
(614, 152, '_edit_last', '1'),
(615, 152, '_edit_lock', '1614833081:1'),
(616, 27, 'recruitment_select_post', 'a:2:{i:0;s:3:\"151\";i:1;s:3:\"150\";}'),
(617, 27, '_recruitment_select_post', 'field_603db0ac6352b'),
(618, 27, 'recruitment_environment_title', 'MÔI TRƯỜNG LÀM VIỆC TẠI DANALUX'),
(619, 27, '_recruitment_environment_title', 'field_603db0fa6352d'),
(620, 27, 'recruitment_environment_content', '4'),
(621, 27, '_recruitment_environment_content', 'field_603db1236352e'),
(622, 160, 'recruitment_select_post', 'a:2:{i:0;s:3:\"151\";i:1;s:3:\"150\";}'),
(623, 160, '_recruitment_select_post', 'field_603db0ac6352b'),
(624, 160, 'recruitment_environment_title', ''),
(625, 160, '_recruitment_environment_title', 'field_603db0fa6352d'),
(626, 160, 'recruitment_environment_content', ''),
(627, 160, '_recruitment_environment_content', 'field_603db1236352e'),
(628, 161, '_wp_attached_file', '2021/03/environment__1.png'),
(629, 161, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:161;s:6:\"height\";i:96;s:4:\"file\";s:26:\"2021/03/environment__1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(630, 162, '_wp_attached_file', '2021/03/environment__2.png'),
(631, 162, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:173;s:6:\"height\";i:117;s:4:\"file\";s:26:\"2021/03/environment__2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(632, 163, '_wp_attached_file', '2021/03/environment__3.png'),
(633, 163, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:125;s:6:\"height\";i:125;s:4:\"file\";s:26:\"2021/03/environment__3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(634, 164, '_wp_attached_file', '2021/03/environment__4.png'),
(635, 164, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:165;s:6:\"height\";i:111;s:4:\"file\";s:26:\"2021/03/environment__4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(636, 27, 'recruitment_environment_content_0_image', '161'),
(637, 27, '_recruitment_environment_content_0_image', 'field_603db1416352f'),
(638, 27, 'recruitment_environment_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(639, 27, '_recruitment_environment_content_0_title', 'field_603db15963530'),
(640, 27, 'recruitment_environment_content_1_image', '162'),
(641, 27, '_recruitment_environment_content_1_image', 'field_603db1416352f'),
(642, 27, 'recruitment_environment_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(643, 27, '_recruitment_environment_content_1_title', 'field_603db15963530'),
(644, 27, 'recruitment_environment_content_2_image', '163'),
(645, 27, '_recruitment_environment_content_2_image', 'field_603db1416352f'),
(646, 27, 'recruitment_environment_content_2_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(647, 27, '_recruitment_environment_content_2_title', 'field_603db15963530'),
(648, 27, 'recruitment_environment_content_3_image', '164'),
(649, 27, '_recruitment_environment_content_3_image', 'field_603db1416352f'),
(650, 27, 'recruitment_environment_content_3_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(651, 27, '_recruitment_environment_content_3_title', 'field_603db15963530'),
(652, 165, 'recruitment_select_post', 'a:2:{i:0;s:3:\"151\";i:1;s:3:\"150\";}'),
(653, 165, '_recruitment_select_post', 'field_603db0ac6352b'),
(654, 165, 'recruitment_environment_title', 'MÔI TRƯỜNG LÀM VIỆC TẠI DANALUX'),
(655, 165, '_recruitment_environment_title', 'field_603db0fa6352d'),
(656, 165, 'recruitment_environment_content', '4'),
(657, 165, '_recruitment_environment_content', 'field_603db1236352e'),
(658, 165, 'recruitment_environment_content_0_image', '161'),
(659, 165, '_recruitment_environment_content_0_image', 'field_603db1416352f'),
(660, 165, 'recruitment_environment_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(661, 165, '_recruitment_environment_content_0_title', 'field_603db15963530'),
(662, 165, 'recruitment_environment_content_1_image', '162'),
(663, 165, '_recruitment_environment_content_1_image', 'field_603db1416352f'),
(664, 165, 'recruitment_environment_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(665, 165, '_recruitment_environment_content_1_title', 'field_603db15963530'),
(666, 165, 'recruitment_environment_content_2_image', '163'),
(667, 165, '_recruitment_environment_content_2_image', 'field_603db1416352f'),
(668, 165, 'recruitment_environment_content_2_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(669, 165, '_recruitment_environment_content_2_title', 'field_603db15963530'),
(670, 165, 'recruitment_environment_content_3_image', '164'),
(671, 165, '_recruitment_environment_content_3_image', 'field_603db1416352f'),
(672, 165, 'recruitment_environment_content_3_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(673, 165, '_recruitment_environment_content_3_title', 'field_603db15963530'),
(674, 27, 'recruitment_bonus_title', 'Lương thưởng hấp dẫn'),
(675, 27, '_recruitment_bonus_title', 'field_603db66402843'),
(676, 27, 'recruitment_bonus_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(677, 27, '_recruitment_bonus_content_0_title', 'field_603db6bd02845'),
(678, 27, 'recruitment_bonus_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(679, 27, '_recruitment_bonus_content_1_title', 'field_603db6bd02845'),
(680, 27, 'recruitment_bonus_content', '2'),
(681, 27, '_recruitment_bonus_content', 'field_603db6ad02844'),
(682, 27, 'recruitment_bonus_image', '164'),
(683, 27, '_recruitment_bonus_image', 'field_603db6d802846'),
(684, 27, 'recruitment_welfare_title', 'Phúc lợi đầy đủ'),
(685, 27, '_recruitment_welfare_title', 'field_603db6e102847'),
(686, 27, 'recruitment_welfare_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(687, 27, '_recruitment_welfare_content_0_title', 'field_603db75702849'),
(688, 27, 'recruitment_welfare_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(689, 27, '_recruitment_welfare_content_1_title', 'field_603db75702849'),
(690, 27, 'recruitment_welfare_content', '2'),
(691, 27, '_recruitment_welfare_content', 'field_603db73602848'),
(692, 27, 'recruitment_welfare_gallery', 'a:3:{i:0;s:3:\"163\";i:1;s:3:\"162\";i:2;s:3:\"161\";}'),
(693, 27, '_recruitment_welfare_gallery', 'field_603db7640284a'),
(694, 176, 'recruitment_select_post', 'a:2:{i:0;s:3:\"151\";i:1;s:3:\"150\";}'),
(695, 176, '_recruitment_select_post', 'field_603db0ac6352b'),
(696, 176, 'recruitment_environment_title', 'MÔI TRƯỜNG LÀM VIỆC TẠI DANALUX'),
(697, 176, '_recruitment_environment_title', 'field_603db0fa6352d'),
(698, 176, 'recruitment_environment_content', '4'),
(699, 176, '_recruitment_environment_content', 'field_603db1236352e'),
(700, 176, 'recruitment_environment_content_0_image', '161'),
(701, 176, '_recruitment_environment_content_0_image', 'field_603db1416352f'),
(702, 176, 'recruitment_environment_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(703, 176, '_recruitment_environment_content_0_title', 'field_603db15963530'),
(704, 176, 'recruitment_environment_content_1_image', '162'),
(705, 176, '_recruitment_environment_content_1_image', 'field_603db1416352f'),
(706, 176, 'recruitment_environment_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(707, 176, '_recruitment_environment_content_1_title', 'field_603db15963530'),
(708, 176, 'recruitment_environment_content_2_image', '163'),
(709, 176, '_recruitment_environment_content_2_image', 'field_603db1416352f'),
(710, 176, 'recruitment_environment_content_2_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(711, 176, '_recruitment_environment_content_2_title', 'field_603db15963530'),
(712, 176, 'recruitment_environment_content_3_image', '164'),
(713, 176, '_recruitment_environment_content_3_image', 'field_603db1416352f'),
(714, 176, 'recruitment_environment_content_3_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(715, 176, '_recruitment_environment_content_3_title', 'field_603db15963530'),
(716, 176, 'recruitment_bonus_title', 'Lương thưởng hấp dẫn'),
(717, 176, '_recruitment_bonus_title', 'field_603db66402843'),
(718, 176, 'recruitment_bonus_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(719, 176, '_recruitment_bonus_content_0_title', 'field_603db6bd02845'),
(720, 176, 'recruitment_bonus_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(721, 176, '_recruitment_bonus_content_1_title', 'field_603db6bd02845'),
(722, 176, 'recruitment_bonus_content', '2'),
(723, 176, '_recruitment_bonus_content', 'field_603db6ad02844'),
(724, 176, 'recruitment_bonus_image', '164'),
(725, 176, '_recruitment_bonus_image', 'field_603db6d802846'),
(726, 176, 'recruitment_welfare_title', 'Phúc lợi đầy đủ'),
(727, 176, '_recruitment_welfare_title', 'field_603db6e102847'),
(728, 176, 'recruitment_welfare_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(729, 176, '_recruitment_welfare_content_0_title', 'field_603db75702849'),
(730, 176, 'recruitment_welfare_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(731, 176, '_recruitment_welfare_content_1_title', 'field_603db75702849'),
(732, 176, 'recruitment_welfare_content', '2'),
(733, 176, '_recruitment_welfare_content', 'field_603db73602848'),
(734, 176, 'recruitment_welfare_gallery', 'a:3:{i:0;s:3:\"163\";i:1;s:3:\"162\";i:2;s:3:\"161\";}'),
(735, 176, '_recruitment_welfare_gallery', 'field_603db7640284a'),
(736, 27, 'recruitment_title', 'LỢI ÍCH KHI LÀM VIỆC TẠI DANALUX'),
(737, 27, '_recruitment_title', 'field_603db82aef31c'),
(738, 178, 'recruitment_select_post', 'a:2:{i:0;s:3:\"151\";i:1;s:3:\"150\";}'),
(739, 178, '_recruitment_select_post', 'field_603db0ac6352b'),
(740, 178, 'recruitment_environment_title', 'MÔI TRƯỜNG LÀM VIỆC TẠI DANALUX'),
(741, 178, '_recruitment_environment_title', 'field_603db0fa6352d'),
(742, 178, 'recruitment_environment_content', '4'),
(743, 178, '_recruitment_environment_content', 'field_603db1236352e'),
(744, 178, 'recruitment_environment_content_0_image', '161'),
(745, 178, '_recruitment_environment_content_0_image', 'field_603db1416352f'),
(746, 178, 'recruitment_environment_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(747, 178, '_recruitment_environment_content_0_title', 'field_603db15963530'),
(748, 178, 'recruitment_environment_content_1_image', '162'),
(749, 178, '_recruitment_environment_content_1_image', 'field_603db1416352f'),
(750, 178, 'recruitment_environment_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(751, 178, '_recruitment_environment_content_1_title', 'field_603db15963530'),
(752, 178, 'recruitment_environment_content_2_image', '163'),
(753, 178, '_recruitment_environment_content_2_image', 'field_603db1416352f'),
(754, 178, 'recruitment_environment_content_2_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(755, 178, '_recruitment_environment_content_2_title', 'field_603db15963530'),
(756, 178, 'recruitment_environment_content_3_image', '164'),
(757, 178, '_recruitment_environment_content_3_image', 'field_603db1416352f'),
(758, 178, 'recruitment_environment_content_3_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(759, 178, '_recruitment_environment_content_3_title', 'field_603db15963530'),
(760, 178, 'recruitment_bonus_title', 'Lương thưởng hấp dẫn'),
(761, 178, '_recruitment_bonus_title', 'field_603db66402843'),
(762, 178, 'recruitment_bonus_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(763, 178, '_recruitment_bonus_content_0_title', 'field_603db6bd02845'),
(764, 178, 'recruitment_bonus_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(765, 178, '_recruitment_bonus_content_1_title', 'field_603db6bd02845'),
(766, 178, 'recruitment_bonus_content', '2'),
(767, 178, '_recruitment_bonus_content', 'field_603db6ad02844'),
(768, 178, 'recruitment_bonus_image', '164'),
(769, 178, '_recruitment_bonus_image', 'field_603db6d802846'),
(770, 178, 'recruitment_welfare_title', 'Phúc lợi đầy đủ'),
(771, 178, '_recruitment_welfare_title', 'field_603db6e102847'),
(772, 178, 'recruitment_welfare_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(773, 178, '_recruitment_welfare_content_0_title', 'field_603db75702849'),
(774, 178, 'recruitment_welfare_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(775, 178, '_recruitment_welfare_content_1_title', 'field_603db75702849'),
(776, 178, 'recruitment_welfare_content', '2'),
(777, 178, '_recruitment_welfare_content', 'field_603db73602848'),
(778, 178, 'recruitment_welfare_gallery', 'a:3:{i:0;s:3:\"163\";i:1;s:3:\"162\";i:2;s:3:\"161\";}'),
(779, 178, '_recruitment_welfare_gallery', 'field_603db7640284a'),
(780, 178, 'recruitment_title', 'LỢI ÍCH KHI LÀM VIỆC TẠI DANALUX'),
(781, 178, '_recruitment_title', 'field_603db82aef31c'),
(782, 179, '_form', '<div class=\"module__header\"><h2 class=\"title\">NỘP ĐƠN ỨNG TUYỂN</h2></div>\n<div class=\"module__content\">\n<div class=\"row\">\n\n<div class=\"col-12\">\n<div class=\"form-group\">\n    [text* your-name class:form-control class:input__text placeholder \"Họ và tên\"]\n</div>\n</div>\n<div class=\"col-12 col-lg-6\">\n<div class=\"form-group\">\n    [email* your-email class:form-control class:input__text placeholder \"Email\"]\n</div>\n</div>\n<div class=\"col-12 col-lg-6\">\n<div class=\"form-group\">\n    [tel* your-tel class:form-control class:input__text placeholder \"Số điện thoại\"]\n</div>\n</div>\n<div class=\"col-12\">\n<div class=\"form-group\">\n    [select* your-select class:form-control class:select__text \"Chọn vị trí ứng tuyển\" \"Thực tập sinh 1\" \"Thực tập sinh 2\"]\n</div>\n</div>\n<div class=\"col-12\">\n<div class=\"form-group\">\n    [textarea* your-message class:form-control class:textare__text placeholder \"Thư ứng tuyển\"]\n</div>\n</div>\n\n<div class=\"col-12\">\n<div class=\"form-group\">\n<span class=\"fileName\"></span>\n<label for=\"file\" class=\"file form-control cus__file\">\n    [file* your-file id:file filetypes:.pdf|.doc|.docx|.ppt|.pptx class:input--file]\n    <span class=\"file__text\">\n        <span class=\"icon\"><i class=\"fas fa-cloud-upload-alt\"></i></span>\n        <span class=\"text\">TẢI LÊN CV ỨNG TUYỂN CỦA BẠN</span>\n    </span>\n</label>\n</div>\n</div>\n\n<div class=\"col-12\">\n<div class=\"form-group\">\n    [text* your-date class:form-control class:input__date placeholder \"Date\"]\n</div>\n</div>\n\n<div class=\"col-12\">\n    [submit class:btn class:btn__sen \"GỬI ĐƠN ỨNG TUYỂN\"]\n</div>\n\n</div>\n</div>'),
(783, 179, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:27:\"[your-name] - Ứng tuyển\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@danalux.local>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:209:\"Từ: [your-name] <[your-email]>\nSố điện thoại: [your-tel]\n\nVị trí ứng tuyển : [your-select]\nTin nhắn: [your-message]\nFile CV:\n[your-file]\n\nEmail được gửi từ [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:11:\"[your-file]\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(784, 179, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@danalux.local>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:105:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(785, 179, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(786, 179, '_additional_settings', ''),
(787, 179, '_locale', 'vi'),
(788, 27, 'recruitment_form', '[contact-form-7 id=\"179\" title=\"Form tuyển dụng\"]'),
(789, 27, '_recruitment_form', 'field_603dc175c7674'),
(790, 27, 'recruitment_envi_title', 'Môi trường làm việc'),
(791, 27, '_recruitment_envi_title', 'field_603dc218c7677'),
(792, 27, 'recruitment_envi_content_0_title', 'Môi trường làm việc 1'),
(793, 27, '_recruitment_envi_content_0_title', 'field_603dc233c767a'),
(794, 27, 'recruitment_envi_content_1_title', 'Môi trường làm việc 2'),
(795, 27, '_recruitment_envi_content_1_title', 'field_603dc233c767a'),
(796, 27, 'recruitment_envi_content', '2'),
(797, 27, '_recruitment_envi_content', 'field_603dc233c7679'),
(798, 27, 'recruitment_envi_image', '164'),
(799, 27, '_recruitment_envi_image', 'field_603dc23fc767d'),
(800, 27, 'recruitment_time', 'Thời gian làm việc'),
(801, 27, '_recruitment_time', 'field_603dc220c7678'),
(802, 27, 'recruitment_time_content_0_title', 'Thời gian làm việc 1'),
(803, 27, '_recruitment_time_content_0_title', 'field_603dc238c767c'),
(804, 27, 'recruitment_time_content_1_title', 'Thời gian làm việc 2'),
(805, 27, '_recruitment_time_content_1_title', 'field_603dc238c767c'),
(806, 27, 'recruitment_time_content', '2'),
(807, 27, '_recruitment_time_content', 'field_603dc238c767b'),
(808, 27, 'recruitment_time_image', '163'),
(809, 27, '_recruitment_time_image', 'field_603dc244c767e'),
(810, 192, 'recruitment_select_post', 'a:2:{i:0;s:3:\"151\";i:1;s:3:\"150\";}'),
(811, 192, '_recruitment_select_post', 'field_603db0ac6352b'),
(812, 192, 'recruitment_environment_title', 'MÔI TRƯỜNG LÀM VIỆC TẠI DANALUX'),
(813, 192, '_recruitment_environment_title', 'field_603db0fa6352d'),
(814, 192, 'recruitment_environment_content', '4'),
(815, 192, '_recruitment_environment_content', 'field_603db1236352e'),
(816, 192, 'recruitment_environment_content_0_image', '161'),
(817, 192, '_recruitment_environment_content_0_image', 'field_603db1416352f'),
(818, 192, 'recruitment_environment_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(819, 192, '_recruitment_environment_content_0_title', 'field_603db15963530'),
(820, 192, 'recruitment_environment_content_1_image', '162'),
(821, 192, '_recruitment_environment_content_1_image', 'field_603db1416352f'),
(822, 192, 'recruitment_environment_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(823, 192, '_recruitment_environment_content_1_title', 'field_603db15963530'),
(824, 192, 'recruitment_environment_content_2_image', '163'),
(825, 192, '_recruitment_environment_content_2_image', 'field_603db1416352f'),
(826, 192, 'recruitment_environment_content_2_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(827, 192, '_recruitment_environment_content_2_title', 'field_603db15963530'),
(828, 192, 'recruitment_environment_content_3_image', '164'),
(829, 192, '_recruitment_environment_content_3_image', 'field_603db1416352f'),
(830, 192, 'recruitment_environment_content_3_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(831, 192, '_recruitment_environment_content_3_title', 'field_603db15963530'),
(832, 192, 'recruitment_bonus_title', 'Lương thưởng hấp dẫn'),
(833, 192, '_recruitment_bonus_title', 'field_603db66402843'),
(834, 192, 'recruitment_bonus_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(835, 192, '_recruitment_bonus_content_0_title', 'field_603db6bd02845'),
(836, 192, 'recruitment_bonus_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(837, 192, '_recruitment_bonus_content_1_title', 'field_603db6bd02845'),
(838, 192, 'recruitment_bonus_content', '2'),
(839, 192, '_recruitment_bonus_content', 'field_603db6ad02844'),
(840, 192, 'recruitment_bonus_image', '164'),
(841, 192, '_recruitment_bonus_image', 'field_603db6d802846'),
(842, 192, 'recruitment_welfare_title', 'Phúc lợi đầy đủ'),
(843, 192, '_recruitment_welfare_title', 'field_603db6e102847'),
(844, 192, 'recruitment_welfare_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(845, 192, '_recruitment_welfare_content_0_title', 'field_603db75702849'),
(846, 192, 'recruitment_welfare_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(847, 192, '_recruitment_welfare_content_1_title', 'field_603db75702849'),
(848, 192, 'recruitment_welfare_content', '2'),
(849, 192, '_recruitment_welfare_content', 'field_603db73602848'),
(850, 192, 'recruitment_welfare_gallery', 'a:3:{i:0;s:3:\"163\";i:1;s:3:\"162\";i:2;s:3:\"161\";}'),
(851, 192, '_recruitment_welfare_gallery', 'field_603db7640284a'),
(852, 192, 'recruitment_title', 'LỢI ÍCH KHI LÀM VIỆC TẠI DANALUX'),
(853, 192, '_recruitment_title', 'field_603db82aef31c'),
(854, 192, 'recruitment_form', '[contact-form-7 id=\"179\" title=\"Form tuyển dụng\"]'),
(855, 192, '_recruitment_form', 'field_603dc175c7674'),
(856, 192, 'recruitment_envi_title', 'Môi trường làm việc'),
(857, 192, '_recruitment_envi_title', 'field_603dc218c7677'),
(858, 192, 'recruitment_envi_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(859, 192, '_recruitment_envi_content_0_title', 'field_603dc233c767a'),
(860, 192, 'recruitment_envi_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(861, 192, '_recruitment_envi_content_1_title', 'field_603dc233c767a'),
(862, 192, 'recruitment_envi_content', '2'),
(863, 192, '_recruitment_envi_content', 'field_603dc233c7679'),
(864, 192, 'recruitment_envi_image', '164'),
(865, 192, '_recruitment_envi_image', 'field_603dc23fc767d'),
(866, 192, 'recruitment_time', 'Thời gian làm việc'),
(867, 192, '_recruitment_time', 'field_603dc220c7678'),
(868, 192, 'recruitment_time_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(869, 192, '_recruitment_time_content_0_title', 'field_603dc238c767c'),
(870, 192, 'recruitment_time_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(871, 192, '_recruitment_time_content_1_title', 'field_603dc238c767c'),
(872, 192, 'recruitment_time_content', '2'),
(873, 192, '_recruitment_time_content', 'field_603dc238c767b'),
(874, 192, 'recruitment_time_image', '163'),
(875, 192, '_recruitment_time_image', 'field_603dc244c767e'),
(876, 193, '_edit_last', '1'),
(877, 193, '_edit_lock', '1614670872:1'),
(878, 195, 'home_intro_title', 'Giới thiệu chung về DANALUX'),
(879, 195, '_home_intro_title', 'field_603ca3be78a30'),
(880, 195, 'home_intro_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.'),
(881, 195, '_home_intro_desc', 'field_603ca41d78a31'),
(882, 195, 'home_intro_image', '121'),
(883, 195, '_home_intro_image', 'field_603ca43c78a32'),
(884, 195, 'home_service_title', 'DỊCH VỤ'),
(885, 195, '_home_service_title', 'field_603ca46a78a34'),
(886, 195, 'home_service_content_0_title', 'Tư vấn thiết kế xây dựng'),
(887, 195, '_home_service_content_0_title', 'field_603ca49478a36'),
(888, 195, 'home_service_content_0_image', '122'),
(889, 195, '_home_service_content_0_image', 'field_603ca4b278a37'),
(890, 195, 'home_service_content_0_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(891, 195, '_home_service_content_0_link', 'field_603ca4d178a38'),
(892, 195, 'home_service_content_1_title', 'Tư vấn thiết kế xây dựng'),
(893, 195, '_home_service_content_1_title', 'field_603ca49478a36'),
(894, 195, 'home_service_content_1_image', '123'),
(895, 195, '_home_service_content_1_image', 'field_603ca4b278a37'),
(896, 195, 'home_service_content_1_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(897, 195, '_home_service_content_1_link', 'field_603ca4d178a38'),
(898, 195, 'home_service_content_2_title', 'Tư vấn thiết kế xây dựng'),
(899, 195, '_home_service_content_2_title', 'field_603ca49478a36'),
(900, 195, 'home_service_content_2_image', '125'),
(901, 195, '_home_service_content_2_image', 'field_603ca4b278a37'),
(902, 195, 'home_service_content_2_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(903, 195, '_home_service_content_2_link', 'field_603ca4d178a38'),
(904, 195, 'home_service_content', '3'),
(905, 195, '_home_service_content', 'field_603ca47778a35'),
(906, 195, 'home_project_title', 'DỰ ÁN'),
(907, 195, '_home_project_title', 'field_603ca51f296cc'),
(908, 195, 'home_project_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent'),
(909, 195, '_home_project_desc', 'field_603ca53f296cd'),
(910, 195, 'home_ads_content_0_image', '126'),
(911, 195, '_home_ads_content_0_image', 'field_603ca5a9296d1'),
(912, 195, 'home_ads_content_0_title', '5 +'),
(913, 195, '_home_ads_content_0_title', 'field_603ca5a1296d0'),
(914, 195, 'home_ads_content_0_desc', 'NĂM KINH NGHIỆM <br> CHUYÊN SÂU'),
(915, 195, '_home_ads_content_0_desc', 'field_603ca5bd296d2'),
(916, 195, 'home_ads_content_1_image', '127'),
(917, 195, '_home_ads_content_1_image', 'field_603ca5a9296d1'),
(918, 195, 'home_ads_content_1_title', '15 +'),
(919, 195, '_home_ads_content_1_title', 'field_603ca5a1296d0'),
(920, 195, 'home_ads_content_1_desc', 'CHUYÊN GIA <br> TƯ VẤN'),
(921, 195, '_home_ads_content_1_desc', 'field_603ca5bd296d2'),
(922, 195, 'home_ads_content_2_image', '128'),
(923, 195, '_home_ads_content_2_image', 'field_603ca5a9296d1'),
(924, 195, 'home_ads_content_2_title', '15 +'),
(925, 195, '_home_ads_content_2_title', 'field_603ca5a1296d0'),
(926, 195, 'home_ads_content_2_desc', 'NGÀNH NGHỀ <br> LĨNH VỰC'),
(927, 195, '_home_ads_content_2_desc', 'field_603ca5bd296d2'),
(928, 195, 'home_ads_content_3_image', '129'),
(929, 195, '_home_ads_content_3_image', 'field_603ca5a9296d1'),
(930, 195, 'home_ads_content_3_title', '500 +'),
(931, 195, '_home_ads_content_3_title', 'field_603ca5a1296d0'),
(932, 195, 'home_ads_content_3_desc', 'DỰ ÁN <br> ĐÃ HOÀN THÀNH'),
(933, 195, '_home_ads_content_3_desc', 'field_603ca5bd296d2'),
(934, 195, 'home_ads_content', '4'),
(935, 195, '_home_ads_content', 'field_603ca57c296cf'),
(936, 195, 'home_intro_video_title', 'VIDEO'),
(937, 195, '_home_intro_video_title', 'field_603ca62d863f4'),
(938, 195, 'home_intro_video_iframe', '<iframe class=\"frame--iframe\" width=\"100%\" src=\"https://www.youtube.com/embed/seuNwJCKdk4\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(939, 195, '_home_intro_video_iframe', 'field_603ca65a863f6'),
(940, 195, 'home_intro_video_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt'),
(941, 195, '_home_intro_video_desc', 'field_603ca642863f5'),
(942, 195, 'home_news_title', 'TIN MỚI NHẤT'),
(943, 195, '_home_news_title', 'field_603ca6ac863f8'),
(944, 195, 'home_news_selectpost', 'a:1:{i:0;s:2:\"19\";}'),
(945, 195, '_home_news_selectpost', 'field_603ca6ca863f9'),
(946, 195, 'home_news_readmore', 'Tin mới khác'),
(947, 195, '_home_news_readmore', 'field_603ca726863fa'),
(948, 195, 'home_news_readmore_url', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(949, 195, '_home_news_readmore_url', 'field_603ca73e863fb'),
(950, 195, 'home_project_select_cat', 'a:2:{i:0;s:2:\"10\";i:1;s:1:\"9\";}'),
(951, 195, '_home_project_select_cat', 'field_603cb1e456fde'),
(952, 196, 'recruitment_select_post', 'a:2:{i:0;s:3:\"151\";i:1;s:3:\"150\";}'),
(953, 196, '_recruitment_select_post', 'field_603db0ac6352b'),
(954, 196, 'recruitment_environment_title', 'MÔI TRƯỜNG LÀM VIỆC TẠI DANALUX'),
(955, 196, '_recruitment_environment_title', 'field_603db0fa6352d'),
(956, 196, 'recruitment_environment_content', '4'),
(957, 196, '_recruitment_environment_content', 'field_603db1236352e'),
(958, 196, 'recruitment_environment_content_0_image', '161'),
(959, 196, '_recruitment_environment_content_0_image', 'field_603db1416352f'),
(960, 196, 'recruitment_environment_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(961, 196, '_recruitment_environment_content_0_title', 'field_603db15963530'),
(962, 196, 'recruitment_environment_content_1_image', '162'),
(963, 196, '_recruitment_environment_content_1_image', 'field_603db1416352f'),
(964, 196, 'recruitment_environment_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(965, 196, '_recruitment_environment_content_1_title', 'field_603db15963530'),
(966, 196, 'recruitment_environment_content_2_image', '163'),
(967, 196, '_recruitment_environment_content_2_image', 'field_603db1416352f'),
(968, 196, 'recruitment_environment_content_2_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(969, 196, '_recruitment_environment_content_2_title', 'field_603db15963530'),
(970, 196, 'recruitment_environment_content_3_image', '164'),
(971, 196, '_recruitment_environment_content_3_image', 'field_603db1416352f'),
(972, 196, 'recruitment_environment_content_3_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(973, 196, '_recruitment_environment_content_3_title', 'field_603db15963530'),
(974, 196, 'recruitment_bonus_title', 'Lương thưởng hấp dẫn'),
(975, 196, '_recruitment_bonus_title', 'field_603db66402843'),
(976, 196, 'recruitment_bonus_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(977, 196, '_recruitment_bonus_content_0_title', 'field_603db6bd02845'),
(978, 196, 'recruitment_bonus_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(979, 196, '_recruitment_bonus_content_1_title', 'field_603db6bd02845'),
(980, 196, 'recruitment_bonus_content', '2'),
(981, 196, '_recruitment_bonus_content', 'field_603db6ad02844'),
(982, 196, 'recruitment_bonus_image', '164'),
(983, 196, '_recruitment_bonus_image', 'field_603db6d802846'),
(984, 196, 'recruitment_welfare_title', 'Phúc lợi đầy đủ'),
(985, 196, '_recruitment_welfare_title', 'field_603db6e102847'),
(986, 196, 'recruitment_welfare_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(987, 196, '_recruitment_welfare_content_0_title', 'field_603db75702849'),
(988, 196, 'recruitment_welfare_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(989, 196, '_recruitment_welfare_content_1_title', 'field_603db75702849'),
(990, 196, 'recruitment_welfare_content', '2'),
(991, 196, '_recruitment_welfare_content', 'field_603db73602848'),
(992, 196, 'recruitment_welfare_gallery', 'a:3:{i:0;s:3:\"163\";i:1;s:3:\"162\";i:2;s:3:\"161\";}'),
(993, 196, '_recruitment_welfare_gallery', 'field_603db7640284a'),
(994, 196, 'recruitment_title', 'LỢI ÍCH KHI LÀM VIỆC TẠI DANALUX'),
(995, 196, '_recruitment_title', 'field_603db82aef31c'),
(996, 196, 'recruitment_form', '[contact-form-7 id=\"179\" title=\"Form tuyển dụng\" customtext=\"xxxxxx@example.com\"]'),
(997, 196, '_recruitment_form', 'field_603dc175c7674'),
(998, 196, 'recruitment_envi_title', 'Môi trường làm việc'),
(999, 196, '_recruitment_envi_title', 'field_603dc218c7677'),
(1000, 196, 'recruitment_envi_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1001, 196, '_recruitment_envi_content_0_title', 'field_603dc233c767a'),
(1002, 196, 'recruitment_envi_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1003, 196, '_recruitment_envi_content_1_title', 'field_603dc233c767a'),
(1004, 196, 'recruitment_envi_content', '2'),
(1005, 196, '_recruitment_envi_content', 'field_603dc233c7679'),
(1006, 196, 'recruitment_envi_image', '164'),
(1007, 196, '_recruitment_envi_image', 'field_603dc23fc767d'),
(1008, 196, 'recruitment_time', 'Thời gian làm việc'),
(1009, 196, '_recruitment_time', 'field_603dc220c7678'),
(1010, 196, 'recruitment_time_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1011, 196, '_recruitment_time_content_0_title', 'field_603dc238c767c'),
(1012, 196, 'recruitment_time_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1013, 196, '_recruitment_time_content_1_title', 'field_603dc238c767c'),
(1014, 196, 'recruitment_time_content', '2'),
(1015, 196, '_recruitment_time_content', 'field_603dc238c767b'),
(1016, 196, 'recruitment_time_image', '163'),
(1017, 196, '_recruitment_time_image', 'field_603dc244c767e'),
(1018, 197, 'recruitment_select_post', 'a:2:{i:0;s:3:\"151\";i:1;s:3:\"150\";}'),
(1019, 197, '_recruitment_select_post', 'field_603db0ac6352b'),
(1020, 197, 'recruitment_environment_title', 'MÔI TRƯỜNG LÀM VIỆC TẠI DANALUX'),
(1021, 197, '_recruitment_environment_title', 'field_603db0fa6352d'),
(1022, 197, 'recruitment_environment_content', '4'),
(1023, 197, '_recruitment_environment_content', 'field_603db1236352e'),
(1024, 197, 'recruitment_environment_content_0_image', '161'),
(1025, 197, '_recruitment_environment_content_0_image', 'field_603db1416352f'),
(1026, 197, 'recruitment_environment_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(1027, 197, '_recruitment_environment_content_0_title', 'field_603db15963530'),
(1028, 197, 'recruitment_environment_content_1_image', '162'),
(1029, 197, '_recruitment_environment_content_1_image', 'field_603db1416352f'),
(1030, 197, 'recruitment_environment_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(1031, 197, '_recruitment_environment_content_1_title', 'field_603db15963530'),
(1032, 197, 'recruitment_environment_content_2_image', '163'),
(1033, 197, '_recruitment_environment_content_2_image', 'field_603db1416352f'),
(1034, 197, 'recruitment_environment_content_2_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(1035, 197, '_recruitment_environment_content_2_title', 'field_603db15963530'),
(1036, 197, 'recruitment_environment_content_3_image', '164'),
(1037, 197, '_recruitment_environment_content_3_image', 'field_603db1416352f'),
(1038, 197, 'recruitment_environment_content_3_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(1039, 197, '_recruitment_environment_content_3_title', 'field_603db15963530'),
(1040, 197, 'recruitment_bonus_title', 'Lương thưởng hấp dẫn'),
(1041, 197, '_recruitment_bonus_title', 'field_603db66402843'),
(1042, 197, 'recruitment_bonus_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1043, 197, '_recruitment_bonus_content_0_title', 'field_603db6bd02845'),
(1044, 197, 'recruitment_bonus_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1045, 197, '_recruitment_bonus_content_1_title', 'field_603db6bd02845'),
(1046, 197, 'recruitment_bonus_content', '2'),
(1047, 197, '_recruitment_bonus_content', 'field_603db6ad02844'),
(1048, 197, 'recruitment_bonus_image', '164'),
(1049, 197, '_recruitment_bonus_image', 'field_603db6d802846'),
(1050, 197, 'recruitment_welfare_title', 'Phúc lợi đầy đủ'),
(1051, 197, '_recruitment_welfare_title', 'field_603db6e102847'),
(1052, 197, 'recruitment_welfare_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1053, 197, '_recruitment_welfare_content_0_title', 'field_603db75702849'),
(1054, 197, 'recruitment_welfare_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1055, 197, '_recruitment_welfare_content_1_title', 'field_603db75702849'),
(1056, 197, 'recruitment_welfare_content', '2'),
(1057, 197, '_recruitment_welfare_content', 'field_603db73602848'),
(1058, 197, 'recruitment_welfare_gallery', 'a:3:{i:0;s:3:\"163\";i:1;s:3:\"162\";i:2;s:3:\"161\";}'),
(1059, 197, '_recruitment_welfare_gallery', 'field_603db7640284a'),
(1060, 197, 'recruitment_title', 'LỢI ÍCH KHI LÀM VIỆC TẠI DANALUX'),
(1061, 197, '_recruitment_title', 'field_603db82aef31c'),
(1062, 197, 'recruitment_form', '[contact-form-7 id=\"179\" title=\"Form tuyển dụng\"]'),
(1063, 197, '_recruitment_form', 'field_603dc175c7674'),
(1064, 197, 'recruitment_envi_title', 'Môi trường làm việc'),
(1065, 197, '_recruitment_envi_title', 'field_603dc218c7677'),
(1066, 197, 'recruitment_envi_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1067, 197, '_recruitment_envi_content_0_title', 'field_603dc233c767a'),
(1068, 197, 'recruitment_envi_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1069, 197, '_recruitment_envi_content_1_title', 'field_603dc233c767a'),
(1070, 197, 'recruitment_envi_content', '2'),
(1071, 197, '_recruitment_envi_content', 'field_603dc233c7679'),
(1072, 197, 'recruitment_envi_image', '164'),
(1073, 197, '_recruitment_envi_image', 'field_603dc23fc767d'),
(1074, 197, 'recruitment_time', 'Thời gian làm việc'),
(1075, 197, '_recruitment_time', 'field_603dc220c7678'),
(1076, 197, 'recruitment_time_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1077, 197, '_recruitment_time_content_0_title', 'field_603dc238c767c'),
(1078, 197, 'recruitment_time_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1079, 197, '_recruitment_time_content_1_title', 'field_603dc238c767c'),
(1080, 197, 'recruitment_time_content', '2'),
(1081, 197, '_recruitment_time_content', 'field_603dc238c767b'),
(1082, 197, 'recruitment_time_image', '163'),
(1083, 197, '_recruitment_time_image', 'field_603dc244c767e'),
(1084, 21, 'home_intro_video_image', '121'),
(1085, 21, '_home_intro_video_image', 'field_60405967b8fbd'),
(1086, 200, 'home_intro_title', 'Giới thiệu chung về DANALUX'),
(1087, 200, '_home_intro_title', 'field_603ca3be78a30'),
(1088, 200, 'home_intro_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.'),
(1089, 200, '_home_intro_desc', 'field_603ca41d78a31'),
(1090, 200, 'home_intro_image', '121'),
(1091, 200, '_home_intro_image', 'field_603ca43c78a32'),
(1092, 200, 'home_service_title', 'DỊCH VỤ'),
(1093, 200, '_home_service_title', 'field_603ca46a78a34'),
(1094, 200, 'home_service_content_0_title', 'Tư vấn thiết kế xây dựng'),
(1095, 200, '_home_service_content_0_title', 'field_603ca49478a36'),
(1096, 200, 'home_service_content_0_image', '122'),
(1097, 200, '_home_service_content_0_image', 'field_603ca4b278a37'),
(1098, 200, 'home_service_content_0_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(1099, 200, '_home_service_content_0_link', 'field_603ca4d178a38'),
(1100, 200, 'home_service_content_1_title', 'Tư vấn thiết kế xây dựng'),
(1101, 200, '_home_service_content_1_title', 'field_603ca49478a36'),
(1102, 200, 'home_service_content_1_image', '123'),
(1103, 200, '_home_service_content_1_image', 'field_603ca4b278a37'),
(1104, 200, 'home_service_content_1_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(1105, 200, '_home_service_content_1_link', 'field_603ca4d178a38'),
(1106, 200, 'home_service_content_2_title', 'Tư vấn thiết kế xây dựng'),
(1107, 200, '_home_service_content_2_title', 'field_603ca49478a36'),
(1108, 200, 'home_service_content_2_image', '125'),
(1109, 200, '_home_service_content_2_image', 'field_603ca4b278a37'),
(1110, 200, 'home_service_content_2_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(1111, 200, '_home_service_content_2_link', 'field_603ca4d178a38'),
(1112, 200, 'home_service_content', '3'),
(1113, 200, '_home_service_content', 'field_603ca47778a35'),
(1114, 200, 'home_project_title', 'DỰ ÁN'),
(1115, 200, '_home_project_title', 'field_603ca51f296cc'),
(1116, 200, 'home_project_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent'),
(1117, 200, '_home_project_desc', 'field_603ca53f296cd'),
(1118, 200, 'home_ads_content_0_image', '126'),
(1119, 200, '_home_ads_content_0_image', 'field_603ca5a9296d1'),
(1120, 200, 'home_ads_content_0_title', '5 +'),
(1121, 200, '_home_ads_content_0_title', 'field_603ca5a1296d0'),
(1122, 200, 'home_ads_content_0_desc', 'NĂM KINH NGHIỆM <br> CHUYÊN SÂU'),
(1123, 200, '_home_ads_content_0_desc', 'field_603ca5bd296d2'),
(1124, 200, 'home_ads_content_1_image', '127'),
(1125, 200, '_home_ads_content_1_image', 'field_603ca5a9296d1'),
(1126, 200, 'home_ads_content_1_title', '15 +'),
(1127, 200, '_home_ads_content_1_title', 'field_603ca5a1296d0'),
(1128, 200, 'home_ads_content_1_desc', 'CHUYÊN GIA <br> TƯ VẤN'),
(1129, 200, '_home_ads_content_1_desc', 'field_603ca5bd296d2'),
(1130, 200, 'home_ads_content_2_image', '128'),
(1131, 200, '_home_ads_content_2_image', 'field_603ca5a9296d1'),
(1132, 200, 'home_ads_content_2_title', '15 +'),
(1133, 200, '_home_ads_content_2_title', 'field_603ca5a1296d0'),
(1134, 200, 'home_ads_content_2_desc', 'NGÀNH NGHỀ <br> LĨNH VỰC'),
(1135, 200, '_home_ads_content_2_desc', 'field_603ca5bd296d2'),
(1136, 200, 'home_ads_content_3_image', '129'),
(1137, 200, '_home_ads_content_3_image', 'field_603ca5a9296d1'),
(1138, 200, 'home_ads_content_3_title', '500 +'),
(1139, 200, '_home_ads_content_3_title', 'field_603ca5a1296d0'),
(1140, 200, 'home_ads_content_3_desc', 'DỰ ÁN <br> ĐÃ HOÀN THÀNH'),
(1141, 200, '_home_ads_content_3_desc', 'field_603ca5bd296d2'),
(1142, 200, 'home_ads_content', '4'),
(1143, 200, '_home_ads_content', 'field_603ca57c296cf'),
(1144, 200, 'home_intro_video_title', 'VIDEO'),
(1145, 200, '_home_intro_video_title', 'field_603ca62d863f4'),
(1146, 200, 'home_intro_video_iframe', '<iframe class=\"frame--iframe\" width=\"100%\" src=\"https://www.youtube.com/embed/seuNwJCKdk4\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(1147, 200, '_home_intro_video_iframe', 'field_603ca65a863f6'),
(1148, 200, 'home_intro_video_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt'),
(1149, 200, '_home_intro_video_desc', 'field_603ca642863f5'),
(1150, 200, 'home_news_title', 'TIN MỚI NHẤT'),
(1151, 200, '_home_news_title', 'field_603ca6ac863f8'),
(1152, 200, 'home_news_selectpost', 'a:1:{i:0;s:2:\"19\";}'),
(1153, 200, '_home_news_selectpost', 'field_603ca6ca863f9'),
(1154, 200, 'home_news_readmore', 'Tin mới khác'),
(1155, 200, '_home_news_readmore', 'field_603ca726863fa'),
(1156, 200, 'home_news_readmore_url', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(1157, 200, '_home_news_readmore_url', 'field_603ca73e863fb');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1158, 200, 'home_project_select_cat', 'a:2:{i:0;s:2:\"10\";i:1;s:1:\"9\";}'),
(1159, 200, '_home_project_select_cat', 'field_603cb1e456fde'),
(1160, 200, 'home_intro_video_image', '121'),
(1161, 200, '_home_intro_video_image', 'field_60405967b8fbd'),
(1162, 201, 'home_intro_title', 'Giới thiệu chung về DANALUX'),
(1163, 201, '_home_intro_title', 'field_603ca3be78a30'),
(1164, 201, 'home_intro_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.\r\n\r\nLorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.'),
(1165, 201, '_home_intro_desc', 'field_603ca41d78a31'),
(1166, 201, 'home_intro_image', '121'),
(1167, 201, '_home_intro_image', 'field_603ca43c78a32'),
(1168, 201, 'home_service_title', 'DỊCH VỤ'),
(1169, 201, '_home_service_title', 'field_603ca46a78a34'),
(1170, 201, 'home_service_content_0_title', 'Tư vấn thiết kế xây dựng'),
(1171, 201, '_home_service_content_0_title', 'field_603ca49478a36'),
(1172, 201, 'home_service_content_0_image', '122'),
(1173, 201, '_home_service_content_0_image', 'field_603ca4b278a37'),
(1174, 201, 'home_service_content_0_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(1175, 201, '_home_service_content_0_link', 'field_603ca4d178a38'),
(1176, 201, 'home_service_content_1_title', 'Tư vấn thiết kế xây dựng'),
(1177, 201, '_home_service_content_1_title', 'field_603ca49478a36'),
(1178, 201, 'home_service_content_1_image', '123'),
(1179, 201, '_home_service_content_1_image', 'field_603ca4b278a37'),
(1180, 201, 'home_service_content_1_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(1181, 201, '_home_service_content_1_link', 'field_603ca4d178a38'),
(1182, 201, 'home_service_content_2_title', 'Tư vấn thiết kế xây dựng'),
(1183, 201, '_home_service_content_2_title', 'field_603ca49478a36'),
(1184, 201, 'home_service_content_2_image', '125'),
(1185, 201, '_home_service_content_2_image', 'field_603ca4b278a37'),
(1186, 201, 'home_service_content_2_link', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(1187, 201, '_home_service_content_2_link', 'field_603ca4d178a38'),
(1188, 201, 'home_service_content', '3'),
(1189, 201, '_home_service_content', 'field_603ca47778a35'),
(1190, 201, 'home_project_title', 'DỰ ÁN'),
(1191, 201, '_home_project_title', 'field_603ca51f296cc'),
(1192, 201, 'home_project_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent'),
(1193, 201, '_home_project_desc', 'field_603ca53f296cd'),
(1194, 201, 'home_ads_content_0_image', '126'),
(1195, 201, '_home_ads_content_0_image', 'field_603ca5a9296d1'),
(1196, 201, 'home_ads_content_0_title', '5 +'),
(1197, 201, '_home_ads_content_0_title', 'field_603ca5a1296d0'),
(1198, 201, 'home_ads_content_0_desc', 'NĂM KINH NGHIỆM <br> CHUYÊN SÂU'),
(1199, 201, '_home_ads_content_0_desc', 'field_603ca5bd296d2'),
(1200, 201, 'home_ads_content_1_image', '127'),
(1201, 201, '_home_ads_content_1_image', 'field_603ca5a9296d1'),
(1202, 201, 'home_ads_content_1_title', '15 +'),
(1203, 201, '_home_ads_content_1_title', 'field_603ca5a1296d0'),
(1204, 201, 'home_ads_content_1_desc', 'CHUYÊN GIA <br> TƯ VẤN'),
(1205, 201, '_home_ads_content_1_desc', 'field_603ca5bd296d2'),
(1206, 201, 'home_ads_content_2_image', '128'),
(1207, 201, '_home_ads_content_2_image', 'field_603ca5a9296d1'),
(1208, 201, 'home_ads_content_2_title', '15 +'),
(1209, 201, '_home_ads_content_2_title', 'field_603ca5a1296d0'),
(1210, 201, 'home_ads_content_2_desc', 'NGÀNH NGHỀ <br> LĨNH VỰC'),
(1211, 201, '_home_ads_content_2_desc', 'field_603ca5bd296d2'),
(1212, 201, 'home_ads_content_3_image', '129'),
(1213, 201, '_home_ads_content_3_image', 'field_603ca5a9296d1'),
(1214, 201, 'home_ads_content_3_title', '500 +'),
(1215, 201, '_home_ads_content_3_title', 'field_603ca5a1296d0'),
(1216, 201, 'home_ads_content_3_desc', 'DỰ ÁN <br> ĐÃ HOÀN THÀNH'),
(1217, 201, '_home_ads_content_3_desc', 'field_603ca5bd296d2'),
(1218, 201, 'home_ads_content', '4'),
(1219, 201, '_home_ads_content', 'field_603ca57c296cf'),
(1220, 201, 'home_intro_video_title', 'VIDEO'),
(1221, 201, '_home_intro_video_title', 'field_603ca62d863f4'),
(1222, 201, 'home_intro_video_iframe', '<iframe class=\"frame--iframe\" width=\"100%\" src=\"https://www.youtube.com/embed/seuNwJCKdk4\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(1223, 201, '_home_intro_video_iframe', 'field_603ca65a863f6'),
(1224, 201, 'home_intro_video_desc', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt'),
(1225, 201, '_home_intro_video_desc', 'field_603ca642863f5'),
(1226, 201, 'home_news_title', 'TIN MỚI NHẤT'),
(1227, 201, '_home_news_title', 'field_603ca6ac863f8'),
(1228, 201, 'home_news_selectpost', 'a:3:{i:0;s:2:\"19\";i:1;s:3:\"137\";i:2;s:3:\"140\";}'),
(1229, 201, '_home_news_selectpost', 'field_603ca6ca863f9'),
(1230, 201, 'home_news_readmore', 'Tin mới khác'),
(1231, 201, '_home_news_readmore', 'field_603ca726863fa'),
(1232, 201, 'home_news_readmore_url', 'https://translate.google.com/?sl=vi&tl=en&text=Kh%C3%A1m%20ph%C3%A1&op=translate'),
(1233, 201, '_home_news_readmore_url', 'field_603ca73e863fb'),
(1234, 201, 'home_project_select_cat', 'a:2:{i:0;s:2:\"10\";i:1;s:1:\"9\";}'),
(1235, 201, '_home_project_select_cat', 'field_603cb1e456fde'),
(1236, 201, 'home_intro_video_image', '121'),
(1237, 201, '_home_intro_video_image', 'field_60405967b8fbd'),
(1238, 202, 'recruitment_select_post', 'a:2:{i:0;s:3:\"151\";i:1;s:3:\"150\";}'),
(1239, 202, '_recruitment_select_post', 'field_603db0ac6352b'),
(1240, 202, 'recruitment_environment_title', 'MÔI TRƯỜNG LÀM VIỆC TẠI DANALUX'),
(1241, 202, '_recruitment_environment_title', 'field_603db0fa6352d'),
(1242, 202, 'recruitment_environment_content', '4'),
(1243, 202, '_recruitment_environment_content', 'field_603db1236352e'),
(1244, 202, 'recruitment_environment_content_0_image', '161'),
(1245, 202, '_recruitment_environment_content_0_image', 'field_603db1416352f'),
(1246, 202, 'recruitment_environment_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(1247, 202, '_recruitment_environment_content_0_title', 'field_603db15963530'),
(1248, 202, 'recruitment_environment_content_1_image', '162'),
(1249, 202, '_recruitment_environment_content_1_image', 'field_603db1416352f'),
(1250, 202, 'recruitment_environment_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(1251, 202, '_recruitment_environment_content_1_title', 'field_603db15963530'),
(1252, 202, 'recruitment_environment_content_2_image', '163'),
(1253, 202, '_recruitment_environment_content_2_image', 'field_603db1416352f'),
(1254, 202, 'recruitment_environment_content_2_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(1255, 202, '_recruitment_environment_content_2_title', 'field_603db15963530'),
(1256, 202, 'recruitment_environment_content_3_image', '164'),
(1257, 202, '_recruitment_environment_content_3_image', 'field_603db1416352f'),
(1258, 202, 'recruitment_environment_content_3_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod'),
(1259, 202, '_recruitment_environment_content_3_title', 'field_603db15963530'),
(1260, 202, 'recruitment_bonus_title', 'Lương thưởng hấp dẫn'),
(1261, 202, '_recruitment_bonus_title', 'field_603db66402843'),
(1262, 202, 'recruitment_bonus_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1263, 202, '_recruitment_bonus_content_0_title', 'field_603db6bd02845'),
(1264, 202, 'recruitment_bonus_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1265, 202, '_recruitment_bonus_content_1_title', 'field_603db6bd02845'),
(1266, 202, 'recruitment_bonus_content', '2'),
(1267, 202, '_recruitment_bonus_content', 'field_603db6ad02844'),
(1268, 202, 'recruitment_bonus_image', '164'),
(1269, 202, '_recruitment_bonus_image', 'field_603db6d802846'),
(1270, 202, 'recruitment_welfare_title', 'Phúc lợi đầy đủ'),
(1271, 202, '_recruitment_welfare_title', 'field_603db6e102847'),
(1272, 202, 'recruitment_welfare_content_0_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1273, 202, '_recruitment_welfare_content_0_title', 'field_603db75702849'),
(1274, 202, 'recruitment_welfare_content_1_title', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed'),
(1275, 202, '_recruitment_welfare_content_1_title', 'field_603db75702849'),
(1276, 202, 'recruitment_welfare_content', '2'),
(1277, 202, '_recruitment_welfare_content', 'field_603db73602848'),
(1278, 202, 'recruitment_welfare_gallery', 'a:3:{i:0;s:3:\"163\";i:1;s:3:\"162\";i:2;s:3:\"161\";}'),
(1279, 202, '_recruitment_welfare_gallery', 'field_603db7640284a'),
(1280, 202, 'recruitment_title', 'LỢI ÍCH KHI LÀM VIỆC TẠI DANALUX'),
(1281, 202, '_recruitment_title', 'field_603db82aef31c'),
(1282, 202, 'recruitment_form', '[contact-form-7 id=\"179\" title=\"Form tuyển dụng\"]'),
(1283, 202, '_recruitment_form', 'field_603dc175c7674'),
(1284, 202, 'recruitment_envi_title', 'Môi trường làm việc'),
(1285, 202, '_recruitment_envi_title', 'field_603dc218c7677'),
(1286, 202, 'recruitment_envi_content_0_title', 'Môi trường làm việc 1'),
(1287, 202, '_recruitment_envi_content_0_title', 'field_603dc233c767a'),
(1288, 202, 'recruitment_envi_content_1_title', 'Môi trường làm việc 2'),
(1289, 202, '_recruitment_envi_content_1_title', 'field_603dc233c767a'),
(1290, 202, 'recruitment_envi_content', '2'),
(1291, 202, '_recruitment_envi_content', 'field_603dc233c7679'),
(1292, 202, 'recruitment_envi_image', '164'),
(1293, 202, '_recruitment_envi_image', 'field_603dc23fc767d'),
(1294, 202, 'recruitment_time', 'Thời gian làm việc'),
(1295, 202, '_recruitment_time', 'field_603dc220c7678'),
(1296, 202, 'recruitment_time_content_0_title', 'Thời gian làm việc 1'),
(1297, 202, '_recruitment_time_content_0_title', 'field_603dc238c767c'),
(1298, 202, 'recruitment_time_content_1_title', 'Thời gian làm việc 2'),
(1299, 202, '_recruitment_time_content_1_title', 'field_603dc238c767c'),
(1300, 202, 'recruitment_time_content', '2'),
(1301, 202, '_recruitment_time_content', 'field_603dc238c767b'),
(1302, 202, 'recruitment_time_image', '163'),
(1303, 202, '_recruitment_time_image', 'field_603dc244c767e'),
(1304, 203, '_form', '<label> Your name\n    [text* your-name] </label>\n\n<label> Your email\n    [email* your-email] </label>\n\n<label> Subject\n    [text* your-subject] </label>\n\n<label> Your message (optional)\n    [textarea your-message] </label>\n[file file-562]\n[submit \"Submit\"]'),
(1305, 203, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@danalux.local>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:163:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(1306, 203, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@danalux.local>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:105:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(1307, 203, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(1308, 203, '_additional_settings', ''),
(1309, 203, '_locale', 'vi'),
(1312, 204, '_edit_last', '1'),
(1313, 204, '_edit_lock', '1614851560:1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(3, 1, '2021-02-27 04:21:07', '2021-02-27 04:21:07', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://danalux.local.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you request a password reset, your IP address will be included in the reset email.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2021-02-27 04:21:07', '2021-02-27 04:21:07', '', 0, 'http://danalux.local/?page_id=3', 0, 'page', '', 0),
(4, 1, '2021-02-27 04:29:15', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2021-02-27 04:29:15', '0000-00-00 00:00:00', '', 0, 'http://danalux.local/?p=4', 0, 'post', '', 0),
(5, 1, '2021-02-27 11:33:05', '0000-00-00 00:00:00', '', 'Trang chủ', '', 'draft', 'closed', 'closed', '', '', '', '', '2021-02-27 11:33:05', '0000-00-00 00:00:00', '', 0, 'http://danalux.local/?p=5', 1, 'nav_menu_item', '', 0),
(9, 1, '2021-02-27 12:08:50', '2021-02-27 05:08:50', '', 'icon__logo', '', 'inherit', 'open', 'closed', '', 'icon__logo', '', '', '2021-02-27 12:08:50', '2021-02-27 05:08:50', '', 0, 'http://danalux.local/wp-content/uploads/2021/02/icon__logo.png', 0, 'attachment', 'image/png', 0),
(10, 1, '2021-02-27 12:09:21', '2021-02-27 05:09:21', '{\n    \"site_icon\": {\n        \"value\": 9,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2021-02-27 05:09:21\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '86e05786-8f20-4e2c-a546-ceaea7b60d85', '', '', '2021-02-27 12:09:21', '2021-02-27 05:09:21', '', 0, 'http://danalux.local/uncategorized/86e05786-8f20-4e2c-a546-ceaea7b60d85.html', 0, 'customize_changeset', '', 0),
(12, 1, '2021-02-27 12:30:27', '2021-02-27 05:30:27', '<div class=\"module__header\"><h2 class=\"title\">Thông tin liên hệ</h2></div>\r\n<div class=\"module__content\">\r\n<div class=\"row\">\r\n\r\n<div class=\"col-12\">\r\n<div class=\"form-group\">\r\n    [text* your-name class:form-control class:input__text placeholder \"Họ và tên\"]\r\n</div>\r\n</div>\r\n<div class=\"col-12 col-lg-6\">\r\n<div class=\"form-group\">\r\n    [email* your-email class:form-control class:input__text placeholder \"Email\"]\r\n</div>\r\n</div>\r\n<div class=\"col-12 col-lg-6\">\r\n<div class=\"form-group\">\r\n    [tel* your-tel class:form-control class:input__text placeholder \"Số điện thoại\"]\r\n</div>\r\n</div>\r\n<div class=\"col-12\">\r\n<div class=\"form-group\">\r\n    [textarea* your-message class:form-control class:textare__text placeholder \"Ghi chú\"]\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-12\">\r\n<div class=\"form-group\">\r\n    [text* your-date class:form-control class:input__date placeholder \"Date\"]\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-12\">\r\n    [submit class:btn class:btn__sen \"GỬI\"]\r\n</div>\r\n\r\n</div>\r\n</div>\n1\n[your-name] - Liên hệ\n[_site_title] <wordpress@danalux.local>\n[_site_admin_email]\nTừ: [your-name] <[your-email]>\r\nSố điện thoại: [your-tel]\r\n\r\nTin nhắn: [your-message]\r\n\r\nEmail được gửi từ [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@danalux.local>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Form liên hệ', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2021-03-05 11:44:13', '2021-03-05 04:44:13', '', 0, 'http://danalux.local/?post_type=wpcf7_contact_form&#038;p=12', 0, 'wpcf7_contact_form', '', 0),
(13, 1, '2021-02-27 13:00:04', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-02-27 13:00:04', '0000-00-00 00:00:00', '', 0, 'http://danalux.local/?post_type=dich-vu&p=13', 0, 'dich-vu', '', 0),
(14, 1, '2021-02-27 13:00:58', '2021-02-27 06:00:58', '', 'Bài dịch vụ 1', '', 'publish', 'open', 'closed', '', 'bai-dich-vu-1', '', '', '2021-02-27 13:00:58', '2021-02-27 06:00:58', '', 0, 'http://danalux.local/?post_type=dich-vu&#038;p=14', 0, 'dich-vu', '', 0),
(15, 1, '2021-03-01 09:38:25', '2021-03-01 02:38:25', '', 'Bài viết dịch vụ 1', '', 'publish', 'open', 'closed', '', 'bai-viet-dich-vu-1', '', '', '2021-03-01 09:43:16', '2021-03-01 02:43:16', '', 0, 'http://danalux.local/?post_type=dichvu&#038;p=15', 0, 'dichvu', '', 0),
(16, 1, '2021-03-01 09:58:13', '2021-03-01 02:58:13', '', 'Dự án chi tiết 1', '', 'publish', 'open', 'closed', '', 'du-an-chi-tiet-1', '', '', '2021-03-02 09:57:07', '2021-03-02 02:57:07', '', 0, 'http://danalux.local/?post_type=duan&#038;p=16', 0, 'duan', '', 0),
(17, 1, '2021-03-01 10:02:35', '2021-03-01 03:02:35', '', 'Bài viết dịch vụ 1', '', 'publish', 'open', 'closed', '', 'bai-viet-dich-vu-1', '', '', '2021-03-01 10:02:35', '2021-03-01 03:02:35', '', 0, 'http://danalux.local/?post_type=service&#038;p=17', 0, 'service', '', 0),
(18, 1, '2021-03-01 10:14:18', '2021-03-01 03:14:18', '', 'Bài viết dự án 1', '', 'publish', 'open', 'closed', '', 'bai-viet-du-an-1', '', '', '2021-03-01 16:27:43', '2021-03-01 09:27:43', '', 0, 'http://danalux.local/?post_type=project&#038;p=18', 0, 'project', '', 0),
(19, 1, '2021-03-01 10:14:51', '2021-03-01 03:14:51', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor', 'Bài viết tin tức 1', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor', 'publish', 'open', 'open', '', 'bai-viet-tin-tuc-1', '', '', '2021-03-01 17:17:59', '2021-03-01 10:17:59', '', 0, 'http://danalux.local/?p=19', 0, 'post', '', 0),
(20, 1, '2021-03-01 10:14:51', '2021-03-01 03:14:51', '', 'Bài viết tin tức 1', '', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2021-03-01 10:14:51', '2021-03-01 03:14:51', '', 19, 'http://danalux.local/uncategorized/19-revision-v1.html', 0, 'revision', '', 0),
(21, 1, '2021-03-01 10:15:16', '2021-03-01 03:15:16', '', 'Trang chủ', '', 'publish', 'closed', 'closed', '', 'trang-chu', '', '', '2021-03-04 10:56:42', '2021-03-04 03:56:42', '', 0, 'http://danalux.local/?page_id=21', 0, 'page', '', 0),
(22, 1, '2021-03-01 10:15:16', '2021-03-01 03:15:16', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2021-03-01 10:15:16', '2021-03-01 03:15:16', '', 21, 'http://danalux.local/uncategorized/21-revision-v1.html', 0, 'revision', '', 0),
(23, 1, '2021-03-01 10:15:24', '2021-03-01 03:15:24', 'Nội dung trang Giới thiệu', 'Giới thiệu', '', 'publish', 'closed', 'closed', '', 'gioi-thieu', '', '', '2021-03-01 13:47:34', '2021-03-01 06:47:34', '', 0, 'http://danalux.local/?page_id=23', 0, 'page', '', 0),
(24, 1, '2021-03-01 10:15:24', '2021-03-01 03:15:24', '', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '23-revision-v1', '', '', '2021-03-01 10:15:24', '2021-03-01 03:15:24', '', 23, 'http://danalux.local/uncategorized/23-revision-v1.html', 0, 'revision', '', 0),
(25, 1, '2021-03-01 10:15:31', '2021-03-01 03:15:31', '<h2><span style=\"color: #339966; font-size: 25px;\"><strong>CÔNG TY CỔ PHẦN TƯ VẤN VÀ ĐẦU TƯ DANALUX</strong></span></h2>', 'Liên hệ', '', 'publish', 'closed', 'closed', '', 'lien-he', '', '', '2021-03-01 15:13:40', '2021-03-01 08:13:40', '', 0, 'http://danalux.local/?page_id=25', 0, 'page', '', 0),
(26, 1, '2021-03-01 10:15:31', '2021-03-01 03:15:31', '', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2021-03-01 10:15:31', '2021-03-01 03:15:31', '', 25, 'http://danalux.local/uncategorized/25-revision-v1.html', 0, 'revision', '', 0),
(27, 1, '2021-03-01 10:16:41', '2021-03-01 03:16:41', '', 'Tuyển dụng', '', 'publish', 'closed', 'closed', '', 'tuyen-dung', '', '', '2021-03-04 11:05:34', '2021-03-04 04:05:34', '', 0, 'http://danalux.local/?page_id=27', 0, 'page', '', 0),
(28, 1, '2021-03-01 10:16:41', '2021-03-01 03:16:41', '', 'Tuyển dụng', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2021-03-01 10:16:41', '2021-03-01 03:16:41', '', 27, 'http://danalux.local/uncategorized/27-revision-v1.html', 0, 'revision', '', 0),
(29, 1, '2021-03-01 10:18:16', '2021-03-01 03:18:16', '', 'Bài viết tuyển dụng 1', '', 'publish', 'open', 'closed', '', 'bai-viet-tuyen-dung-1', '', '', '2021-03-01 10:18:16', '2021-03-01 03:18:16', '', 0, 'http://danalux.local/?post_type=recruitment&#038;p=29', 0, 'recruitment', '', 0),
(30, 1, '2021-03-01 10:22:01', '2021-03-01 03:22:01', ' ', '', '', 'publish', 'closed', 'closed', '', '30', '', '', '2021-03-05 16:11:20', '2021-03-05 09:11:20', '', 0, 'http://danalux.local/?p=30', 1, 'nav_menu_item', '', 0),
(31, 1, '2021-03-01 10:22:01', '2021-03-01 03:22:01', ' ', '', '', 'publish', 'closed', 'closed', '', '31', '', '', '2021-03-05 16:11:20', '2021-03-05 09:11:20', '', 0, 'http://danalux.local/?p=31', 2, 'nav_menu_item', '', 0),
(32, 1, '2021-03-01 10:22:01', '2021-03-01 03:22:01', ' ', '', '', 'publish', 'closed', 'closed', '', '32', '', '', '2021-03-05 16:11:20', '2021-03-05 09:11:20', '', 0, 'http://danalux.local/?p=32', 7, 'nav_menu_item', '', 0),
(33, 1, '2021-03-01 10:22:01', '2021-03-01 03:22:01', ' ', '', '', 'publish', 'closed', 'closed', '', '33', '', '', '2021-03-05 16:11:20', '2021-03-05 09:11:20', '', 0, 'http://danalux.local/?p=33', 6, 'nav_menu_item', '', 0),
(36, 1, '2021-03-01 10:23:20', '2021-03-01 03:23:20', ' ', '', '', 'publish', 'closed', 'closed', '', '36', '', '', '2021-03-05 16:11:20', '2021-03-05 09:11:20', '', 0, 'http://danalux.local/?p=36', 5, 'nav_menu_item', '', 0),
(37, 1, '2021-03-01 10:44:20', '0000-00-00 00:00:00', '', 'Lưu bản nháp tự động', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2021-03-01 10:44:20', '0000-00-00 00:00:00', '', 0, 'http://danalux.local/?post_type=acf-field-group&p=37', 0, 'acf-field-group', '', 0),
(38, 1, '2021-03-01 11:22:41', '2021-03-01 04:22:41', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"theme-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Header', 'header', 'publish', 'closed', 'closed', '', 'group_603c6b2e1c64d', '', '', '2021-03-01 11:55:34', '2021-03-01 04:55:34', '', 0, 'http://danalux.local/?post_type=acf-field-group&#038;p=38', 0, 'acf-field-group', '', 0),
(39, 1, '2021-03-01 11:22:41', '2021-03-01 04:22:41', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Logo', 'logo', 'publish', 'closed', 'closed', '', 'field_603c6b5b90fd3', '', '', '2021-03-01 11:22:41', '2021-03-01 04:22:41', '', 38, 'http://danalux.local/?post_type=acf-field&p=39', 0, 'acf-field', '', 0),
(40, 1, '2021-03-01 11:22:41', '2021-03-01 04:22:41', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh', 'h_logo', 'publish', 'closed', 'closed', '', 'field_603c6b6490fd4', '', '', '2021-03-01 11:22:41', '2021-03-01 04:22:41', '', 38, 'http://danalux.local/?post_type=acf-field&p=40', 1, 'acf-field', '', 0),
(41, 1, '2021-03-01 11:22:41', '2021-03-01 04:22:41', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Gmail', '', 'publish', 'closed', 'closed', '', 'field_603c6b8e90fd5', '', '', '2021-03-01 11:22:41', '2021-03-01 04:22:41', '', 38, 'http://danalux.local/?post_type=acf-field&p=41', 2, 'acf-field', '', 0),
(42, 1, '2021-03-01 11:22:41', '2021-03-01 04:22:41', 'a:9:{s:4:\"type\";s:5:\"email\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";}', 'Gmail', 'h_gmail', 'publish', 'closed', 'closed', '', 'field_603c6bb490fd6', '', '', '2021-03-01 11:22:41', '2021-03-01 04:22:41', '', 38, 'http://danalux.local/?post_type=acf-field&p=42', 3, 'acf-field', '', 0),
(43, 1, '2021-03-01 11:22:41', '2021-03-01 04:22:41', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Phone', 'phone', 'publish', 'closed', 'closed', '', 'field_603c6bdd90fd7', '', '', '2021-03-01 11:22:41', '2021-03-01 04:22:41', '', 38, 'http://danalux.local/?post_type=acf-field&p=43', 4, 'acf-field', '', 0),
(44, 1, '2021-03-01 11:22:41', '2021-03-01 04:22:41', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Phone', 'h_phone', 'publish', 'closed', 'closed', '', 'field_603c6be390fd8', '', '', '2021-03-01 11:22:41', '2021-03-01 04:22:41', '', 38, 'http://danalux.local/?post_type=acf-field&p=44', 5, 'acf-field', '', 0),
(45, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"theme-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Footer', 'footer', 'publish', 'closed', 'closed', '', 'group_603c6d574ad44', '', '', '2021-03-01 11:37:26', '2021-03-01 04:37:26', '', 0, 'http://danalux.local/?post_type=acf-field-group&#038;p=45', 0, 'acf-field-group', '', 0),
(46, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Logo footer', 'logo_footer', 'publish', 'closed', 'closed', '', 'field_603c6d5c94512', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 45, 'http://danalux.local/?post_type=acf-field&p=46', 0, 'acf-field', '', 0),
(47, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh', 'f_logo', 'publish', 'closed', 'closed', '', 'field_603c6d8294513', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 45, 'http://danalux.local/?post_type=acf-field&p=47', 1, 'acf-field', '', 0),
(48, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Slogan', 'slogan', 'publish', 'closed', 'closed', '', 'field_603c6e1394514', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 45, 'http://danalux.local/?post_type=acf-field&p=48', 2, 'acf-field', '', 0),
(49, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:6;s:9:\"new_lines\";s:0:\"\";}', 'Nội dung', 'f_slogan', 'publish', 'closed', 'closed', '', 'field_603c6e1994515', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 45, 'http://danalux.local/?post_type=acf-field&p=49', 3, 'acf-field', '', 0),
(50, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Địa chỉ', 'dịa_chỉ', 'publish', 'closed', 'closed', '', 'field_603c6e3894516', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 45, 'http://danalux.local/?post_type=acf-field&p=50', 4, 'acf-field', '', 0),
(51, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Nội dung', 'f_address', 'publish', 'closed', 'closed', '', 'field_603c6e3f94517', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 45, 'http://danalux.local/?post_type=acf-field&p=51', 5, 'acf-field', '', 0),
(52, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Phone', 'phone', 'publish', 'closed', 'closed', '', 'field_603c6e4b94518', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 45, 'http://danalux.local/?post_type=acf-field&p=52', 6, 'acf-field', '', 0),
(53, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Nội dung', 'f_phone', 'publish', 'closed', 'closed', '', 'field_603c6e5994519', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 45, 'http://danalux.local/?post_type=acf-field&p=53', 7, 'acf-field', '', 0),
(54, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Gmail', 'email', 'publish', 'closed', 'closed', '', 'field_603c6e6d9451a', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 45, 'http://danalux.local/?post_type=acf-field&p=54', 8, 'acf-field', '', 0),
(55, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:9:{s:4:\"type\";s:5:\"email\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";}', 'Nội dung', 'f_gmail', 'publish', 'closed', 'closed', '', 'field_603c6e759451b', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 45, 'http://danalux.local/?post_type=acf-field&p=55', 9, 'acf-field', '', 0),
(56, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Khám phá', 'kham_pha', 'publish', 'closed', 'closed', '', 'field_603c6e999451c', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 45, 'http://danalux.local/?post_type=acf-field&p=56', 10, 'acf-field', '', 0),
(57, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'f_menu_title', 'publish', 'closed', 'closed', '', 'field_603c6ea89451d', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 45, 'http://danalux.local/?post_type=acf-field&p=57', 11, 'acf-field', '', 0),
(58, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Nội dung', 'f_menu_content', 'publish', 'closed', 'closed', '', 'field_603c6ed79451e', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 45, 'http://danalux.local/?post_type=acf-field&p=58', 12, 'acf-field', '', 0),
(59, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_603c6f079451f', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 58, 'http://danalux.local/?post_type=acf-field&p=59', 0, 'acf-field', '', 0),
(60, 1, '2021-03-01 11:36:21', '2021-03-01 04:36:21', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'Đường dẫn', 'link', 'publish', 'closed', 'closed', '', 'field_603c6f1094520', '', '', '2021-03-01 11:36:21', '2021-03-01 04:36:21', '', 58, 'http://danalux.local/?post_type=acf-field&p=60', 1, 'acf-field', '', 0),
(61, 1, '2021-03-01 11:37:26', '2021-03-01 04:37:26', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Bản đồ', 'bản_dồ', 'publish', 'closed', 'closed', '', 'field_603c6f4b5f621', '', '', '2021-03-01 11:37:26', '2021-03-01 04:37:26', '', 45, 'http://danalux.local/?post_type=acf-field&p=61', 13, 'acf-field', '', 0),
(62, 1, '2021-03-01 11:37:26', '2021-03-01 04:37:26', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:6;s:9:\"new_lines\";s:0:\"\";}', 'Nhúng toạ độ google', 'f_map_iframe', 'publish', 'closed', 'closed', '', 'field_603c6f5d5f622', '', '', '2021-03-01 11:37:26', '2021-03-01 04:37:26', '', 45, 'http://danalux.local/?post_type=acf-field&p=62', 14, 'acf-field', '', 0),
(63, 1, '2021-03-01 11:39:33', '2021-03-01 04:39:33', '', 'icon__footer--logo', '', 'inherit', 'open', 'closed', '', 'icon__footer-logo', '', '', '2021-03-01 11:39:33', '2021-03-01 04:39:33', '', 0, 'http://danalux.local/wp-content/uploads/2021/03/icon__footer-logo.png', 0, 'attachment', 'image/png', 0),
(64, 1, '2021-03-01 11:55:34', '2021-03-01 04:55:34', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Banner', 'banner', 'publish', 'closed', 'closed', '', 'field_603c72d60ac65', '', '', '2021-03-01 11:55:34', '2021-03-01 04:55:34', '', 38, 'http://danalux.local/?post_type=acf-field&p=64', 6, 'acf-field', '', 0),
(65, 1, '2021-03-01 11:55:34', '2021-03-01 04:55:34', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Nội dung', 'home_slide', 'publish', 'closed', 'closed', '', 'field_603c72f40ac66', '', '', '2021-03-01 11:55:34', '2021-03-01 04:55:34', '', 38, 'http://danalux.local/?post_type=acf-field&p=65', 7, 'acf-field', '', 0),
(66, 1, '2021-03-01 11:55:34', '2021-03-01 04:55:34', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh', 'image', 'publish', 'closed', 'closed', '', 'field_603c73780ac67', '', '', '2021-03-01 11:55:34', '2021-03-01 04:55:34', '', 65, 'http://danalux.local/?post_type=acf-field&p=66', 0, 'acf-field', '', 0),
(67, 1, '2021-03-01 11:55:34', '2021-03-01 04:55:34', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'Đường dẫn', 'link', 'publish', 'closed', 'closed', '', 'field_603c738b0ac68', '', '', '2021-03-01 11:55:34', '2021-03-01 04:55:34', '', 65, 'http://danalux.local/?post_type=acf-field&p=67', 1, 'acf-field', '', 0),
(68, 1, '2021-03-01 11:56:13', '2021-03-01 04:56:13', '', 'slide__1', '', 'inherit', 'open', 'closed', '', 'slide__1', '', '', '2021-03-01 11:56:13', '2021-03-01 04:56:13', '', 0, 'http://danalux.local/wp-content/uploads/2021/03/slide__1.jpg', 0, 'attachment', 'image/jpeg', 0),
(69, 1, '2021-03-01 13:47:34', '2021-03-01 06:47:34', 'Nội dung trang Giới thiệu', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '23-revision-v1', '', '', '2021-03-01 13:47:34', '2021-03-01 06:47:34', '', 23, 'http://danalux.local/uncategorized/23-revision-v1.html', 0, 'revision', '', 0),
(70, 1, '2021-03-01 13:55:41', '2021-03-01 06:55:41', '<h2 class=\"contact__title\">\r\n    Công ty cổ phần tư vấn và đầu tư Danalux\r\n</h2>\r\n<div class=\"contact__info\">\r\n    <div class=\"info__group\">\r\n        <div class=\"info__item\">\r\n            <h3 class=\"info__title\">\r\n                Văn phòng tại:\r\n            </h3>\r\n            <p class=\"info__text\">\r\n                20 Nguyễn Văn Tố, P. Thuận Phước, Q.Hải Châu,\r\n                Thành phố Đà Nẵng. <br />\r\n                <b> Hotline:</b> 0905 133 077\r\n            </p>\r\n        </div>\r\n    </div>\r\n    <div class=\"info__group\">\r\n        <div class=\"info__item\">\r\n            <h3 class=\"info__title\">\r\n                EMAIL LIÊN HỆ:\r\n            </h3>\r\n            <p class=\"info__text\">\r\n                <b> Email:</b> Danalux@gmail.com\r\n            </p>\r\n        </div>\r\n        <div class=\"info__item\">\r\n            <h3 class=\"info__title\">\r\n                giờ làm việc:\r\n            </h3>\r\n            <p class=\"info__text\">\r\n                <b> Sáng:</b> 8h00 – 12h00<br />\r\n                <b> Chiều:</b> 13h00 – 17h00\r\n            </p>\r\n        </div>\r\n    </div>\r\n</div>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2021-03-01 13:55:41', '2021-03-01 06:55:41', '', 25, 'http://danalux.local/uncategorized/25-revision-v1.html', 0, 'revision', '', 0),
(71, 1, '2021-03-01 14:15:42', '2021-03-01 07:15:42', '<h2 class=\"contact__title\">Công ty cổ phần tư vấn và đầu tư Danalux</h2>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2021-03-01 14:15:42', '2021-03-01 07:15:42', '', 25, 'http://danalux.local/uncategorized/25-revision-v1.html', 0, 'revision', '', 0),
(72, 1, '2021-03-01 14:17:55', '2021-03-01 07:17:55', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:20:\"template-contact.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Nhóm trường trang Liên hệ', 'nhom-truong-trang-lien-he', 'publish', 'closed', 'closed', '', 'group_603c94c78222a', '', '', '2021-03-01 14:20:44', '2021-03-01 07:20:44', '', 0, 'http://danalux.local/?post_type=acf-field-group&#038;p=72', 0, 'acf-field-group', '', 0),
(73, 1, '2021-03-01 14:17:55', '2021-03-01 07:17:55', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'VĂN PHÒNG TẠI:', 'van_phong_tại:', 'publish', 'closed', 'closed', '', 'field_603c94dda3c4f', '', '', '2021-03-01 14:17:55', '2021-03-01 07:17:55', '', 72, 'http://danalux.local/?post_type=acf-field&p=73', 0, 'acf-field', '', 0),
(74, 1, '2021-03-01 14:17:55', '2021-03-01 07:17:55', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Nội dung', 'contact_address', 'publish', 'closed', 'closed', '', 'field_603c94fea3c50', '', '', '2021-03-01 14:17:55', '2021-03-01 07:17:55', '', 72, 'http://danalux.local/?post_type=acf-field&p=74', 1, 'acf-field', '', 0),
(75, 1, '2021-03-01 14:20:44', '2021-03-01 07:20:44', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'EMAIL LIÊN HỆ:', 'email_lien_hệ:', 'publish', 'closed', 'closed', '', 'field_603c9546c47ea', '', '', '2021-03-01 14:20:44', '2021-03-01 07:20:44', '', 72, 'http://danalux.local/?post_type=acf-field&p=75', 2, 'acf-field', '', 0),
(76, 1, '2021-03-01 14:20:44', '2021-03-01 07:20:44', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Nội dung', 'contact_email', 'publish', 'closed', 'closed', '', 'field_603c954ac47eb', '', '', '2021-03-01 14:20:44', '2021-03-01 07:20:44', '', 72, 'http://danalux.local/?post_type=acf-field&p=76', 3, 'acf-field', '', 0),
(77, 1, '2021-03-01 14:20:44', '2021-03-01 07:20:44', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'GIỜ LÀM VIỆC:', 'giờ_lam_việc:', 'publish', 'closed', 'closed', '', 'field_603c9564c47ec', '', '', '2021-03-01 14:20:44', '2021-03-01 07:20:44', '', 72, 'http://danalux.local/?post_type=acf-field&p=77', 4, 'acf-field', '', 0),
(78, 1, '2021-03-01 14:20:44', '2021-03-01 07:20:44', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Nội dung', 'contact_work', 'publish', 'closed', 'closed', '', 'field_603c956bc47ed', '', '', '2021-03-01 14:20:44', '2021-03-01 07:20:44', '', 72, 'http://danalux.local/?post_type=acf-field&p=78', 5, 'acf-field', '', 0),
(79, 1, '2021-03-01 14:20:44', '2021-03-01 07:20:44', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Bản đồ tới văn phòng công ty', 'bản_dồ_tới_van_phong_cong_ty', 'publish', 'closed', 'closed', '', 'field_603c9587c47ee', '', '', '2021-03-01 14:20:44', '2021-03-01 07:20:44', '', 72, 'http://danalux.local/?post_type=acf-field&p=79', 6, 'acf-field', '', 0),
(80, 1, '2021-03-01 14:20:44', '2021-03-01 07:20:44', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Nội dung', 'contact_map', 'publish', 'closed', 'closed', '', 'field_603c958cc47ef', '', '', '2021-03-01 14:20:44', '2021-03-01 07:20:44', '', 72, 'http://danalux.local/?post_type=acf-field&p=80', 7, 'acf-field', '', 0),
(81, 1, '2021-03-01 14:20:44', '2021-03-01 07:20:44', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Form liên hệ', 'form_lien_hệ', 'publish', 'closed', 'closed', '', 'field_603c959fc47f0', '', '', '2021-03-01 14:20:44', '2021-03-01 07:20:44', '', 72, 'http://danalux.local/?post_type=acf-field&p=81', 8, 'acf-field', '', 0),
(82, 1, '2021-03-01 14:20:44', '2021-03-01 07:20:44', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Nội dung', 'contact_form', 'publish', 'closed', 'closed', '', 'field_603c95bbc47f1', '', '', '2021-03-01 14:20:44', '2021-03-01 07:20:44', '', 72, 'http://danalux.local/?post_type=acf-field&p=82', 9, 'acf-field', '', 0),
(83, 1, '2021-03-01 14:23:58', '2021-03-01 07:23:58', '<h2 class=\"contact__title\">Công ty cổ phần tư vấn và đầu tư Danalux</h2>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2021-03-01 14:23:58', '2021-03-01 07:23:58', '', 25, 'http://danalux.local/uncategorized/25-revision-v1.html', 0, 'revision', '', 0),
(84, 1, '2021-03-01 15:03:36', '2021-03-01 08:03:36', '<h2 class=\"contact__title\">Công ty cổ phần tư vấn và đầu tư Danalux</h2>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2021-03-01 15:03:36', '2021-03-01 08:03:36', '', 25, 'http://danalux.local/uncategorized/25-revision-v1.html', 0, 'revision', '', 0),
(85, 1, '2021-03-01 15:06:03', '2021-03-01 08:06:03', '<h2><span style=\"color: #00ff00;\">CÔNG TY CỔ PHẦN TƯ VẤN VÀ ĐẦU TƯ DANALUX</span></h2>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '25-autosave-v1', '', '', '2021-03-01 15:06:03', '2021-03-01 08:06:03', '', 25, 'http://danalux.local/uncategorized/25-autosave-v1.html', 0, 'revision', '', 0),
(86, 1, '2021-03-01 15:04:54', '2021-03-01 08:04:54', '<h1><span style=\"color: #00ff00;\"><strong>Công ty cổ phần tư vấn và đầu tư Danalux</strong></span></h1>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2021-03-01 15:04:54', '2021-03-01 08:04:54', '', 25, 'http://danalux.local/uncategorized/25-revision-v1.html', 0, 'revision', '', 0),
(87, 1, '2021-03-01 15:06:08', '2021-03-01 08:06:08', '<h2><span style=\"color: #00ff00;\">CÔNG TY CỔ PHẦN TƯ VẤN VÀ ĐẦU TƯ DANALUX </span></h2>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2021-03-01 15:06:08', '2021-03-01 08:06:08', '', 25, 'http://danalux.local/uncategorized/25-revision-v1.html', 0, 'revision', '', 0),
(88, 1, '2021-03-01 15:06:39', '2021-03-01 08:06:39', '<h2><span style=\"color: #339966;\"><strong>CÔNG TY CỔ PHẦN TƯ VẤN VÀ ĐẦU TƯ DANALUX</strong></span></h2>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2021-03-01 15:06:39', '2021-03-01 08:06:39', '', 25, 'http://danalux.local/uncategorized/25-revision-v1.html', 0, 'revision', '', 0),
(89, 1, '2021-03-01 15:12:50', '2021-03-01 08:12:50', '<h2><span style=\"color: #339966; font-size: 28px;\"><strong>CÔNG TY CỔ PHẦN TƯ VẤN VÀ ĐẦU TƯ DANALUX</strong></span></h2>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2021-03-01 15:12:50', '2021-03-01 08:12:50', '', 25, 'http://danalux.local/uncategorized/25-revision-v1.html', 0, 'revision', '', 0),
(90, 1, '2021-03-01 15:13:12', '2021-03-01 08:13:12', '<h2><span style=\"color: #339966; font-size: 26px;\"><strong>CÔNG TY CỔ PHẦN TƯ VẤN VÀ ĐẦU TƯ DANALUX</strong></span></h2>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2021-03-01 15:13:12', '2021-03-01 08:13:12', '', 25, 'http://danalux.local/uncategorized/25-revision-v1.html', 0, 'revision', '', 0),
(91, 1, '2021-03-01 15:13:40', '2021-03-01 08:13:40', '<h2><span style=\"color: #339966; font-size: 25px;\"><strong>CÔNG TY CỔ PHẦN TƯ VẤN VÀ ĐẦU TƯ DANALUX</strong></span></h2>', 'Liên hệ', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2021-03-01 15:13:40', '2021-03-01 08:13:40', '', 25, 'http://danalux.local/uncategorized/25-revision-v1.html', 0, 'revision', '', 0),
(93, 1, '2021-03-01 15:25:32', '2021-03-01 08:25:32', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"page_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:10:\"front_page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Nhóm trường Trang chủ', 'nhom-truong-trang-chu', 'publish', 'closed', 'closed', '', 'group_603ca39e9e796', '', '', '2021-03-04 10:56:34', '2021-03-04 03:56:34', '', 0, 'http://danalux.local/?post_type=acf-field-group&#038;p=93', 0, 'acf-field-group', '', 0),
(94, 1, '2021-03-01 15:25:32', '2021-03-01 08:25:32', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Giới thiệu chung', 'giới_thiệu_chung', 'publish', 'closed', 'closed', '', 'field_603ca3b378a2f', '', '', '2021-03-01 15:25:32', '2021-03-01 08:25:32', '', 93, 'http://danalux.local/?post_type=acf-field&p=94', 0, 'acf-field', '', 0),
(95, 1, '2021-03-01 15:25:32', '2021-03-01 08:25:32', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'home_intro_title', 'publish', 'closed', 'closed', '', 'field_603ca3be78a30', '', '', '2021-03-01 15:25:32', '2021-03-01 08:25:32', '', 93, 'http://danalux.local/?post_type=acf-field&p=95', 1, 'acf-field', '', 0),
(96, 1, '2021-03-01 15:25:32', '2021-03-01 08:25:32', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Mô tả', 'home_intro_desc', 'publish', 'closed', 'closed', '', 'field_603ca41d78a31', '', '', '2021-03-01 15:25:32', '2021-03-01 08:25:32', '', 93, 'http://danalux.local/?post_type=acf-field&p=96', 2, 'acf-field', '', 0),
(97, 1, '2021-03-01 15:25:32', '2021-03-01 08:25:32', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh', 'home_intro_image', 'publish', 'closed', 'closed', '', 'field_603ca43c78a32', '', '', '2021-03-01 15:25:32', '2021-03-01 08:25:32', '', 93, 'http://danalux.local/?post_type=acf-field&p=97', 3, 'acf-field', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(98, 1, '2021-03-01 15:25:32', '2021-03-01 08:25:32', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Dịch vụ', 'dịch_vụ', 'publish', 'closed', 'closed', '', 'field_603ca46178a33', '', '', '2021-03-01 15:25:32', '2021-03-01 08:25:32', '', 93, 'http://danalux.local/?post_type=acf-field&p=98', 4, 'acf-field', '', 0),
(99, 1, '2021-03-01 15:25:32', '2021-03-01 08:25:32', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'home_service_title', 'publish', 'closed', 'closed', '', 'field_603ca46a78a34', '', '', '2021-03-01 15:25:32', '2021-03-01 08:25:32', '', 93, 'http://danalux.local/?post_type=acf-field&p=99', 5, 'acf-field', '', 0),
(100, 1, '2021-03-01 15:25:32', '2021-03-01 08:25:32', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";i:3;s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Nội dung', 'home_service_content', 'publish', 'closed', 'closed', '', 'field_603ca47778a35', '', '', '2021-03-01 15:35:42', '2021-03-01 08:35:42', '', 93, 'http://danalux.local/?post_type=acf-field&#038;p=100', 6, 'acf-field', '', 0),
(101, 1, '2021-03-01 15:25:32', '2021-03-01 08:25:32', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_603ca49478a36', '', '', '2021-03-01 15:25:32', '2021-03-01 08:25:32', '', 100, 'http://danalux.local/?post_type=acf-field&p=101', 0, 'acf-field', '', 0),
(102, 1, '2021-03-01 15:25:32', '2021-03-01 08:25:32', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh', 'image', 'publish', 'closed', 'closed', '', 'field_603ca4b278a37', '', '', '2021-03-01 15:25:32', '2021-03-01 08:25:32', '', 100, 'http://danalux.local/?post_type=acf-field&p=102', 1, 'acf-field', '', 0),
(103, 1, '2021-03-01 15:25:32', '2021-03-01 08:25:32', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'Đường dẫn', 'link', 'publish', 'closed', 'closed', '', 'field_603ca4d178a38', '', '', '2021-03-01 15:25:32', '2021-03-01 08:25:32', '', 100, 'http://danalux.local/?post_type=acf-field&p=103', 2, 'acf-field', '', 0),
(104, 1, '2021-03-01 15:29:18', '2021-03-01 08:29:18', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Dự án', 'dự_an', 'publish', 'closed', 'closed', '', 'field_603ca509296cb', '', '', '2021-03-01 15:29:18', '2021-03-01 08:29:18', '', 93, 'http://danalux.local/?post_type=acf-field&p=104', 7, 'acf-field', '', 0),
(105, 1, '2021-03-01 15:29:18', '2021-03-01 08:29:18', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'home_project_title', 'publish', 'closed', 'closed', '', 'field_603ca51f296cc', '', '', '2021-03-01 15:29:18', '2021-03-01 08:29:18', '', 93, 'http://danalux.local/?post_type=acf-field&p=105', 8, 'acf-field', '', 0),
(106, 1, '2021-03-01 15:29:18', '2021-03-01 08:29:18', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Mô tả', 'home_project_desc', 'publish', 'closed', 'closed', '', 'field_603ca53f296cd', '', '', '2021-03-01 15:29:18', '2021-03-01 08:29:18', '', 93, 'http://danalux.local/?post_type=acf-field&p=106', 9, 'acf-field', '', 0),
(107, 1, '2021-03-01 15:29:18', '2021-03-01 08:29:18', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Quảng cáo', 'quảng_cao', 'publish', 'closed', 'closed', '', 'field_603ca571296ce', '', '', '2021-03-01 16:21:38', '2021-03-01 09:21:38', '', 93, 'http://danalux.local/?post_type=acf-field&#038;p=107', 11, 'acf-field', '', 0),
(108, 1, '2021-03-01 15:29:18', '2021-03-01 08:29:18', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";i:4;s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Nội dung', 'home_ads_content', 'publish', 'closed', 'closed', '', 'field_603ca57c296cf', '', '', '2021-03-01 16:21:38', '2021-03-01 09:21:38', '', 93, 'http://danalux.local/?post_type=acf-field&#038;p=108', 12, 'acf-field', '', 0),
(109, 1, '2021-03-01 15:29:18', '2021-03-01 08:29:18', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_603ca5a1296d0', '', '', '2021-03-01 15:35:42', '2021-03-01 08:35:42', '', 108, 'http://danalux.local/?post_type=acf-field&#038;p=109', 1, 'acf-field', '', 0),
(110, 1, '2021-03-01 15:29:18', '2021-03-01 08:29:18', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh', 'image', 'publish', 'closed', 'closed', '', 'field_603ca5a9296d1', '', '', '2021-03-01 15:35:42', '2021-03-01 08:35:42', '', 108, 'http://danalux.local/?post_type=acf-field&#038;p=110', 0, 'acf-field', '', 0),
(111, 1, '2021-03-01 15:29:18', '2021-03-01 08:29:18', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Mô tả', 'desc', 'publish', 'closed', 'closed', '', 'field_603ca5bd296d2', '', '', '2021-03-01 15:29:18', '2021-03-01 08:29:18', '', 108, 'http://danalux.local/?post_type=acf-field&p=111', 2, 'acf-field', '', 0),
(112, 1, '2021-03-01 15:35:42', '2021-03-01 08:35:42', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Video', 'video', 'publish', 'closed', 'closed', '', 'field_603ca61e863f3', '', '', '2021-03-01 16:21:38', '2021-03-01 09:21:38', '', 93, 'http://danalux.local/?post_type=acf-field&#038;p=112', 13, 'acf-field', '', 0),
(113, 1, '2021-03-01 15:35:42', '2021-03-01 08:35:42', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'home_intro_video_title', 'publish', 'closed', 'closed', '', 'field_603ca62d863f4', '', '', '2021-03-01 16:21:38', '2021-03-01 09:21:38', '', 93, 'http://danalux.local/?post_type=acf-field&#038;p=113', 14, 'acf-field', '', 0),
(114, 1, '2021-03-01 15:35:42', '2021-03-01 08:35:42', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:6;s:9:\"new_lines\";s:0:\"\";}', 'Video', 'home_intro_video_iframe', 'publish', 'closed', 'closed', '', 'field_603ca65a863f6', '', '', '2021-03-04 10:53:43', '2021-03-04 03:53:43', '', 93, 'http://danalux.local/?post_type=acf-field&#038;p=114', 16, 'acf-field', '', 0),
(115, 1, '2021-03-01 15:35:42', '2021-03-01 08:35:42', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:6;s:9:\"new_lines\";s:0:\"\";}', 'Mô tả', 'home_intro_video_desc', 'publish', 'closed', 'closed', '', 'field_603ca642863f5', '', '', '2021-03-04 10:53:43', '2021-03-04 03:53:43', '', 93, 'http://danalux.local/?post_type=acf-field&#038;p=115', 17, 'acf-field', '', 0),
(116, 1, '2021-03-01 15:35:42', '2021-03-01 08:35:42', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Tin tức', 'tin_tức', 'publish', 'closed', 'closed', '', 'field_603ca683863f7', '', '', '2021-03-04 10:53:43', '2021-03-04 03:53:43', '', 93, 'http://danalux.local/?post_type=acf-field&#038;p=116', 18, 'acf-field', '', 0),
(117, 1, '2021-03-01 15:35:42', '2021-03-01 08:35:42', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'home_news_title', 'publish', 'closed', 'closed', '', 'field_603ca6ac863f8', '', '', '2021-03-04 10:53:43', '2021-03-04 03:53:43', '', 93, 'http://danalux.local/?post_type=acf-field&#038;p=117', 19, 'acf-field', '', 0),
(118, 1, '2021-03-01 15:35:42', '2021-03-01 08:35:42', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:4:\"post\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:1:{i:0;s:6:\"search\";}s:8:\"elements\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";i:3;s:13:\"return_format\";s:6:\"object\";}', 'Nội dung', 'home_news_selectpost', 'publish', 'closed', 'closed', '', 'field_603ca6ca863f9', '', '', '2021-03-04 10:56:34', '2021-03-04 03:56:34', '', 93, 'http://danalux.local/?post_type=acf-field&#038;p=118', 20, 'acf-field', '', 0),
(119, 1, '2021-03-01 15:35:42', '2021-03-01 08:35:42', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề xem thêm', 'home_news_readmore', 'publish', 'closed', 'closed', '', 'field_603ca726863fa', '', '', '2021-03-04 10:53:43', '2021-03-04 03:53:43', '', 93, 'http://danalux.local/?post_type=acf-field&#038;p=119', 21, 'acf-field', '', 0),
(120, 1, '2021-03-01 15:35:42', '2021-03-01 08:35:42', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:8:\"https://\";}', 'Đường dẫn xem thêm', 'home_news_readmore_url', 'publish', 'closed', 'closed', '', 'field_603ca73e863fb', '', '', '2021-03-04 10:53:43', '2021-03-04 03:53:43', '', 93, 'http://danalux.local/?post_type=acf-field&#038;p=120', 22, 'acf-field', '', 0),
(121, 1, '2021-03-01 15:44:20', '2021-03-01 08:44:20', '', 'about__1', '', 'inherit', 'open', 'closed', '', 'about__1', '', '', '2021-03-01 15:44:20', '2021-03-01 08:44:20', '', 21, 'http://danalux.local/wp-content/uploads/2021/03/about__1.jpg', 0, 'attachment', 'image/jpeg', 0),
(122, 1, '2021-03-01 15:46:15', '2021-03-01 08:46:15', '', 'service__1', '', 'inherit', 'open', 'closed', '', 'service__1', '', '', '2021-03-01 15:46:15', '2021-03-01 08:46:15', '', 21, 'http://danalux.local/wp-content/uploads/2021/03/service__1.jpg', 0, 'attachment', 'image/jpeg', 0),
(123, 1, '2021-03-01 15:47:38', '2021-03-01 08:47:38', '', 'service__2', '', 'inherit', 'open', 'closed', '', 'service__2', '', '', '2021-03-01 15:47:38', '2021-03-01 08:47:38', '', 21, 'http://danalux.local/wp-content/uploads/2021/03/service__2.jpg', 0, 'attachment', 'image/jpeg', 0),
(124, 1, '2021-03-01 15:50:52', '2021-03-01 08:50:52', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2021-03-01 15:50:52', '2021-03-01 08:50:52', '', 21, 'http://danalux.local/uncategorized/21-revision-v1.html', 0, 'revision', '', 0),
(125, 1, '2021-03-01 15:51:04', '2021-03-01 08:51:04', '', 'service__3', '', 'inherit', 'open', 'closed', '', 'service__3', '', '', '2021-03-01 15:51:04', '2021-03-01 08:51:04', '', 21, 'http://danalux.local/wp-content/uploads/2021/03/service__3.jpg', 0, 'attachment', 'image/jpeg', 0),
(126, 1, '2021-03-01 15:51:35', '2021-03-01 08:51:35', '', 'icon__field--1', '', 'inherit', 'open', 'closed', '', 'icon__field-1', '', '', '2021-03-01 15:51:35', '2021-03-01 08:51:35', '', 21, 'http://danalux.local/wp-content/uploads/2021/03/icon__field-1.png', 0, 'attachment', 'image/png', 0),
(127, 1, '2021-03-01 15:51:36', '2021-03-01 08:51:36', '', 'icon__field--2', '', 'inherit', 'open', 'closed', '', 'icon__field-2', '', '', '2021-03-01 15:51:36', '2021-03-01 08:51:36', '', 21, 'http://danalux.local/wp-content/uploads/2021/03/icon__field-2.png', 0, 'attachment', 'image/png', 0),
(128, 1, '2021-03-01 15:51:37', '2021-03-01 08:51:37', '', 'icon__field--3', '', 'inherit', 'open', 'closed', '', 'icon__field-3', '', '', '2021-03-01 15:51:37', '2021-03-01 08:51:37', '', 21, 'http://danalux.local/wp-content/uploads/2021/03/icon__field-3.png', 0, 'attachment', 'image/png', 0),
(129, 1, '2021-03-01 15:51:38', '2021-03-01 08:51:38', '', 'icon__field--4', '', 'inherit', 'open', 'closed', '', 'icon__field-4', '', '', '2021-03-01 15:51:38', '2021-03-01 08:51:38', '', 21, 'http://danalux.local/wp-content/uploads/2021/03/icon__field-4.png', 0, 'attachment', 'image/png', 0),
(130, 1, '2021-03-01 15:54:05', '2021-03-01 08:54:05', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2021-03-01 15:54:05', '2021-03-01 08:54:05', '', 21, 'http://danalux.local/uncategorized/21-revision-v1.html', 0, 'revision', '', 0),
(131, 1, '2021-03-01 16:21:38', '2021-03-01 09:21:38', 'a:13:{s:4:\"type\";s:8:\"taxonomy\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:8:\"taxonomy\";s:12:\"danhmuc-duan\";s:10:\"field_type\";s:8:\"checkbox\";s:8:\"add_term\";i:1;s:10:\"save_terms\";i:0;s:10:\"load_terms\";i:0;s:13:\"return_format\";s:2:\"id\";s:8:\"multiple\";i:0;s:10:\"allow_null\";i:0;}', 'Nội dung', 'home_project_select_cat', 'publish', 'closed', 'closed', '', 'field_603cb1e456fde', '', '', '2021-03-02 15:37:35', '2021-03-02 08:37:35', '', 93, 'http://danalux.local/?post_type=acf-field&#038;p=131', 10, 'acf-field', '', 0),
(132, 1, '2021-03-01 16:21:51', '2021-03-01 09:21:51', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2021-03-01 16:21:51', '2021-03-01 09:21:51', '', 21, 'http://danalux.local/uncategorized/21-revision-v1.html', 0, 'revision', '', 0),
(133, 1, '2021-03-01 16:40:23', '2021-03-01 09:40:23', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:8:\"taxonomy\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:12:\"danhmuc-duan\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Ảnh đại diện danh mục Dự án', 'anh-dai-dien-danh-muc-du-an', 'publish', 'closed', 'closed', '', 'group_603cb64c655f7', '', '', '2021-03-05 10:36:32', '2021-03-05 03:36:32', '', 0, 'http://danalux.local/?post_type=acf-field-group&#038;p=133', 0, 'acf-field-group', '', 0),
(134, 1, '2021-03-01 16:40:23', '2021-03-01 09:40:23', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh', 'image_cat', 'publish', 'closed', 'closed', '', 'field_603cb65d225af', '', '', '2021-03-01 16:40:23', '2021-03-01 09:40:23', '', 133, 'http://danalux.local/?post_type=acf-field&p=134', 0, 'acf-field', '', 0),
(135, 1, '2021-03-01 16:41:15', '2021-03-01 09:41:15', '', 'project__1', '', 'inherit', 'open', 'closed', '', 'project__1', '', '', '2021-03-01 16:41:15', '2021-03-01 09:41:15', '', 0, 'http://danalux.local/wp-content/uploads/2021/03/project__1.jpg', 0, 'attachment', 'image/jpeg', 0),
(136, 1, '2021-03-01 16:41:16', '2021-03-01 09:41:16', '', 'project__2', '', 'inherit', 'open', 'closed', '', 'project__2', '', '', '2021-03-01 16:41:16', '2021-03-01 09:41:16', '', 0, 'http://danalux.local/wp-content/uploads/2021/03/project__2.jpg', 0, 'attachment', 'image/jpeg', 0),
(137, 1, '2021-03-01 17:17:25', '2021-03-01 10:17:25', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor', 'Bài viết tin tức 2', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor', 'publish', 'open', 'open', '', 'bai-viet-tin-tuc-2', '', '', '2021-03-01 17:17:25', '2021-03-01 10:17:25', '', 0, 'http://danalux.local/?p=137', 0, 'post', '', 0),
(138, 1, '2021-03-01 17:17:25', '2021-03-01 10:17:25', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor', 'Bài viết tin tức 2', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor', 'inherit', 'closed', 'closed', '', '137-revision-v1', '', '', '2021-03-01 17:17:25', '2021-03-01 10:17:25', '', 137, 'http://danalux.local/uncategorized/137-revision-v1.html', 0, 'revision', '', 0),
(139, 1, '2021-03-01 17:17:59', '2021-03-01 10:17:59', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor', 'Bài viết tin tức 1', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2021-03-01 17:17:59', '2021-03-01 10:17:59', '', 19, 'http://danalux.local/uncategorized/19-revision-v1.html', 0, 'revision', '', 0),
(140, 1, '2021-03-01 17:29:17', '2021-03-01 10:29:17', '', 'Bài viết tin tức 3', '', 'publish', 'open', 'open', '', 'bai-viet-tin-tuc-3', '', '', '2021-03-01 17:29:17', '2021-03-01 10:29:17', '', 0, 'http://danalux.local/?p=140', 0, 'post', '', 0),
(141, 1, '2021-03-01 17:29:17', '2021-03-01 10:29:17', '', 'Bài viết tin tức 3', '', 'inherit', 'closed', 'closed', '', '140-revision-v1', '', '', '2021-03-01 17:29:17', '2021-03-01 10:29:17', '', 140, 'http://danalux.local/uncategorized/140-revision-v1.html', 0, 'revision', '', 0),
(142, 1, '2021-03-01 18:35:41', '2021-03-01 11:35:41', '', 'Bài viết dịch vụ 2', '', 'publish', 'open', 'closed', '', 'bai-viet-dich-vu-2', '', '', '2021-03-01 18:35:41', '2021-03-01 11:35:41', '', 0, 'http://danalux.local/?post_type=service&#038;p=142', 0, 'service', '', 0),
(143, 1, '2021-03-02 08:56:09', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2021-03-02 08:56:09', '0000-00-00 00:00:00', '', 0, 'http://danalux.local/?p=143', 1, 'nav_menu_item', '', 0),
(144, 1, '2021-03-02 09:11:32', '2021-03-02 02:11:32', ' ', '', '', 'publish', 'closed', 'closed', '', '144', '', '', '2021-03-05 16:11:20', '2021-03-05 09:11:20', '', 0, 'http://danalux.local/?p=144', 3, 'nav_menu_item', '', 0),
(145, 1, '2021-03-02 09:11:32', '2021-03-02 02:11:32', ' ', '', '', 'publish', 'closed', 'closed', '', '145', '', '', '2021-03-05 16:11:20', '2021-03-05 09:11:20', '', 0, 'http://danalux.local/?p=145', 4, 'nav_menu_item', '', 0),
(146, 1, '2021-03-02 09:18:20', '2021-03-02 02:18:20', '', 'Bài viết dịch vụ 2', '', 'publish', 'open', 'closed', '', 'bai-viet-dich-vu-2', '', '', '2021-03-02 10:25:35', '2021-03-02 03:25:35', '', 0, 'http://danalux.local/?post_type=dichvu&#038;p=146', 0, 'dichvu', '', 0),
(147, 1, '2021-03-02 09:54:08', '2021-03-02 02:54:08', '', 'Dự án chi tiết 2', '', 'publish', 'open', 'closed', '', 'du-an-chi-tiet-2', '', '', '2021-03-02 09:57:24', '2021-03-02 02:57:24', '', 0, 'http://danalux.local/?post_type=duan&#038;p=147', 0, 'duan', '', 0),
(148, 1, '2021-03-02 09:57:38', '2021-03-02 02:57:38', '', 'Dự án thực hiện 1', '', 'publish', 'open', 'closed', '', 'du-an-thuc-hien-1', '', '', '2021-03-02 10:25:24', '2021-03-02 03:25:24', '', 0, 'http://danalux.local/?post_type=duan&#038;p=148', 0, 'duan', '', 0),
(149, 1, '2021-03-02 09:57:46', '2021-03-02 02:57:46', '', 'Dự án thực hiện 2', '', 'trash', 'open', 'closed', '', 'du-an-thuc-hien-2__trashed', '', '', '2021-03-02 09:59:07', '2021-03-02 02:59:07', '', 0, 'http://danalux.local/?post_type=duan&#038;p=149', 0, 'duan', '', 0),
(150, 1, '2021-03-02 10:24:54', '2021-03-02 03:24:54', 'Công ty cổ phần tư vấn và đầu tư Danalux là một trong những đơn vị tiên phong trong việc tư vấn & đầu tư, nhằm cung cấp cho các nhà đầu tư ý tưởng đầu tư, hướng dẫn và trợ giúp điều hành đối với nhà kinh doanh và các tổ chức khác trong vấn đề: lập dự án đầu tư; quản lý, lập chiến lược và kế hoạch hoạt động, ra quyết định tài chính, mục tiêu và chính sách thị trường, chính sách nguồn nhân lực, thực thi và kế hoạch; chương trình sản xuất và kế hoạch điều khiển.… giúp nhà đầu tư có sự lựa chọn, nắm bắt cơ hội để từ đó mang lại hiệu quả, lợi nhuận cao nhất trên địa bàn Đà Nẵng và các tỉnh Miền Trung, Tây Nguyên.\r\n\r\nVới đội ngũ nhân sự chuyên nghiệp và nhiều kinh nghiệm, chúng tôi sẽ đem đến cho các nhà đầu tư dịch vụ tốt nhất, tháo gỡ hầu hết các vướng mắc trong việc kinh doanh thông qua hoạt động Quan hệ và thông tin cộng đồng.', 'Bài viết tuyển dụng 1', 'Công ty cổ phần tư vấn và đầu tư Danalux là một trong những đơn vị tiên phong trong việc tư vấn & đầu tư, nhằm cung cấp cho các nhà đầu tư ý tưởng đầu tư, hướng dẫn và trợ giúp điều hành đối với nhà kinh doanh và các tổ chức khác trong vấn đề: lập dự án đầu tư; quản lý, lập chiến lược và kế hoạch hoạt động, ra quyết định tài chính, mục tiêu và chính sách thị trường, chính sách nguồn nhân lực, thực thi và kế hoạch; chương trình sản xuất và kế hoạch điều khiển.… giúp nhà đầu tư có sự lựa chọn, nắm bắt cơ hội để từ đó mang lại hiệu quả, lợi nhuận cao nhất trên địa bàn Đà Nẵng và các tỉnh Miền Trung, Tây Nguyên.\r\n\r\nVới đội ngũ nhân sự chuyên nghiệp và nhiều kinh nghiệm, chúng tôi sẽ đem đến cho các nhà đầu tư dịch vụ tốt nhất, tháo gỡ hầu hết các vướng mắc trong việc kinh doanh thông qua hoạt động Quan hệ và thông tin cộng đồng.', 'publish', 'open', 'closed', '', 'bai-viet-tuyen-dung-1', '', '', '2021-03-02 10:25:45', '2021-03-02 03:25:45', '', 0, 'http://danalux.local/?post_type=tuyendung&#038;p=150', 0, 'tuyendung', '', 0),
(151, 1, '2021-03-02 10:26:02', '2021-03-02 03:26:02', 'Công ty cổ phần tư vấn và đầu tư Danalux là một trong những đơn vị tiên phong trong việc tư vấn & đầu tư, nhằm cung cấp cho các nhà đầu tư ý tưởng đầu tư, hướng dẫn và trợ giúp điều hành đối với nhà kinh doanh và các tổ chức khác trong vấn đề: lập dự án đầu tư; quản lý, lập chiến lược và kế hoạch hoạt động, ra quyết định tài chính, mục tiêu và chính sách thị trường, chính sách nguồn nhân lực, thực thi và kế hoạch; chương trình sản xuất và kế hoạch điều khiển.… giúp nhà đầu tư có sự lựa chọn, nắm bắt cơ hội để từ đó mang lại hiệu quả, lợi nhuận cao nhất trên địa bàn Đà Nẵng và các tỉnh Miền Trung, Tây Nguyên.\r\n\r\nVới đội ngũ nhân sự chuyên nghiệp và nhiều kinh nghiệm, chúng tôi sẽ đem đến cho các nhà đầu tư dịch vụ tốt nhất, tháo gỡ hầu hết các vướng mắc trong việc kinh doanh thông qua hoạt động Quan hệ và thông tin cộng đồng.', 'Bài viết tuyển dụng 2', 'Công ty cổ phần tư vấn và đầu tư Danalux là một trong những đơn vị tiên phong trong việc tư vấn & đầu tư, nhằm cung cấp cho các nhà đầu tư ý tưởng đầu tư, hướng dẫn và trợ giúp điều hành đối với nhà kinh doanh và các tổ chức khác trong vấn đề: lập dự án đầu tư; quản lý, lập chiến lược và kế hoạch hoạt động, ra quyết định tài chính, mục tiêu và chính sách thị trường, chính sách nguồn nhân lực, thực thi và kế hoạch; chương trình sản xuất và kế hoạch điều khiển.… giúp nhà đầu tư có sự lựa chọn, nắm bắt cơ hội để từ đó mang lại hiệu quả, lợi nhuận cao nhất trên địa bàn Đà Nẵng và các tỉnh Miền Trung, Tây Nguyên.\r\n\r\nVới đội ngũ nhân sự chuyên nghiệp và nhiều kinh nghiệm, chúng tôi sẽ đem đến cho các nhà đầu tư dịch vụ tốt nhất, tháo gỡ hầu hết các vướng mắc trong việc kinh doanh thông qua hoạt động Quan hệ và thông tin cộng đồng.', 'publish', 'open', 'closed', '', 'bai-viet-tuyen-dung-2', '', '', '2021-03-02 10:26:02', '2021-03-02 03:26:02', '', 0, 'http://danalux.local/?post_type=tuyendung&#038;p=151', 0, 'tuyendung', '', 0),
(152, 1, '2021-03-02 10:33:44', '2021-03-02 03:33:44', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:24:\"template-recruitment.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Nhóm trường trang Tuyển dụng', 'nhom-truong-trang-tuyen-dung', 'publish', 'closed', 'closed', '', 'group_603db0825d497', '', '', '2021-03-04 11:07:38', '2021-03-04 04:07:38', '', 0, 'http://danalux.local/?post_type=acf-field-group&#038;p=152', 0, 'acf-field-group', '', 0),
(153, 1, '2021-03-02 10:33:44', '2021-03-02 03:33:44', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Chọn bài viết', 'chọn_bai_viết', 'publish', 'closed', 'closed', '', 'field_603db0a36352a', '', '', '2021-03-04 11:07:38', '2021-03-04 04:07:38', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=153', 0, 'acf-field', '', 0),
(154, 1, '2021-03-02 10:33:44', '2021-03-02 03:33:44', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:9:\"tuyendung\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:1:{i:0;s:6:\"search\";}s:8:\"elements\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'Nội dung', 'recruitment_select_post', 'publish', 'closed', 'closed', '', 'field_603db0ac6352b', '', '', '2021-03-04 11:07:38', '2021-03-04 04:07:38', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=154', 1, 'acf-field', '', 0),
(155, 1, '2021-03-02 10:33:44', '2021-03-02 03:33:44', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Môi trường làm việc tại công ty', 'moi_truờng_lam_việc_tại_cong_ty', 'publish', 'closed', 'closed', '', 'field_603db0f46352c', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=155', 2, 'acf-field', '', 0),
(156, 1, '2021-03-02 10:33:44', '2021-03-02 03:33:44', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'recruitment_environment_title', 'publish', 'closed', 'closed', '', 'field_603db0fa6352d', '', '', '2021-03-02 10:33:44', '2021-03-02 03:33:44', '', 152, 'http://danalux.local/?post_type=acf-field&p=156', 3, 'acf-field', '', 0),
(157, 1, '2021-03-02 10:33:44', '2021-03-02 03:33:44', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";i:4;s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Nội dung', 'recruitment_environment_content', 'publish', 'closed', 'closed', '', 'field_603db1236352e', '', '', '2021-03-02 10:34:43', '2021-03-02 03:34:43', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=157', 4, 'acf-field', '', 0),
(158, 1, '2021-03-02 10:33:44', '2021-03-02 03:33:44', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh', 'image', 'publish', 'closed', 'closed', '', 'field_603db1416352f', '', '', '2021-03-02 10:33:44', '2021-03-02 03:33:44', '', 157, 'http://danalux.local/?post_type=acf-field&p=158', 0, 'acf-field', '', 0),
(159, 1, '2021-03-02 10:33:44', '2021-03-02 03:33:44', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_603db15963530', '', '', '2021-03-02 10:33:44', '2021-03-02 03:33:44', '', 157, 'http://danalux.local/?post_type=acf-field&p=159', 1, 'acf-field', '', 0),
(160, 1, '2021-03-02 10:34:14', '2021-03-02 03:34:14', '', 'Tuyển dụng', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2021-03-02 10:34:14', '2021-03-02 03:34:14', '', 27, 'http://danalux.local/uncategorized/27-revision-v1.html', 0, 'revision', '', 0),
(161, 1, '2021-03-02 10:35:58', '2021-03-02 03:35:58', '', 'environment__1', '', 'inherit', 'open', 'closed', '', 'environment__1', '', '', '2021-03-02 10:35:58', '2021-03-02 03:35:58', '', 27, 'http://danalux.local/wp-content/uploads/2021/03/environment__1.png', 0, 'attachment', 'image/png', 0),
(162, 1, '2021-03-02 10:35:59', '2021-03-02 03:35:59', '', 'environment__2', '', 'inherit', 'open', 'closed', '', 'environment__2', '', '', '2021-03-02 10:35:59', '2021-03-02 03:35:59', '', 27, 'http://danalux.local/wp-content/uploads/2021/03/environment__2.png', 0, 'attachment', 'image/png', 0),
(163, 1, '2021-03-02 10:36:00', '2021-03-02 03:36:00', '', 'environment__3', '', 'inherit', 'open', 'closed', '', 'environment__3', '', '', '2021-03-02 10:36:00', '2021-03-02 03:36:00', '', 27, 'http://danalux.local/wp-content/uploads/2021/03/environment__3.png', 0, 'attachment', 'image/png', 0),
(164, 1, '2021-03-02 10:36:01', '2021-03-02 03:36:01', '', 'environment__4', '', 'inherit', 'open', 'closed', '', 'environment__4', '', '', '2021-03-02 10:36:01', '2021-03-02 03:36:01', '', 27, 'http://danalux.local/wp-content/uploads/2021/03/environment__4.png', 0, 'attachment', 'image/png', 0),
(165, 1, '2021-03-02 10:36:31', '2021-03-02 03:36:31', '', 'Tuyển dụng', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2021-03-02 10:36:31', '2021-03-02 03:36:31', '', 27, 'http://danalux.local/uncategorized/27-revision-v1.html', 0, 'revision', '', 0),
(167, 1, '2021-03-02 10:57:20', '2021-03-02 03:57:20', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Lợi ích khi làm việc', 'lợi_ich', 'publish', 'closed', 'closed', '', 'field_603db63102842', '', '', '2021-03-02 10:57:20', '2021-03-02 03:57:20', '', 152, 'http://danalux.local/?post_type=acf-field&p=167', 5, 'acf-field', '', 0),
(168, 1, '2021-03-02 10:57:20', '2021-03-02 03:57:20', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Lương thưởng', 'recruitment_bonus_title', 'publish', 'closed', 'closed', '', 'field_603db66402843', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=168', 7, 'acf-field', '', 0),
(169, 1, '2021-03-02 10:57:20', '2021-03-02 03:57:20', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Nội dung Lương thưởng', 'recruitment_bonus_content', 'publish', 'closed', 'closed', '', 'field_603db6ad02844', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=169', 8, 'acf-field', '', 0),
(170, 1, '2021-03-02 10:57:20', '2021-03-02 03:57:20', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_603db6bd02845', '', '', '2021-03-02 10:57:20', '2021-03-02 03:57:20', '', 169, 'http://danalux.local/?post_type=acf-field&p=170', 0, 'acf-field', '', 0),
(171, 1, '2021-03-02 10:57:20', '2021-03-02 03:57:20', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh Lương thưởng', 'recruitment_bonus_image', 'publish', 'closed', 'closed', '', 'field_603db6d802846', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=171', 9, 'acf-field', '', 0),
(172, 1, '2021-03-02 10:57:20', '2021-03-02 03:57:20', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Phúc lợi', 'recruitment_welfare_title', 'publish', 'closed', 'closed', '', 'field_603db6e102847', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=172', 10, 'acf-field', '', 0),
(173, 1, '2021-03-02 10:57:20', '2021-03-02 03:57:20', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Nội dung Phúc lợi', 'recruitment_welfare_content', 'publish', 'closed', 'closed', '', 'field_603db73602848', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=173', 11, 'acf-field', '', 0),
(174, 1, '2021-03-02 10:57:20', '2021-03-02 03:57:20', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_603db75702849', '', '', '2021-03-02 10:57:20', '2021-03-02 03:57:20', '', 173, 'http://danalux.local/?post_type=acf-field&p=174', 0, 'acf-field', '', 0),
(175, 1, '2021-03-02 10:57:20', '2021-03-02 03:57:20', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Album ảnh Phúc lợi', 'recruitment_welfare_gallery', 'publish', 'closed', 'closed', '', 'field_603db7640284a', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=175', 12, 'acf-field', '', 0),
(176, 1, '2021-03-02 10:58:26', '2021-03-02 03:58:26', '', 'Tuyển dụng', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2021-03-02 10:58:26', '2021-03-02 03:58:26', '', 27, 'http://danalux.local/uncategorized/27-revision-v1.html', 0, 'revision', '', 0),
(177, 1, '2021-03-02 11:00:11', '2021-03-02 04:00:11', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'recruitment_title', 'publish', 'closed', 'closed', '', 'field_603db82aef31c', '', '', '2021-03-02 11:00:11', '2021-03-02 04:00:11', '', 152, 'http://danalux.local/?post_type=acf-field&p=177', 6, 'acf-field', '', 0),
(178, 1, '2021-03-02 11:00:48', '2021-03-02 04:00:48', '', 'Tuyển dụng', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2021-03-02 11:00:48', '2021-03-02 04:00:48', '', 27, 'http://danalux.local/uncategorized/27-revision-v1.html', 0, 'revision', '', 0),
(179, 1, '2021-03-02 11:22:06', '2021-03-02 04:22:06', '<div class=\"module__header\"><h2 class=\"title\">NỘP ĐƠN ỨNG TUYỂN</h2></div>\r\n<div class=\"module__content\">\r\n<div class=\"row\">\r\n\r\n<div class=\"col-12\">\r\n<div class=\"form-group\">\r\n    [text* your-name class:form-control class:input__text placeholder \"Họ và tên\"]\r\n</div>\r\n</div>\r\n<div class=\"col-12 col-lg-6\">\r\n<div class=\"form-group\">\r\n    [email* your-email class:form-control class:input__text placeholder \"Email\"]\r\n</div>\r\n</div>\r\n<div class=\"col-12 col-lg-6\">\r\n<div class=\"form-group\">\r\n    [tel* your-tel class:form-control class:input__text placeholder \"Số điện thoại\"]\r\n</div>\r\n</div>\r\n<div class=\"col-12\">\r\n<div class=\"form-group\">\r\n    [select* your-select class:form-control class:select__text \"Chọn vị trí ứng tuyển\" \"Thực tập sinh 1\" \"Thực tập sinh 2\"]\r\n</div>\r\n</div>\r\n<div class=\"col-12\">\r\n<div class=\"form-group\">\r\n    [textarea* your-message class:form-control class:textare__text placeholder \"Thư ứng tuyển\"]\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-12\">\r\n<div class=\"form-group\">\r\n<span class=\"fileName\"></span>\r\n<label for=\"file\" class=\"file form-control cus__file\">\r\n    [file* your-file id:file filetypes:.pdf|.doc|.docx|.ppt|.pptx class:input--file]\r\n    <span class=\"file__text\">\r\n        <span class=\"icon\"><i class=\"fas fa-cloud-upload-alt\"></i></span>\r\n        <span class=\"text\">TẢI LÊN CV ỨNG TUYỂN CỦA BẠN</span>\r\n    </span>\r\n</label>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-12\">\r\n<div class=\"form-group\">\r\n    [text* your-date class:form-control class:input__date placeholder \"Date\"]\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-12\">\r\n    [submit class:btn class:btn__sen \"GỬI ĐƠN ỨNG TUYỂN\"]\r\n</div>\r\n\r\n</div>\r\n</div>\n1\n[your-name] - Ứng tuyển\n[_site_title] <wordpress@danalux.local>\n[_site_admin_email]\nTừ: [your-name] <[your-email]>\r\nSố điện thoại: [your-tel]\r\n\r\nVị trí ứng tuyển : [your-select]\r\nTin nhắn: [your-message]\r\nFile CV:\r\n[your-file]\r\n\r\nEmail được gửi từ [_site_title] ([_site_url])\nReply-To: [your-email]\n[your-file]\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@danalux.local>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Form tuyển dụng', '', 'publish', 'closed', 'closed', '', 'form-tuyen-dung', '', '', '2021-03-05 10:32:11', '2021-03-05 03:32:11', '', 0, 'http://danalux.local/?post_type=wpcf7_contact_form&#038;p=179', 0, 'wpcf7_contact_form', '', 0),
(180, 1, '2021-03-02 11:44:32', '2021-03-02 04:44:32', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Form tuyển dụng', 'form_tuyển_dụng', 'publish', 'closed', 'closed', '', 'field_603dc16bc7673', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 152, 'http://danalux.local/?post_type=acf-field&p=180', 13, 'acf-field', '', 0),
(181, 1, '2021-03-02 11:44:32', '2021-03-02 04:44:32', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Nội dung', 'recruitment_form', 'publish', 'closed', 'closed', '', 'field_603dc175c7674', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 152, 'http://danalux.local/?post_type=acf-field&p=181', 14, 'acf-field', '', 0),
(182, 1, '2021-03-02 11:44:32', '2021-03-02 04:44:32', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Môi trường làm việc', 'moi_truờng', 'publish', 'closed', 'closed', '', 'field_603dc1c9c7675', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 152, 'http://danalux.local/?post_type=acf-field&p=182', 15, 'acf-field', '', 0),
(183, 1, '2021-03-02 11:44:32', '2021-03-02 04:44:32', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'recruitment_envi_title', 'publish', 'closed', 'closed', '', 'field_603dc218c7677', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 152, 'http://danalux.local/?post_type=acf-field&p=183', 16, 'acf-field', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(184, 1, '2021-03-02 11:44:32', '2021-03-02 04:44:32', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Nội dung Môi trường làm việc', 'recruitment_envi_content', 'publish', 'closed', 'closed', '', 'field_603dc233c7679', '', '', '2021-03-04 11:03:06', '2021-03-04 04:03:06', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=184', 17, 'acf-field', '', 0),
(185, 1, '2021-03-02 11:44:32', '2021-03-02 04:44:32', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_603dc233c767a', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 184, 'http://danalux.local/?post_type=acf-field&p=185', 0, 'acf-field', '', 0),
(186, 1, '2021-03-02 11:44:32', '2021-03-02 04:44:32', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh', 'recruitment_envi_image', 'publish', 'closed', 'closed', '', 'field_603dc23fc767d', '', '', '2021-03-04 11:04:11', '2021-03-04 04:04:11', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=186', 18, 'acf-field', '', 0),
(187, 1, '2021-03-02 11:44:32', '2021-03-02 04:44:32', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Thời gian làm việc', 'thời_gian', 'publish', 'closed', 'closed', '', 'field_603dc1d4c7676', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 152, 'http://danalux.local/?post_type=acf-field&p=187', 19, 'acf-field', '', 0),
(188, 1, '2021-03-02 11:44:32', '2021-03-02 04:44:32', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'recruitment_time', 'publish', 'closed', 'closed', '', 'field_603dc220c7678', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 152, 'http://danalux.local/?post_type=acf-field&p=188', 20, 'acf-field', '', 0),
(189, 1, '2021-03-02 11:44:32', '2021-03-02 04:44:32', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Nội dung Thời gian làm việc', 'recruitment_time_content', 'publish', 'closed', 'closed', '', 'field_603dc238c767b', '', '', '2021-03-04 11:04:11', '2021-03-04 04:04:11', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=189', 21, 'acf-field', '', 0),
(190, 1, '2021-03-02 11:44:32', '2021-03-02 04:44:32', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tiêu đề', 'title', 'publish', 'closed', 'closed', '', 'field_603dc238c767c', '', '', '2021-03-02 11:44:32', '2021-03-02 04:44:32', '', 189, 'http://danalux.local/?post_type=acf-field&p=190', 0, 'acf-field', '', 0),
(191, 1, '2021-03-02 11:44:32', '2021-03-02 04:44:32', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh', 'recruitment_time_image', 'publish', 'closed', 'closed', '', 'field_603dc244c767e', '', '', '2021-03-04 11:04:11', '2021-03-04 04:04:11', '', 152, 'http://danalux.local/?post_type=acf-field&#038;p=191', 22, 'acf-field', '', 0),
(192, 1, '2021-03-02 11:46:11', '2021-03-02 04:46:11', '', 'Tuyển dụng', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2021-03-02 11:46:11', '2021-03-02 04:46:11', '', 27, 'http://danalux.local/uncategorized/27-revision-v1.html', 0, 'revision', '', 0),
(193, 1, '2021-03-02 14:08:51', '2021-03-02 07:08:51', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"theme-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Form ứng tuyển Trang chi tiết tuyển dụng', 'form-ung-tuyen-trang-chi-tiet-tuyen-dung', 'publish', 'closed', 'closed', '', 'group_603de4146acc0', '', '', '2021-03-02 14:10:13', '2021-03-02 07:10:13', '', 0, 'http://danalux.local/?post_type=acf-field-group&#038;p=193', 0, 'acf-field-group', '', 0),
(194, 1, '2021-03-02 14:08:51', '2021-03-02 07:08:51', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Nhập form', 'single_recruitment_form', 'publish', 'closed', 'closed', '', 'field_603de4756efcb', '', '', '2021-03-02 14:10:13', '2021-03-02 07:10:13', '', 193, 'http://danalux.local/?post_type=acf-field&#038;p=194', 0, 'acf-field', '', 0),
(195, 1, '2021-03-02 15:37:43', '2021-03-02 08:37:43', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2021-03-02 15:37:43', '2021-03-02 08:37:43', '', 21, 'http://danalux.local/uncategorized/21-revision-v1.html', 0, 'revision', '', 0),
(196, 1, '2021-03-04 10:27:40', '2021-03-04 03:27:40', '', 'Tuyển dụng', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2021-03-04 10:27:40', '2021-03-04 03:27:40', '', 27, 'http://danalux.local/uncategorized/27-revision-v1.html', 0, 'revision', '', 0),
(197, 1, '2021-03-04 10:41:29', '2021-03-04 03:41:29', '', 'Tuyển dụng', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2021-03-04 10:41:29', '2021-03-04 03:41:29', '', 27, 'http://danalux.local/uncategorized/27-revision-v1.html', 0, 'revision', '', 0),
(198, 1, '2021-03-04 10:53:43', '2021-03-04 03:53:43', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Ảnh hiển thị', 'home_intro_video_image', 'publish', 'closed', 'closed', '', 'field_60405967b8fbd', '', '', '2021-03-04 10:53:43', '2021-03-04 03:53:43', '', 93, 'http://danalux.local/?post_type=acf-field&p=198', 15, 'acf-field', '', 0),
(200, 1, '2021-03-04 10:54:36', '2021-03-04 03:54:36', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2021-03-04 10:54:36', '2021-03-04 03:54:36', '', 21, 'http://danalux.local/uncategorized/21-revision-v1.html', 0, 'revision', '', 0),
(201, 1, '2021-03-04 10:56:42', '2021-03-04 03:56:42', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2021-03-04 10:56:42', '2021-03-04 03:56:42', '', 21, 'http://danalux.local/uncategorized/21-revision-v1.html', 0, 'revision', '', 0),
(202, 1, '2021-03-04 11:05:34', '2021-03-04 04:05:34', '', 'Tuyển dụng', '', 'inherit', 'closed', 'closed', '', '27-revision-v1', '', '', '2021-03-04 11:05:34', '2021-03-04 04:05:34', '', 27, 'http://danalux.local/uncategorized/27-revision-v1.html', 0, 'revision', '', 0),
(203, 1, '2021-03-04 15:24:01', '2021-03-04 08:24:01', '<label> Your name\r\n    [text* your-name] </label>\r\n\r\n<label> Your email\r\n    [email* your-email] </label>\r\n\r\n<label> Subject\r\n    [text* your-subject] </label>\r\n\r\n<label> Your message (optional)\r\n    [textarea your-message] </label>\r\n[file file-562]\r\n[submit \"Submit\"]\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@danalux.local>\n[_site_admin_email]\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@danalux.local>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Untitled', '', 'publish', 'closed', 'closed', '', 'untitled', '', '', '2021-03-04 15:24:05', '2021-03-04 08:24:05', '', 0, 'http://danalux.local/?post_type=wpcf7_contact_form&#038;p=203', 0, 'wpcf7_contact_form', '', 0),
(204, 1, '2021-03-04 16:30:18', '2021-03-04 09:30:18', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"theme-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Sidebar', 'sidebar', 'publish', 'closed', 'closed', '', 'group_6040a858d2f6f', '', '', '2021-03-04 16:30:18', '2021-03-04 09:30:18', '', 0, 'http://danalux.local/?post_type=acf-field-group&#038;p=204', 0, 'acf-field-group', '', 0),
(205, 1, '2021-03-04 16:30:18', '2021-03-04 09:30:18', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:6:\"dichvu\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:1:{i:0;s:6:\"search\";}s:8:\"elements\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";i:6;s:13:\"return_format\";s:6:\"object\";}', 'Chọn bài viết dịch vụ', 'sidebar_service_select_post', 'publish', 'closed', 'closed', '', 'field_6040a86fdb97b', '', '', '2021-03-04 16:30:18', '2021-03-04 09:30:18', '', 204, 'http://danalux.local/?post_type=acf-field&p=205', 0, 'acf-field', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_termmeta`
--

INSERT INTO `wp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 7, 'image_cat', '135'),
(2, 7, '_image_cat', 'field_603cb65d225af'),
(3, 8, 'image_cat', '136'),
(4, 8, '_image_cat', 'field_603cb65d225af'),
(5, 10, 'image_cat', '164'),
(6, 10, '_image_cat', 'field_603cb65d225af');

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Dịch vụ', 'dich-vu', 0),
(3, 'Dịch vụ', 'dich-vu', 0),
(5, 'Tin tức', 'tin-tuc', 0),
(6, 'Menu chính', 'menu-chinh', 0),
(7, 'Dự án thực hiện', 'du-an-thuc-hien', 0),
(8, 'Chi tiết dự án', 'chi-tiet-du-an', 0),
(9, 'Dự án thực hiện', 'du-an-thuc-hien', 0),
(10, 'Dự án chi tiết', 'du-an-chi-tiet', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(15, 2, 0),
(16, 10, 0),
(17, 3, 0),
(18, 7, 0),
(18, 8, 0),
(19, 5, 0),
(30, 6, 0),
(31, 6, 0),
(32, 6, 0),
(33, 6, 0),
(36, 6, 0),
(137, 5, 0),
(140, 5, 0),
(142, 3, 0),
(144, 6, 0),
(145, 6, 0),
(146, 2, 0),
(147, 10, 0),
(148, 9, 0),
(149, 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'danhmuc-dichvu', '', 0, 2),
(3, 3, 'service_cat', '', 0, 2),
(5, 5, 'category', '', 0, 3),
(6, 6, 'nav_menu', '', 0, 7),
(7, 7, 'project_cat', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.', 0, 1),
(8, 8, 'project_cat', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.', 0, 1),
(9, 9, 'danhmuc-duan', '', 0, 1),
(10, 10, 'danhmuc-duan', '', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"f6960e0a79ce54ab498da9891673887b95ee1bdff2737408a42f8155c55dbcc7\";a:4:{s:10:\"expiration\";i:1615609753;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36\";s:5:\"login\";i:1614400153;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:3:{i:0;s:22:\"add-post-type-services\";i:1;s:12:\"add-post_tag\";i:2;s:16:\"add-services-cat\";}'),
(21, 1, 'wp_user-settings', 'libraryContent=browse&editor=html&hidetb=1'),
(22, 1, 'wp_user-settings-time', '1614588841'),
(23, 1, 'wp_mail_smtp_pro_banner_dismissed', '1'),
(24, 1, 'nav_menu_recently_edited', '6'),
(25, 1, 'closedpostboxes_post', 'a:0:{}'),
(26, 1, 'metaboxhidden_post', 'a:5:{i:0;s:13:\"trackbacksdiv\";i:1;s:16:\"commentstatusdiv\";i:2;s:11:\"commentsdiv\";i:3;s:7:\"slugdiv\";i:4;s:9:\"authordiv\";}'),
(27, 1, 'closedpostboxes_tuyendung', 'a:0:{}'),
(28, 1, 'metaboxhidden_tuyendung', 'a:3:{i:0;s:16:\"commentstatusdiv\";i:1;s:11:\"commentsdiv\";i:2;s:7:\"slugdiv\";}'),
(29, 1, 'closedpostboxes_duan', 'a:0:{}'),
(30, 1, 'metaboxhidden_duan', 'a:3:{i:0;s:16:\"commentstatusdiv\";i:1;s:11:\"commentsdiv\";i:2;s:7:\"slugdiv\";}'),
(31, 1, 'closedpostboxes_dichvu', 'a:0:{}'),
(32, 1, 'metaboxhidden_dichvu', 'a:3:{i:0;s:16:\"commentstatusdiv\";i:1;s:11:\"commentsdiv\";i:2;s:7:\"slugdiv\";}'),
(33, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(34, 1, 'metaboxhidden_dashboard', 'a:3:{i:0;s:21:\"dashboard_site_health\";i:1;s:21:\"dashboard_quick_press\";i:2;s:17:\"dashboard_primary\";}'),
(35, 1, 'meta-box-order_dashboard', 'a:4:{s:6:\"normal\";s:41:\"dashboard_site_health,dashboard_right_now\";s:4:\"side\";s:58:\"dashboard_quick_press,dashboard_primary,dashboard_activity\";s:7:\"column3\";s:0:\"\";s:7:\"column4\";s:0:\"\";}'),
(36, 1, 'closedpostboxes_toplevel_page_theme-settings', 'a:3:{i:0;s:23:\"acf-group_603c6d574ad44\";i:1;s:23:\"acf-group_603de4146acc0\";i:2;s:23:\"acf-group_603c6b2e1c64d\";}'),
(37, 1, 'metaboxhidden_toplevel_page_theme-settings', 'a:0:{}'),
(38, 1, 'wpcf7_hide_welcome_panel_on', 'a:1:{i:0;s:3:\"5.3\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$Bqo6cmRvyYCjC0/6J5WaRjrvMaiDSr/', 'admin', 'tiepnguyen220194@gmail.com', 'http://danalux.local', '2021-02-27 04:21:07', '', 0, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpmailsmtp_tasks_meta`
--

CREATE TABLE `wp_wpmailsmtp_tasks_meta` (
  `id` bigint(20) NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_wpmailsmtp_tasks_meta`
--

INSERT INTO `wp_wpmailsmtp_tasks_meta` (`id`, `action`, `data`, `date`) VALUES
(1, 'wp_mail_smtp_admin_notifications_update', 'W10=', '2021-03-01 08:10:42'),
(2, 'wp_mail_smtp_admin_notifications_update', 'W10=', '2021-03-02 08:31:28'),
(3, 'wp_mail_smtp_admin_notifications_update', 'W10=', '2021-03-04 01:13:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`);

--
-- Indexes for table `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Indexes for table `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Indexes for table `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Indexes for table `wp_cf7_vdata`
--
ALTER TABLE `wp_cf7_vdata`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_cf7_vdata_entry`
--
ALTER TABLE `wp_cf7_vdata_entry`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_wpmailsmtp_tasks_meta`
--
ALTER TABLE `wp_wpmailsmtp_tasks_meta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `wp_cf7_vdata`
--
ALTER TABLE `wp_cf7_vdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `wp_cf7_vdata_entry`
--
ALTER TABLE `wp_cf7_vdata_entry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=967;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1314;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_wpmailsmtp_tasks_meta`
--
ALTER TABLE `wp_wpmailsmtp_tasks_meta`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
